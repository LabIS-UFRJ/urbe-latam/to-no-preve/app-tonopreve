import React from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import { useState } from 'react';
import Constants from 'expo-constants';
import i18n from 'i18n-js'

const pickerSelectStyles = StyleSheet.create(pickerStyles);

export const NovoTipoConflito = (props) => {
    const [novoTipo,setNovoTipo] = useState("");

    const onValueChange = (value) => {
        //Outro uso; 
        setNovoTipo(value[1]);
        props.onValueChange({ tipoConflito: {  nome:value[1], id:value[0] } })
        
    };


    return ( 
        <TextInput
            label={`${i18n.t('novoTipoConflitoLabel')} ${Constants.manifest.extra.translations.badCategorySingular}`}
            value={novoTipo}
            style={Inputs.textInput}
            labelStyle={Inputs.textInputLabel}
            onChangeText={text => onValueChange( [tipos.data.tiposConflito.length,text]) }
            clearTextOnFocus ={true}
            autoFocus={true}
        />
    )
};

