import React from 'react'
import { TextInput } from 'react-native-paper'
import { StyleSheet, View, Icon } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { pickerStyles } from '../styles/pickers'
import { Inputs } from '../styles'

const pickerSelectStyles = StyleSheet.create(pickerStyles)

export const TipoFormSelect = ({ label, onValueChange, value, options }) => {
    return (
        <View>
            <TextInput
                label={label}
                value={(() => {
                    //console.log('O VALUE 0', value)
                    return value
                })()}
                style={Inputs.textInput}
                labelStyle={Inputs.textInputLabel}
                render={() => (
                    <RNPickerSelect
                        onValueChange={(selectedValue) => {
                            if (selectedValue) {
                                onValueChange(selectedValue)
                            }
                        }}
                        placeholder={{ label: '', value: null }}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{multiline: true}}
                        style={pickerSelectStyles}
                        items={[...options]}
                        value={value}
                    />
                )}
            />
        </View>
    )
}
