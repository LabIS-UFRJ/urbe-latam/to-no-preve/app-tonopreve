import React,{useEffect} from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import { useState } from 'react';
import {getTiposAreaDeUsoCache} from "../db/api"
import Constants from 'expo-constants';
import i18n from 'i18n-js'

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoAreaDeUsoSelect = (props) => {
    const [localTipos,setLocalTipos] = useState([])

    useEffect(  ()=>{
        getTiposAreaDeUsoCache()
        .then((val) =>{
            setLocalTipos(val )
        })
    },[])

    const onValueChange = (value) => {        
        if (value != null){
            
            const nome = (getTiposAreaDeUso().filter(v => v.value == value))[0].nome
            props.onValueChange(value,nome)
        }
    };

    const getTiposAreaDeUso = () => {
        return localTipos;
    };

    return(
    <TextInput
        label={`${i18n.t('tipoAreaDeUsoSelectLabel')} ${Constants.manifest.extra.translations.goodCategorySingular}`}
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getTiposAreaDeUso()}
                value={props.selectedValue}
            />
        )}
    />)
};

export default TipoAreaDeUsoSelect;
