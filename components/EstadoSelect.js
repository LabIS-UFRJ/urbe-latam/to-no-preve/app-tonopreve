import React from 'react'
import estados from "../bases/estados";
import {Inputs} from "../styles";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import i18n from 'i18n-js';

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const EstadoSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    return <TextInput
        label={i18n.t("State")}
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={estados}
                value={props.selectedValue}
            />
        )}
    />
};

export default EstadoSelect;
