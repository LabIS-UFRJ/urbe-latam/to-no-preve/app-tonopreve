import React from 'react'
import { View } from 'react-native'
import { Switch, Text } from 'react-native-paper'
import colors from '../../assets/colors'

const ToggleStyles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25
}

export const Toggle = ({ label, value, onValueChange }) => {
    return (
        <View style={ToggleStyles}>
            <Text
                style={{
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'gray'
                }}
            >
                {label}
            </Text>
            <Switch
                color={colors.primary}
                value={value}
                onValueChange={(state) => {
                    onValueChange(state)
                }}
            />
        </View>
    )
}
