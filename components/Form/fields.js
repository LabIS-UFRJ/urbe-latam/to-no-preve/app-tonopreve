export const fields = [
    {
        name: 'entrevistador',
        label: 'Nome do entrevistador',
        type: 'string',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'setor',
        label: 'Setor em que a casa está alocada',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'endereco',
        label: 'Endereço residencial',
        type: 'string',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'telefone',
        label: 'Telefone de contato',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'numeroCasa',
        label: 'Número da casa',
        type: 'string',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'numMen18',
        label: 'Menores de 18 anos',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'num1855',
        label: 'Pessoas entre 18 e 55 anos',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'num55Mais',
        label: 'Maiores de 55 anos',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'numNe',
        label: 'Quantas pessoas na casa têm dificuldade de locomoção',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'numPets',
        label: 'Número de animais de estimação na casa',
        type: 'number',
        isSubForm: false,
        isSelect: false
    },
    {
        name: 'matPredom',
        label: 'Material predominante na construção da casa',
        type: 'string',
        options: [
            {
                value: 'tijolo',
                label: 'Tijolo'
            },
            {
                value: 'bloco de concreto',
                label: 'Bloco de concreto'
            },
            {
                value: 'madeira',
                label: 'Madeira'
            },
            {
                value: 'mistura de materiais',
                label: 'Mistura de materiais (mais de um dos materiais acima)'
            },
            {
                value: 'outros materiais',
                label: 'Outros materiais (Plástico, papelão, etc.)'
            },
            {
                value: 'não sei',
                label: 'Não sei'
            }
        ],

        isSelect: true,
        isSubForm: false
    },
    {
        name: 'sustentacao',
        label: 'A casa possui estruturas de sustentação como vigas e/ou pilares de concreto ou aço',
        type: 'string',
        isSubForm: false,
        options: [
            {
                value: 'Sim',
                label: 'Sim'
            },
            {
                value: 'Não',
                label: 'Não'
            }
        ],
        isSelect: true,
    },
    {
        name: 'conserv',
        label: 'Você considera o estado de conservação de sua casa',
        type: 'string',
        options: [
            {
                value: 'bom',
                label: 'Bom (Não há umidade/rachaduras/piso afundando)'
            },
            {
                value: 'aceitável',
                label: 'Aceitável (Existe pelo menos um ponto de umidade/rachaduras/piso afundando)'
            },
            {
                value: 'deficiente',
                label: 'Deficiente'
            },
            {
                value: 'não sei',
                label: 'Não sei'
            }
        ],
        isSubForm: false,
        isSelect: true
    },
    {
        name: 'matTelhado',
        label: 'Material predominante que cobre a casa (cobertura ou telhado da casa)',
        type: 'string',
        options: [
            {
                value: 'Telha de barro',
                label: 'Telha de barro'
            },
            {
                value: 'Telha de amianto (Eternit)',
                label: 'Telha de amianto (Eternit)'
            },
            {
                value: 'Teto de madeira',
                label: 'Teto de madeira'
            },
            {
                value: 'Laje de concreto',
                label: 'Laje de concreto'
            },
            {
                value: 'não sei',
                label: 'Não sei'
            }
        ],
        isSelect: true,
        isSubForm: false
    },
    {
        name: 'matCobre',
        label: 'Você considera o estado de conservação da cobertura (ou telhado) da casa',
        type: 'string',
        options: [
            {
                value: 'bom',
                label: 'Bom (madeira de suporte não tem deterioração/há amarração das telhas)'
            },
            { value: 'deficiente', label: 'Deficiente' },
            {
                value: 'não sei',
                label: 'Não sei'
            }
        ],
        isSelect: true,
        isSubForm: false
    },
    {
        name: 'idadeCasa',
        label: 'Qual é a idade da casa aproximadamente?',
        type: 'number',
        isSelect: false,
        isSubForm: false
    },
    {
        name: 'ultRef',
        label: 'Quando foi a última reforma da casa aproximadamente?',
        type: 'string',
        isSelect: false,
        isSubForm: false
    },
    {
        name: 'numPisos',
        label: 'Número de pisos (andares) da casa',
        type: 'number',
        isSelect: false,
        isSubForm: false
    },
    {
        name: 'pisoMorador',
        label: 'Em que piso (andar) você mora',
        type: 'number',
        isSelect: false,
        isSubForm: false
    },
    {
        name: 'matAndMora',
        label: 'Material predominante do andar em que mora',
        type: 'string',
        options: [
            {
                value: 'mármore, porcelanato, taco, madeira polida e envernizado',
                label: 'Mármore, porcelanato, taco, madeira polida e envernizado'
            },
            {
                value: 'azulejo, cerâmica, vinil, pastilha ou tijolo',
                label: 'Azulejo, cerâmica, vinil, pastilha ou tijolo'
            },
            {
                value: 'cimento ou argamassa',
                label: 'Cimento ou argamassa'
            },
            {
                value: 'madeira bruta, madeira em mau estado, prancha',
                label: 'Madeira bruta, madeira em mau estado, prancha'
            },
            {
                value: 'terra ou areia',
                label: 'Terra ou areia'
            },
            {
                value: 'não sei',
                label: 'Não sei'
            }
        ],
        isSelect: true,
        isSubForm: false
    },
    {
        name: 'evento',
        label: 'A casa já foi atingida por deslizamentos de terra, de rochas ou de pedras ou inundações?',
        type: 'boolean',
        isSelect: false,
        isSubForm: true,
        subForm: [
            {
                name: 'tipo',
                label: 'Tipo de evento',
                isSelect: true,
                isEvent: true,
                isSubForm: false,
                options: [
                    {
                        value: 'deslizamentos de terra, de rochas ou de pedras',
                        label: 'Deslizamentos de terra, de rochas ou de pedras'
                    },
                    {
                        value: 'inundações',
                        label: 'Inundações'
                    },
                    {
                        value: 'não sei',
                        label: 'Não sei'
                    }
                ]
            },
            {
                name: 'data',
                label: 'Mês e ano do evento aproximadamente',
                isSelect: false,
                isSubForm: false,
                type: 'string'
            },
            {
                name: 'dano',
                label: 'Houve algum dano resultante do evento',
                isSelect: false,
                isSubForm: true,
                type: 'boolean',
                subForm: [
                    {
                        name: 'impacto',
                        label: 'Na sua opinião, o dano foi de impacto',
                        isSubForm: false,
                        isSelect: true,
                        options: [
                            {
                                value: 'alto',
                                label: 'Alto (A casa teve danos estruturais em suas paredes, fundações ou telhado)'
                            },
                            {
                                value: 'médio',
                                label: 'Médio (A casa teve danos pequenos  em paredes, fundações ou telhado)'
                            },
                            {
                                value: 'baixo',
                                label: 'Baixo (A casa não foi muito danificada)'
                            },
                            {
                                value: 'não sei',
                                label: 'Não sei'
                            }
                        ]
                    },
                    {
                        name: 'reparo',
                        label: 'Os danos foram reparados principalmente de forma',
                        isSelect: true,
                        isSubForm: false,
                        options: [
                            {
                                value: 'autônomo',
                                label: 'Autónoma (feita pela própria família)'
                            },
                            {
                                value: 'coletiva',
                                label: 'Coletiva (em mutirão com a comunidade)'
                            },
                            {
                                value: 'não sei',
                                label: 'Não sei'
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

export const findLabel = (name) => {
    const field = fields.filter((field) => field.name === name)
    if (field.length) {
        return field[0].label
    }
    return null
}
