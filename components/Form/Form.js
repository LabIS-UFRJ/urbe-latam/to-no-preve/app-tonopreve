import React, { useState, useEffect } from 'react'
import { TextInput, Button } from 'react-native-paper'
import { fields } from './fields'
import { Text,View,StyleSheet } from 'react-native'
import { Inputs, Layout } from '../../styles'
import { TipoFormSelect } from '../TipoFormSelect'
import colors from '../../assets/colors'
import { Toggle } from './Toggle'
import { eventTypes } from './events'
import {TextInputMask} from 'react-native-masked-text';

const TextStyle = StyleSheet.create({
    baseText: {
        paddingTop: 20,
        fontSize: 20,
        marginBottom: 0,
        paddingBottom:0
    }
  });
  


export const Form = ({
    formValue,
    editMode,
    shouldClose,
    onSave,
    onDelete
}) => {
    const [formRendered, setformRendered] = useState(false);
    const [state, setState] = useState({ ...formValue })
    const [toggle, setToggle] = useState({
        dano: !!state.evento?.impacto,
        evento: !!state.evento
    })


    if (!state['evento']) {
        setState({
            ...state,
            evento: {
                id: null,
                tipo: null,
                data: null,
                impacto: null,
                reparo: null
            }
        })
    }

    useEffect(()=> {
        if(formRendered)
            shouldClose()
    },[formValue])


    useEffect(() => {
        if (!toggle.dano) {
            setState({
                ...state,
                evento: {
                    ...state.evento,
                    impacto: null,
                    reparo: null
                }
            })
        }
    }, [toggle.dano])

    //useEffect(() => {
    //    if (saved) {
    //        shouldClose()
    //    }
    //}, [saved])

    useEffect(() => {
        if (!toggle.evento) {
            const newState = { ...state }
            Object.keys(newState['evento']).forEach((key) => {
                newState['evento'][key] = null
            })

            setState({ ...newState })
        }
    }, [toggle.evento])

    const handleFormValues = (key, value) => {
        const newState = { ...state, [key]: value }
        setState({ ...newState })
    }

    const handleSubForValue = (key, parentName, newValue, _id) => {
        let newObjectValue
        if (_id) {
            const id = eventTypes.filter(
                (type) => type.name === newValue.toLowerCase()
            )[0].id

            newObjectValue = {
                ...state[parentName],
                [key]: newValue,
                id
            }
        } else {
            newObjectValue = {
                ...state[parentName],
                [key]: newValue
            }
        }

        handleFormValues(parentName, { ...newObjectValue })
    }

    const handleSubForm = (subForm, parentName) => {
        return subForm.map((element, _index) => {
            if (element.isSubForm) {
                return (                    
                    <View key={element.label}>
                        <Toggle
                            label={element.label}
                            value={!!toggle[element.name]}
                            onValueChange={(v) => {
                                setToggle({
                                    ...toggle,
                                    [element.name]: v
                                })
                            }}
                        />

                        {!!toggle[element.name] &&
                            handleSubForm(element.subForm, parentName)}
                    </View>
                )
            } else {
                return element.isSelect ? (
                    <TipoFormSelect
                        key={element.label}
                        label={element.label}
                        onValueChange={(newSelectedValue) => {
                            if (newSelectedValue) {
                                handleSubForValue(
                                    element.name,
                                    parentName,
                                    newSelectedValue,
                                    !!element.isEvent
                                )
                            }
                        }}
                        options={element.options}
                        value={
                            !!state[parentName][element.name]
                                ? `${state[parentName][element.name]}`
                                : null
                        }
                    />
                ) : (
                    <TextInput
                        key={element.label}
                        label={element.label}
                        style={Inputs.textInput}
                        labelStyle={Inputs.textInputLabel}
                        keyboardType={resolveInputType(element.type)}
                        value={
                            !!state[parentName][element.name]
                                ? `${state[parentName][element.name]}`
                                : null
                        }
                        onChangeText={(newTextValue) =>
                            handleSubForValue(
                                element.name,
                                parentName,
                                newTextValue,
                                !!element.isEvent
                            )
                        }
                    />
                )
            }
        })
    }

    const resolveInputType = (inputType) => {
        switch (inputType) {
            case 'number': {
                return 'numeric'
            }
            case 'string': {
                return 'default'
            }
            case "number":{

            }
        }
    }

    const resolveTexInput= (field)=>{
        if (field.label!="Telefone de contato"){
            return <TextInput
                key={"textI".concat(field.label)}
                style={Inputs.textInput}
                labelStyle={Inputs.textInputLabel}
                value={state[field.name] ? `${state[field.name]}` : ''}
                keyboardType={resolveInputType(field.type)}
                onChangeText={(newTextValue) =>
                    handleFormValues(field.name, newTextValue)
                }
                />
        }
        return <TextInput
        key={"textI".concat(field.label)}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        value={state[field.name] ? `${state[field.name]}` : ''}
        keyboardType={resolveInputType(field.type)}
        onChangeText={(newTextValue) =>
            handleFormValues(field.name, newTextValue)
        }
        render={(props) =><TextInputMask
                {...props}
                type={'cel-phone'}
                keyboardType="phone-pad"
                options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(99) '
                }}/>}
        />
    }
    return (
        <View>
            {fields.map((field, _index) => {
                if (field.isSubForm) {
                    return (
                        <View key={field.label}>
                            <Toggle
                                label={field.label}
                                value={!!toggle[field.name]}
                                onValueChange={(v) => {
                                    setToggle({
                                        ...toggle,
                                        [field.name]: v
                                    })
                                }}
                            />

                            {toggle[field.name] &&
                                handleSubForm([...field.subForm], field.name)}
                        </View>
                    )
                } else {
                    return field.isSelect ? (
                        <React.Fragment key={field.label} >
                            <Text style={TextStyle.baseText} key={"text".concat(field.label)}>
                                {field.label}
                            </Text>
                            <TipoFormSelect
                                key={"textI".concat(field.label)}
                                onValueChange={(newSelectedValue) => {
                                    handleFormValues(field.name, newSelectedValue)
                                }}
                                options={field.options}
                                value={state[field.name] ?? ''}
                            />
                        </React.Fragment>
                    ) : 
                     <React.Fragment key={field.label} >
                        <Text style={TextStyle.baseText} key={"text".concat(field.label)} >
                            {field.label}
                        </Text>
                        {resolveTexInput(field)}
                    </React.Fragment>
                }
            })}
            <View style={Layout.buttonsBottomWrapperScroll}>
                <View
                    style={(() => {
                        if (editMode) {
                            return {
                                ...Layout.row,
                                marginBottom: 20,
                                marginTop: 10
                            }
                        }
                        return {
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 20,
                            marginTop: 10
                        }
                    })()}
                >
                    {editMode && (
                        <Button
                            mode='contained'
                            style={Layout.buttonLeft}
                            color={colors.danger}
                            onPress={onDelete}
                        >
                            Apagar
                        </Button>
                    )}
                    <Button
                        mode='contained'
                        dark={true}
                        style={Layout.buttonRight}
                        color={colors.primary}
                        onPress={() => {
                            setformRendered(true);
                            onSave({ ...state })
                        }}
                    >
                        Salvar
                    </Button>
                </View>
            </View>
        </View>
    )
}
