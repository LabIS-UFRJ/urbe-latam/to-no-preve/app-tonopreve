import React,{useState} from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import Constants from 'expo-constants';
import i18n from 'i18n-js'

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const NovaAreaDeUso = (props) => {
    const [novoTipo,setNovoTipo] = useState("");

    const onValueChange = (value) => {
        //Outro uso; 
        setNovoTipo(value[1]);
        props.onValueChange({ tipoAreaDeUso: {  nome:value[1], id:value[0] } })
        
    };


    return ( 
        <TextInput
            label={`${i18n.t('novaAreDeUsoLabel')} ${Constants.manifest.extra.translations.goodCategorySingular}`}
            value={novoTipo}
            style={Inputs.textInput}
            labelStyle={Inputs.textInputLabel}
            onChangeText={text => onValueChange( [tipos.data.tiposAreaDeUso.length,text]) }
            clearTextOnFocus ={true}
            autoFocus={true}
        />
    )
};

export default NovaAreaDeUso;
