const pt_br = {
    NoResultsComponent_txt: "Não há tipo de comunidade com esta busca",
    communityType :"Tipo de comunidade",
    concluded : "Concluído",
    types: "tipos",
    search : "Buscar",
}   

export const TipoComunidade = {
    'pt-BR': pt_br
}