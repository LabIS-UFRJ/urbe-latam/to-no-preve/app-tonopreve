import React, {useState} from 'react'
import {MaterialIcons} from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet, View, Text} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import metrics from "../utils/metrics";
import i18n from 'i18n-js';


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoComunidadeSelect = (props) => {

    const [selectedItems, setSelectedItems] = useState(props.selectedItems);

    const onValueChange = (values) => {
        setSelectedItems(values);
        props.onValueChange(values);
    };

    const getTiposComunidade = () => {
        return tipos.data.tiposComunidade.map(tipoComunidade => ({
            ...tipoComunidade,
            name: tipoComunidade.nome,
            id: tipoComunidade.id
        }));
    };

    const NoResultsComponent = () => {
        return <View style={{textAlign: 'center', flex: 1, margin: 10}}><Text>{i18n.t('NoResultsComponent_txt')}</Text></View>
    };

    return <SectionedMultiSelect
        items={getTiposComunidade()}
        IconRenderer={MaterialIcons}
        uniqueKey="id"
        selectText= {i18n.t('communityType')}
        confirmText={i18n.t('concluded')}
        selectedText={i18n.t('types')}
        searchPlaceholderText={i18n.t('search')}
        noResultsComponent={NoResultsComponent}
        styles={{selectToggle: {marginBottom: 8}, selectToggleText: {fontSize: metrics.tenWidth*1.8}}}
        single={false}
        alwaysShowSelectText={true}
        readOnlyHeadings={false}
        onSelectedItemsChange={onValueChange}
        selectedItems={selectedItems}
    />

    /*return <TextInput
        label="Tipo de comunidade"
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getTiposComunidade()}
                value={props.selectedValue}
            />
        )}
    />*/
};

export default TipoComunidadeSelect;
