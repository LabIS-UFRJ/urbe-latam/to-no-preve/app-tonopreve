import React from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import i18n from 'i18n-js'

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoProducaoSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    const getTiposProducao = () => {
        return tipos.data.tiposProducao.map(tipoProducao => ({
            ...tipoProducao,
            label: tipoProducao.nome,
            value: tipoProducao.id
        }));
    };

    return <TextInput
        label={i18n.t('tipoProducaoSelectLabel')}
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getTiposProducao()}
                value={props.selectedValue}
            />
        )}
    />
};

export default TipoProducaoSelect;
