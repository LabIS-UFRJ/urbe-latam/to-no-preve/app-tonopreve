import React, { useEffect, useState } from 'react'
import tipos from '../bases/tipos'
import { TextInput } from 'react-native-paper'
import { StyleSheet } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { pickerStyles } from '../styles/pickers'
import { Inputs } from '../styles'
import { getTiposConflitoCache } from '../db/api'
import Constants from 'expo-constants'
import i18n from 'i18n-js'

const pickerSelectStyles = StyleSheet.create(pickerStyles)

const TipoConflitoSelect = (props) => {
    const [localTipos, setLocalTipos] = useState([])

    useEffect(() => {
        getTiposConflitoCache().then((val) => {
            setLocalTipos(val)
        })
    }, [])

    const onValueChange = (value) => {
        if (value != null) {
            const nome = getTiposConflito().filter((v) => v.value === value)[0]
                .nome

            props.onValueChange(value, nome)
        }
    }

    const getTiposConflito = () => {
        return localTipos
    }

    return (
        <TextInput
            label={`${i18n.t('tipoConflitoSelectLabel')} ${
                Constants.manifest.extra.translations.badCategorySingular
            }`}
            value={props.selectedValue}
            style={Inputs.textInput}
            labelStyle={Inputs.textInputLabel}
            render={(props2) => (
                <RNPickerSelect
                    onValueChange={onValueChange}
                    placeholder={{ label: '', value: null }}
                    useNativeAndroidPickerStyle={false}
                    style={pickerSelectStyles}
                    items={getTiposConflito()}
                    value={props.selectedValue}
                />
            )}
        />
    )
}

export default TipoConflitoSelect
