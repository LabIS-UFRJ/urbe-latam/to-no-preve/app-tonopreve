import React from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import i18n from 'i18n-js'


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const ModoDeVidaSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    const getModosDeVida = () => {
        return tipos.data.modosDeVida.map(modoDeVida => ({
                ...modoDeVida,
                label: modoDeVida.nome,
                value: modoDeVida.id
            }));
    };

    return <TextInput
        label={i18n.t('modoDeVidaLabel')}
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getModosDeVida()}
                value={props.selectedValue}
            />
        )}
    />
};

export default ModoDeVidaSelect;
