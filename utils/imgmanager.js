import { v4 as uuidv4 } from 'uuid';
//import client from "../db/api";
import { writeConflitosImgs, writeAreasDeUsoImgs, writeFormsImgs } from '../db/db'
import {updateImgAreaDeUso, updateImgConflito, updateImgForm} from "../db/queries"
import * as ImageManipulator from "expo-image-manipulator";
//const compressedImage = await imageResizer(conflito.image)
//            if (compressedImage) {
//                conflito.image =
//                    'data:image/jpeg;base64,' + compressedImage.base64
//            }



const imageResizer = async (image) => {
    return await ImageManipulator.manipulateAsync(image, [], {
        compress: 0.1,
        format: ImageManipulator.SaveFormat.JPEG,
        base64: true
    })
}

  



const imageMaker = {
    imagesToSendPot: new Map(),
    imagesToSendVul: new Map(),
    imagesToSendCenso: new Map(),
    savedImages: new Map(),
    seending: false,

    addImage: function (point, imgId){
        console.log(this)
        const id = (imgId === undefined || imgId === null) ? uuidv4(): imgId;
        const img = point.image
        if (img === undefined){
            throw new Error("Undefined image")
        }
        switch (point.__typename){
            case "AreaDeUso":
                this.imagesToSendPot.set(id, img)
                break
            case "Conflito":
                this.imagesToSendVul.set(id, img)
                break
            case "Form":
                this.imagesToSendCenso.set(id, img)
                break
            default:
                console.log(point.__typename)
                throw new Error("Undefined Point Type")
        }
        return id
        
    },

    SendImages: function(client){
        console.log(this)
        //console.log("\n")
        console.log("======================================================================================")
        console.log("Escrevendo\n")
        Promise.all([
            writeConflitosImgs(Object.fromEntries(this.imagesToSendVul)),
            writeAreasDeUsoImgs(Object.fromEntries(this.imagesToSendPot)),
            writeFormsImgs(Object.fromEntries(this.imagesToSendCenso))
        ] ).then( 
            ()=> this.__sendImages(client)
        ).catch((e)=> console.log(e));
    
    },
    RemoveOldData: function() {
        console.log("Clean");
        this.imagesToSendPot = new Map();
        this.imagesToSendVul = new Map();
        this.imagesToSendCenso = new Map();
        writeConflitosImgs({})
        writeAreasDeUsoImgs({})
        writeFormsImgs({})
    },

    __sendImages: async function (client){
        console.log("======================================================================================")
        console.log("Mandando")
        this.seending = true;
        console.log(this.imagesToSendPot,this.imagesToSendVul,this.imagesToSendCenso)
        const helperSendMut = async(arr, query,resultName) =>{
            for (const [id, img] of arr) {
                try {
                    console.log(id,img)
                    console.log("======================================================================================")
                    let base64 = await imageResizer(img)
                    base64= "data:image/jpeg;base64," + base64.base64;
                    const result = await client.mutate({
                        mutation: query,
                        variables: {uuid:id,img:base64}
                    })
                    console.log(result)
                    if(result.data[resultName]?.ok){
                        arr.delete(id)
                        //savedImages.set( id, result.data[resultName].url);
                    }else{
                        throw "Erro com o servidor"
                    }
                }
                catch (e) {
                    console.log(e)
                    return false;
                }
            }
            return true;
        }
        try{
            await Promise.all([
                helperSendMut(this.imagesToSendPot,updateImgAreaDeUso, "updateImgAreaDeUso"),
                helperSendMut(this.imagesToSendVul,updateImgConflito,"updateImgConflito" ),
                helperSendMut(this.imagesToSendCenso, updateImgForm, "updateImgCensoPreve")
            ]);
        }catch (e){
            //Não deve acontecer
            console.log(e)
            return false
        }
        this.seending = false;
        return true
    }

}



export {imageMaker as ImageMaker}