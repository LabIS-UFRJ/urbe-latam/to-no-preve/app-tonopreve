import { Asset } from "expo-asset"
import * as FileSystem from "expo-file-system"
export const tilesAddresses = [
    [
        () => Asset.loadAsync(require("./assets/tiles/10/389/579.png")),
        `${FileSystem.cacheDirectory}tiles/10/389/579.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/11/778/1158.png")),
        `${FileSystem.cacheDirectory}tiles/11/778/1158.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/12/1557/2316.png")),
        `${FileSystem.cacheDirectory}tiles/12/1557/2316.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/13/3115/4632.png")),
        `${FileSystem.cacheDirectory}tiles/13/3115/4632.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/14/6230/9264.png")),
        `${FileSystem.cacheDirectory}tiles/14/6230/9264.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/14/6230/9265.png")),
        `${FileSystem.cacheDirectory}tiles/14/6230/9265.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/14/6231/9264.png")),
        `${FileSystem.cacheDirectory}tiles/14/6231/9264.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12460/18529.png")),
        `${FileSystem.cacheDirectory}tiles/15/12460/18529.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12460/18530.png")),
        `${FileSystem.cacheDirectory}tiles/15/12460/18530.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12461/18528.png")),
        `${FileSystem.cacheDirectory}tiles/15/12461/18528.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12461/18529.png")),
        `${FileSystem.cacheDirectory}tiles/15/12461/18529.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12461/18530.png")),
        `${FileSystem.cacheDirectory}tiles/15/12461/18530.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12462/18528.png")),
        `${FileSystem.cacheDirectory}tiles/15/12462/18528.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/15/12462/18529.png")),
        `${FileSystem.cacheDirectory}tiles/15/12462/18529.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24920/37058.png")),
        `${FileSystem.cacheDirectory}tiles/16/24920/37058.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24920/37059.png")),
        `${FileSystem.cacheDirectory}tiles/16/24920/37059.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24920/37060.png")),
        `${FileSystem.cacheDirectory}tiles/16/24920/37060.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24921/37058.png")),
        `${FileSystem.cacheDirectory}tiles/16/24921/37058.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24921/37059.png")),
        `${FileSystem.cacheDirectory}tiles/16/24921/37059.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24922/37057.png")),
        `${FileSystem.cacheDirectory}tiles/16/24922/37057.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24921/37060.png")),
        `${FileSystem.cacheDirectory}tiles/16/24921/37060.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24922/37058.png")),
        `${FileSystem.cacheDirectory}tiles/16/24922/37058.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24922/37059.png")),
        `${FileSystem.cacheDirectory}tiles/16/24922/37059.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24922/37060.png")),
        `${FileSystem.cacheDirectory}tiles/16/24922/37060.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24923/37058.png")),
        `${FileSystem.cacheDirectory}tiles/16/24923/37058.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24923/37057.png")),
        `${FileSystem.cacheDirectory}tiles/16/24923/37057.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24923/37059.png")),
        `${FileSystem.cacheDirectory}tiles/16/24923/37059.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24923/37060.png")),
        `${FileSystem.cacheDirectory}tiles/16/24923/37060.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24924/37057.png")),
        `${FileSystem.cacheDirectory}tiles/16/24924/37057.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/16/24924/37058.png")),
        `${FileSystem.cacheDirectory}tiles/16/24924/37058.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49841/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49841/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49842/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49842/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49843/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49843/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49844/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49844/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49845/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49845/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49846/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49846/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74118.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74118.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74119.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74119.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74120.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74120.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74121.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74121.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49848/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49848/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49848/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49848/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49848/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49848/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49849/74114.png")),
        `${FileSystem.cacheDirectory}tiles/17/49849/74114.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49848/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49848/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49849/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49849/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49849/74117.png")),
        `${FileSystem.cacheDirectory}tiles/17/49849/74117.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49849/74116.png")),
        `${FileSystem.cacheDirectory}tiles/17/49849/74116.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99682/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99682/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99683/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99683/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99684/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99684/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99685/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99685/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99686/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99686/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99687/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99687/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99688/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99688/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99690/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99690/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/17/49847/74115.png")),
        `${FileSystem.cacheDirectory}tiles/17/49847/74115.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99691/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99691/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99689/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99689/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99692/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99692/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99693/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99693/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148237.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148237.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148238.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148238.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148239.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148239.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148240.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148240.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148241.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148241.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148242.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148242.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148236.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148236.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99696/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99696/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99695/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99695/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99697/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99697/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148230.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148230.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148229.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148229.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148235.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148235.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99694/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99694/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148231.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148231.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148232.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148232.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148233.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148233.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148234.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148234.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/18/99698/148228.png")),
        `${FileSystem.cacheDirectory}tiles/18/99698/148228.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199365/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199365/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199366/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199366/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199367/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199367/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199369/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199369/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199368/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199368/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199370/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199370/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199371/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199371/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199372/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199372/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199373/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199373/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199374/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199374/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199376/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199376/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199377/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199377/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199378/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199378/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199375/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199375/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199380/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199380/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199379/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199379/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199381/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199381/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199382/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199382/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199383/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199383/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199384/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199384/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199385/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199385/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199386/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199386/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199387/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199387/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199388/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199388/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296469.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296469.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296470.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296470.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296471.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296471.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296472.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296472.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296473.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296473.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296474.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296474.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296476.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296476.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296475.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296475.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296478.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296478.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296477.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296477.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296479.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296479.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296481.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296481.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296483.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296483.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296482.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296482.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296480.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296480.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296484.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296484.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199389/296485.png")),
        `${FileSystem.cacheDirectory}tiles/19/199389/296485.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199390/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199390/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199391/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199391/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199393/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199393/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296457.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199394/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199394/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199395/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199395/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296456.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296456.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296458.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296458.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296459.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296459.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296460.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296460.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296461.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296461.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296462.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296462.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296463.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296463.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296464.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296464.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296465.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296465.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296466.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296466.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/5/12/18.png")),
        `${FileSystem.cacheDirectory}tiles/5/12/18.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/6/24/36.png")),
        `${FileSystem.cacheDirectory}tiles/6/24/36.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/7/48/72.png")),
        `${FileSystem.cacheDirectory}tiles/7/48/72.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/8/97/144.png")),
        `${FileSystem.cacheDirectory}tiles/8/97/144.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/9/194/289.png")),
        `${FileSystem.cacheDirectory}tiles/9/194/289.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296468.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296468.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199392/296467.png")),
        `${FileSystem.cacheDirectory}tiles/19/199392/296467.png`,
    ],
    [
        () => Asset.loadAsync(require("./assets/tiles/19/199396/296457.png")),
        `${FileSystem.cacheDirectory}tiles/19/199396/296457.png`,
    ],
]
