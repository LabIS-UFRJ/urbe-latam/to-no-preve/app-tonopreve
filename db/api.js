import React from 'react'
//import {ApolloClient, ApolloLink} from 'apollo-boost'
import {
    ApolloClient,
    InMemoryCache,
    ApolloLink,
    HttpLink
} from '@apollo/client'
import { onError } from '@apollo/client/link/error'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { setContext } from '@apollo/client/link/context'
import { persistCache, AsyncStorageWrapper } from 'apollo3-cache-persist'
import {
    getTerritorio,
    getListaTerritoriosData,
    SEND_USER_DATA,
    GET_USER_DATA,
    GET_SETTINGS,
    getConflitos,
    getAreasDeUso,
    GET_OTHER_CONFLITOS,
    updateImgAreaDeUso, 
    updateImgConflito,
    GET_OTHER_AREAS_DE_USO
} from './queries'
import {
    baixaThumbs,
    basename,
    renameAnexoToPermanent,
    limpaTodosAnexos
} from './arquivos'
import { ErroGeral } from './errors'
import CURRENT_USER from '../bases/current_user'
import SETTINGS from '../bases/settings'
import colors from '../assets/colors'
import STATUS from '../bases/status'
import { formataArea } from '../maps/mapsUtils'
import { pluralize, deepEqual, ehDadoLocal } from '../utils/utils'
import Constants from 'expo-constants'
import * as Notifications from 'expo-notifications'
import tipos from '../bases/tipos'
import * as ImageManipulator from 'expo-image-manipulator'
import { writeDb, readDb, writeAreasDeUso, readAreasDeUso, readAreasDeUsoImgs, readConflitosImgs, readFormsImgs } from '../db/db'
import { dedentBlockStringValue } from 'graphql/language/blockString'
import { ImageMaker } from '../utils/imgmanager'
import NetInfo from "@react-native-community/netinfo";

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
            console.log(
                `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            )
        )

    if (networkError) {
        console.log(`[Network error]: ${networkError}`);
        ImageMaker.RemoveOldData();
    }
})

export const DASHBOARD_ENDPOINT = Constants.manifest.extra.dashboardEndpoint
export const GRAPHQL_ENDPOINT = `${DASHBOARD_ENDPOINT}/graphql`
export const TOKEN_AUTH_ENDPOINT = `${DASHBOARD_ENDPOINT}/api-token-auth/`
export const ANEXO_UPLOAD_ENDPOINT = `${DASHBOARD_ENDPOINT}/anexos/`

export const LOGIN_ERROR = {
    USER_NOT_FOUND: 'USER_NOT_FOUND',
    NO_USER_AND_NO_PASSWORD: 'NO_USER_AND_NO_PASSWORD',
    NO_PASSWORD: 'NO_PASSWORD',
    MISFORMATTED_USER: 'MISFORMATTED_USER',
    NETWORK: 'NETWORK'
}

export const BRASIL_CENTRO_LATITUDE = -10.2
export const BRASIL_CENTRO_LONGITUDE = -53.12

let INITIAL_LATITUDE = BRASIL_CENTRO_LATITUDE
let INITIAL_LONGITUDE = BRASIL_CENTRO_LONGITUDE

export const tonomapaCache = new InMemoryCache({
    addTypename: false
})

const httpLink = new HttpLink({ uri: GRAPHQL_ENDPOINT })

//---------------------------------------------------------------------------//
// Auth token

let cachedToken

export const otherUsersInfo = async (currentUser) => {
    try {
        const outrosConflitos = await client.query({
            fetchPolicy: 'no-cache',
            query: GET_OTHER_CONFLITOS(currentUser.currentTerritorio.id)
        })
        const outrosAreas = await client.query({
            fetchPolicy: 'no-cache',
            query: GET_OTHER_AREAS_DE_USO(currentUser.currentTerritorio.id)
        })
        return {
            areasDeUso: outrosAreas.data.outrosAreasDeUso,
            conflitos: outrosConflitos.data.outrosConflitos
        }
    } catch (e) {
        //console.log(e)
        return false
    }
}

export const getTiposConflitoCache = async () => {
    //const conflitos = await client.query({
    //    fetchPolicy: 'cache-only',
    //    query: getConflitos
    //});
    const conflitos = await readDb()
    const tiposCache =
        conflitos.data?.currentUser?.currentTerritorio?.conflitos.map(
            (el) => el.tipoConflito
        )
    return [
        ...tipos.data.tiposConflito.map((tipoConflito) => ({
            ...tipoConflito,
            label: tipoConflito.nome,
            value: tipoConflito.id
        })),
        ...tiposCache
            .filter((elem) => elem.id >= tipos.data.tiposConflito.length)
            .sort((a, b) => {
                if (a.nome === b.nome) {
                    return 0
                } else if (a > b) {
                    return 1
                } else {
                    return -1
                }
            })
            .reduce((acc, item) => {
                if (acc.length === 0) {
                    return [item]
                }
                return acc[acc.length - 1].nome === item.nome
                    ? acc
                    : [...acc, item]
            }, [])
            .map((tipoConflito, i) => ({
                ...tipoConflito,
                id: tipos.data.tiposConflito.length + i + 1,
                label: tipoConflito.nome,
                value: tipos.data.tiposConflito.length + i + 1
            }))
    ]
}

export const getTiposAreaDeUsoCache = async () => {
    //const areas = await client.query({
    //    fetchPolicy: 'cache-only',
    //    query: getAreasDeUso
    //});
    const areas = await readDb()
    const tiposCache =
        areas.data?.currentUser?.currentTerritorio?.areasDeUso.map(
            (el) => el.tipoAreaDeUso
        )
    return [
        ...tipos.data.tiposAreaDeUso.map((tiposAreaDeUso) => ({
            ...tiposAreaDeUso,
            label: tiposAreaDeUso.nome,
            value: tiposAreaDeUso.id
        })),
        ...tiposCache
            .filter((elem) => elem.id >= tipos.data.tiposAreaDeUso.length)
            .sort((a, b) => {
                if (a.nome === b.nome) {
                    return 0
                } else if (a > b) {
                    return 1
                } else {
                    return -1
                }
            })
            .reduce((acc, item) => {
                if (acc.length === 0) {
                    return [item]
                }
                return acc[acc.length - 1].nome === item.nome
                    ? acc
                    : [...acc, item]
            }, [])
            .map((tiposAreaDeUso, i) => ({
                ...tiposAreaDeUso,
                id: tipos.data.tiposAreaDeUso.length + i + 1,
                label: tiposAreaDeUso.nome,
                value: tipos.data.tiposAreaDeUso.length + i + 1
            }))
    ]
}

export const getToken = async () => {
    if (cachedToken) {
        //console.log("have cachedToken");
        //console.log(cachedToken);

        return new Promise((resolve, reject) => {
            //console.log("entered 1st promise");
            resolve(cachedToken)
        })
    }

    return AsyncStorage.getItem('token').then((userToken) => {
        cachedToken = userToken
        return cachedToken
    })
}

const withToken = setContext(async ({ headers = {} }) => {
    // if you have a cached value, return it immediately
    const token = await getToken()

    return {
        headers: {
            ...headers,
            authorization: `JWT ${token}`
        },
        credentials: 'same-origin'
    }
})

const resetTokenOnError = onError(({ networkError }) => {
    if (
        networkError &&
        networkError.name === 'ServerError' &&
        networkError.statusCode === 401
    ) {
        // remove cached token on 401 from the server
        cachedToken = null
    }
})

const middlewareLink = withToken.concat(resetTokenOnError)
/**
 * Communicate with the server
 * @param username String
 * @param password String
 * @param onSuccess(token)
 * @param onError(errorCode, message)
 */

export const serverLogin = (username, password, onSuccess, onError) => {
    let data = new FormData()
    data.append('username', username)
    data.append('password', password)

    fetch(TOKEN_AUTH_ENDPOINT, {
        method: 'POST',
        body: data
    })
        .then((res) => {
            res.json().then((res) => {
                /*DEBUG*/ //console.log("login ok!");
                /*DEBUG*/ //console.log(res);
                if (res.token) {
                    onSuccess(res.token)
                } else {
                    let title, message, errorCode
                    if (username == '') {
                        errorCode = LOGIN_ERROR.NO_USER_AND_NO_PASSWORD
                    } else if (username.length != 11) {
                        errorCode = LOGIN_ERROR.MISFORMATTED_USER
                    } else if (password == '') {
                        errorCode = LOGIN_ERROR.NO_PASSWORD
                    } else {
                        errorCode = LOGIN_ERROR.USER_NOT_FOUND
                    }
                    console.log(errorCode)
                    onError(errorCode)
                }
            })
        })
        .catch((err) => {
            console.log(`login error ${LOGIN_ERROR.NETWORK}`)
            console.log(err)
            onError(
                LOGIN_ERROR.NETWORK,
                'Não foi possível conectar ao servidor. Favor verificar sua conexão com a internet e tentar novamente.'
            )
        })
}

//---------------------------------------------------------------------------//

const client = new ApolloClient({
    link: ApolloLink.from([errorLink, middlewareLink, httpLink]),
    cache: tonomapaCache,
    connectToDevTools: true,
    onError: (e) => {
        console.log(e)
    }
})

export const AuthContext = React.createContext()

const initialData = {}

//tonomapaCache.writeData({data: initialData});

persistCache({
    cache: tonomapaCache,
    storage: AsyncStorage
}).then(() => {
    client.onResetStore(async () =>
        tonomapaCache.writeData({ data: initialData })
    )
})

export const sair = () => {
    AsyncStorage.removeItem('token')
    client.resetStore()
    deleteCachedToken()
}

export const validaDados = (params) => {
    /*DEBUG*/ //console.log('entrou validaDados');
    /*DEBUG*/ //console.log(params);
    const DADOS_BASICOS_NAO_PREENCHIDOS = 0
    const DADOS_BASICOS_INCOMPLETOS = 1
    const DADOS_BASICOS_COMPLETOS = 2

    const { currentUser, markers, poligonos, syncedToDashboard } = params
    const result = []

    let camposPreenchidos = 0
    let statusFormularioBasico = DADOS_BASICOS_COMPLETOS
    let statusNovo = {
        poligonos: null,
        usoOuConflito: null
    }
    let statusId

    //ver se poligono está criado
    const territorio = currentUser.currentTerritorio
    if (territorio != null) {
        // Tem área demarcada?
        let temPoligonos = poligonos?.length
        if (temPoligonos) {
            const area = poligonos.reduce(
                (area, current) => area + current.area,
                0
            )
            result.push({
                icon: 'check',
                text: 'Área definida (' + formataArea(area) + ')',
                iconStyle: { color: '#FFF', backgroundColor: colors.success }
            })
        } else {
            result.push({
                icon: 'minus',
                text: 'Nenhuma área definida',
                iconStyle: { color: '#FFF', backgroundColor: colors.danger }
            })
        }
        statusNovo.poligonos = temPoligonos

        // Dados básicos estão completos?
        for (let campo in territorio) {
            switch (campo) {
                case 'id':
                case 'statusId':
                case 'criacao':
                case 'ultimaAlteracao':
                case 'tipoProducaoId':
                case 'modoDeVidaId':
                case 'publico':
                case 'conflitos':
                case 'areasDeUso':
                case 'anexos':
                case 'poligono':
                case '__typename':
                    break
                case 'anexoAta':
                    if (territorio.anexoAta.nome) {
                        camposPreenchidos++
                    } else {
                        statusFormularioBasico = DADOS_BASICOS_INCOMPLETOS
                    }
                    break
                case 'tiposComunidade':
                    if (territorio.tiposComunidade.length > 0) {
                        camposPreenchidos++
                    } else {
                        statusFormularioBasico = DADOS_BASICOS_INCOMPLETOS
                    }
                    break
                default:
                    if (!!territorio[campo]) {
                        camposPreenchidos++
                    } else {
                        statusFormularioBasico = DADOS_BASICOS_INCOMPLETOS
                    }
            }
        }
        if (camposPreenchidos === 0) {
            statusFormularioBasico = DADOS_BASICOS_NAO_PREENCHIDOS
        }

        /*DEBUG*/ //console.log('statusFormularioBasico', statusFormularioBasico);
        switch (statusFormularioBasico) {
            case DADOS_BASICOS_NAO_PREENCHIDOS:
                result.push({
                    icon: 'minus',
                    text: 'Dados básicos não preenchidos',
                    iconStyle: { color: '#FFF', backgroundColor: colors.danger }
                })
                break
            case DADOS_BASICOS_INCOMPLETOS:
                result.push({
                    icon: 'check',
                    text: 'Dados básicos incompletos',
                    iconStyle: {
                        color: '#FFF',
                        backgroundColor: colors.warning
                    }
                })
                break
            case DADOS_BASICOS_COMPLETOS:
                result.push({
                    icon: 'check',
                    text: 'Dados básicos completos',
                    iconStyle: {
                        color: '#FFF',
                        backgroundColor: colors.success
                    }
                })
                break
        }

        // Registrou locais de uso ou conflitos?
        let temUsoOuConflito = markers?.length
        result.push(
            temUsoOuConflito
                ? {
                      icon: 'check',
                      text:
                          markers.length.toString() +
                          pluralize(
                              markers.length,
                              ' local de uso ou conflito',
                              ' locais de uso e conflitos'
                          ),
                      iconStyle: {
                          color: '#FFF',
                          backgroundColor: colors.success
                      }
                  }
                : {
                      icon: 'minus',
                      text: 'Nenhum local de uso ou de conflito',
                      iconStyle: {
                          color: '#FFF',
                          backgroundColor: colors.warning
                      }
                  }
        )
        statusNovo.usoOuConflito = temUsoOuConflito

        // Anexou imagens/documentos?
        let temAnexos = territorio.anexos.length > 0
        result.push(
            temAnexos
                ? {
                      icon: 'check',
                      text:
                          territorio.anexos.length.toString() +
                          pluralize(
                              territorio.anexos.length,
                              ' arquivo ou imagem',
                              ' arquivos e imagens'
                          ),
                      iconStyle: {
                          color: '#FFF',
                          backgroundColor: colors.success
                      }
                  }
                : {
                      icon: 'minus',
                      text: 'Nenhuma imagem ou documento',
                      iconStyle: {
                          color: '#FFF',
                          backgroundColor: colors.warning
                      }
                  }
        )
        statusNovo.anexos = temAnexos

        if (!syncedToDashboard) {
            if (
                statusNovo.poligonos &&
                statusNovo.usoOuConflito &&
                statusFormularioBasico == DADOS_BASICOS_COMPLETOS
            ) {
                statusId = STATUS.PREENCHIDO
            } else if (
                !statusNovo.poligonos &&
                !statusNovo.usoOuConflito &&
                !statusNovo.anexos &&
                statusFormularioBasico == DADOS_BASICOS_NAO_PREENCHIDOS
            ) {
                statusId = STATUS.NADA_PREENCHIDO
            } else {
                statusId = STATUS.SEMIPREENCHIDO
            }
        }

        return { situacaoArray: result, statusId: statusId }
    }
}

export const setCachedToken = (token) => {
    cachedToken = token
}

export const deleteCachedToken = () => {
    cachedToken = null
}

export const setInitialCoordinates = (longitude, latitude) => {
    INITIAL_LATITUDE = latitude
    INITIAL_LONGITUDE = longitude
    /*DEBUG*/ //console.log("set Initial Coordinates "+ INITIAL_LATITUDE.toString() + " " + INITIAL_LONGITUDE.toString());
}

export const getInitialCoordinates = () => {
    /*DEBUG*/ //console.log("get Initial Coordinates "+ INITIAL_LATITUDE.toString() + " " + INITIAL_LONGITUDE.toString());
    return [INITIAL_LONGITUDE, INITIAL_LATITUDE]
}

export const carregaSettings = async () => {
    const resultSettings = await client.query({
        fetchPolicy: 'cache-only',
        query: GET_SETTINGS
    })

    if (resultSettings.data) {
        let newSettings = {
            ...SETTINGS,
            ...resultSettings.data.settings
        }
        if (resultSettings.data.settings?.appFiles) {
            newSettings.appFiles = {
                ...SETTINGS.appFiles,
                ...resultSettings.data.settings.appFiles
            }
        }
        return newSettings
    } else {
        return { SETTINGS }
    }
}
export const persisteSettings = async (newSettings) => {
    let gravaSettings = SETTINGS
    if (newSettings) {
        if (!('settings' in newSettings)) {
            const settings = await carregaSettings()
            gravaSettings = {
                ...settings,
                ...newSettings
            }
        } else {
            gravaSettings = newSettings.settings
        }
    }
    client.writeQuery({
        data: { settings: gravaSettings },
        query: GET_SETTINGS
    })
    return gravaSettings
}

export const carregaListaTerritorios = async (idUsuaria) => {
    if (!idUsuaria) {
        return null
    }
    let res = await client.query({
        fetchPolicy: 'network-only',
        query: getListaTerritoriosData(idUsuaria)
    })
    if (res.data?.territorios) {
        return res.data.territorios
    }
    console.log('Erro ao recuperar os territórios da usuária do dashboard', res)
}

export const getCurrentMensagemIndex = (currentUser) => {
    if (!currentUser?.currentTerritorio?.mensagens) {
        return -1
    }
    let currentMensagem
    let i = currentUser.currentTerritorio.mensagens.length - 1
    while (
        i >= 0 &&
        (!currentUser.currentTerritorio.mensagens[i].originIsDevice ||
            currentUser.currentTerritorio.mensagens[i].id)
    ) {
        i--
    }
    return i
}

/**
 * resultado da promise é do formato currentUser
 * @returns currentUser
 */
export const carregaCurrentUser = async (params) => {
    //const query = (params?.idTerritorio)
    //    ? getTerritorio(params.idTerritorio)
    //    : GET_USER_DATA;
    let res
    if (params?.idTerritorio) {
        const query = getTerritorio(params.idTerritorio)
        res = await client.query({
            fetchPolicy: params?.fetchPolicy
                ? params.fetchPolicy
                : 'cache-only',
            query: query
        })
    } else {
        res = await readDb()
        //console.log("Current Db\n")
        //console.log(res)
    }
    if (res.data?.currentUser) {
        const byteSize = (str) => new Blob([str]).size
        byteSize('TAMANHO DO CURENT USER', res.data?.currentUser)

        let newCurrentUser = {
            ...CURRENT_USER,
            ...res.data.currentUser
        }

        // TODO: This is for compatibility with older versions. Remove after all upgrades in devices:
        if (!newCurrentUser.currentTerritorio.anexoAta) {
            newCurrentUser.currentTerritorio.anexoAta =
                CURRENT_USER.currentTerritorio.anexoAta
        }

        // Se os dados de usuária estão sendo carregados da rede, é preciso:
        // 1. Atualizar o settings.appFiles
        // 2. Atualizar o settings.territorios
        // 3. Colocar o status como ENVIADO_PENDENTE - provisório: no futuro, o dashboard é que tem que devolver o status.
        // TODO: o status tem que vir do DASHBOARD!!!!!!
        if (params?.fetchPolicy == 'network-only') {
            newCurrentUser.currentTerritorio.statusId = STATUS.ENVIADO_PENDENTE
            await salvaCurrentUserNoCache(newCurrentUser)

            const settings = await carregaSettings()
            const newAppFiles = await baixaThumbs({
                currentUser: newCurrentUser,
                appFiles: settings.appFiles
            })
            newAppFiles.anexoAta = SETTINGS.appFiles.anexoAta
            const newTerritorios = await carregaListaTerritorios(
                newCurrentUser.id
            )
            const newSettings = {
                ...settings,
                appFiles: newAppFiles,
                territorios: newTerritorios,
                syncedToDashboard: STATUS.ENVIADO_PENDENTE
            }
            await persisteSettings({ settings: newSettings })
            return { newCurrentUser, newSettings }
        } else {
            return newCurrentUser
        }
    }
    throw new Error('Nenhum/a usuário/a encontrado/a')
}

const salvaCurrentUserNoCache = (currentUser) => {
    return writeDb(currentUser)
    /*DEBUG*/ //console.log('grava no cache', currentUser);
}

const comparaESalvaCurrentUser = async (newCurrentUser, currentUser) => {
    /*DEBUG*/ //console.log('entrou comparaESalvaCurrentUser');
    /*DEBUG*/ //console.log({newCurrentUser, currentUser});
    let dadosMudaram = false
    let newSettings = null
    if (!deepEqual(newCurrentUser, currentUser)) {
        dadosMudaram = true
        await salvaCurrentUserNoCache(newCurrentUser)
        const settings = await carregaSettings()
        if (
            settings.syncedToDashboard &&
            settings.syncedToDashboard != STATUS.TELEFONE_NAO_REGISTRADO
        ) {
            settings.syncedToDashboard = false
            newSettings = await persisteSettings({ settings: settings })
        }
    }
    const res = {
        newCurrentUser: newCurrentUser,
        dadosMudaram: dadosMudaram,
        newSettings: newSettings
    }
    /*DEBUG*/ //console.log(res);
    return res
}

export const persisteCurrentUser = async (newCurrentUser) => {
    let gravaCurrentUser
    let currentUser = null
    try {
        currentUser = await carregaCurrentUser()
        if ('currentUser' in newCurrentUser) {
            gravaCurrentUser = { ...newCurrentUser.currentUser }
        } else {
            gravaCurrentUser = {
                ...currentUser,
                ...newCurrentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    ...newCurrentUser.currentTerritorio
                }
            }
        }
    } catch (e) {
        console.log(e)
        if ('currentUser' in newCurrentUser) {
            gravaCurrentUser = { ...newCurrentUser.currentUser }
        } else {
            return {
                error: true,
                msg: 'Usuário não existe ainda na base local.'
            }
        }
    }

    const res = await comparaESalvaCurrentUser(gravaCurrentUser, currentUser)
    return res
}

export const persisteCurrentTerritorio = async (newCurrentTerritorio) => {
    try {
        const currentUser = await carregaCurrentUser()
        const gravaCurrentUser = {
            ...currentUser,
            currentTerritorio: {
                ...currentUser.currentTerritorio,
                ...newCurrentTerritorio
            }
        }
        const res = await comparaESalvaCurrentUser(
            gravaCurrentUser,
            currentUser
        )
        return res
    } catch (e) {
        console.log(e)
        return { error: true, msg: 'Usuário não existe ainda na base local.' }
    }
}

export const carregaTudoLocal = async (params) => {
    const currentUser = await carregaCurrentUser()
    const settings = await carregaSettings()
    return { currentUser: currentUser, settings: settings }
}

/*
    Atenção: esta função deve ser alterada conforme muda o objeto currentUser. Ela retorna um clone não referenciado do objeto completo (deepshallowcopy)
*/
export const cloneCurrentUser = (currentUser) => {
    const coordinates = currentUser.currentTerritorio.poligono?.coordinates
        ? currentUser.currentTerritorio.poligono.coordinates.map((obj) => [
              ...obj
          ])
        : CURRENT_USER.poligono
    return {
        ...currentUser,
        organizacoes: currentUser.organizacoes.map((obj) => ({ ...obj })),
        currentTerritorio: {
            ...currentUser.currentTerritorio,
            poligono: {
                ...currentUser.currentTerritorio.poligono,
                coordinates: coordinates
            },
            areasDeUso: currentUser.currentTerritorio.areasDeUso.map((obj) => ({
                ...obj
            })),
            conflitos: currentUser.currentTerritorio.conflitos.map((obj) => ({
                ...obj
            })),
            anexos: currentUser.currentTerritorio.anexos.map((obj) => ({
                ...obj
            })),
            mensagens: currentUser.currentTerritorio.mensagens.map((obj) => ({
                ...obj
            })),
            anexoAta: {
                ...currentUser.currentTerritorio.anexoAta
            }
        }
    }
}

/*
    Atenção: esta função deve ser alterada conforme muda o objeto currentUser. Ela retorna um clone não referenciado do objeto completo (deepshallowcopy)
*/
export const cloneAppFiles = (appFiles) => {
    return {
        ...appFiles,
        anexos: {
            ...appFiles.anexos
        },
        anexoAta: {
            ...appFiles.anexoAta
        }
    }
}


const preparaDadosParaEnviar = async (currentUser) => {
    let newCurrentUser = cloneCurrentUser(currentUser)
    delete newCurrentUser.__typename
    delete newCurrentUser.codigoUsoCelular

    const newCensosPreveList =
        newCurrentUser.currentTerritorio.censosPreve?.reduce((res, curr) => {
            delete curr.__typename
            res.push(curr)
            return res
        }, []) || []

    newCurrentUser.currentTerritorio.censosPreve = [...newCensosPreveList]

    // if (ehDadoLocal(newCurrentUser.currentTerritorio.id)) {
    //     newCurrentUser.currentTerritorio.id = null;
    // }
    delete newCurrentUser.currentTerritorio.__typename
    delete newCurrentUser.currentTerritorio.anexoAta.__typename
    for (let conflito of newCurrentUser.currentTerritorio.conflitos) {
        if (conflito.image?.includes('file')  && ehDadoLocal(conflito)) {
            conflito.imgId = ImageMaker.addImage(conflito, conflito.imgId)
        } 
        delete conflito.image
        delete conflito.__typename
        delete conflito.tipoConflito.id
        //delete conflito.tipoConflito.__typename
        if (ehDadoLocal(conflito)) {
            conflito.id = null
        }
    }
    for (let areaDeUso of newCurrentUser.currentTerritorio.areasDeUso) {
        if (areaDeUso.image?.includes('file') && ehDadoLocal(areaDeUso)) {
            areaDeUso.imgId = ImageMaker.addImage(areaDeUso, areaDeUso.imgId)
            console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
            console.log(areaDeUso.imgId, areaDeUso.imgId.length)
        }
        delete areaDeUso.image
        delete areaDeUso.__typename
        delete areaDeUso.tipoAreaDeUso.id
        if (ehDadoLocal(areaDeUso)) {
            areaDeUso.id = null
        }
    }

    for (let form of newCurrentUser.currentTerritorio.censosPreve) {
        form.__typename = "Form";
        if (form.image?.includes('file') && ehDadoLocal(form)) {
            form.imgId = ImageMaker.addImage(form, form.imgId)
        } 
        delete form.image
        delete form.__typename
        if (ehDadoLocal(form)) {
            form.id = null
        }
    }

    for (let anexo of newCurrentUser.currentTerritorio.anexos) {
        delete anexo.__typename
        if (ehDadoLocal(anexo)) {
            anexo.id = null
        }
    }
    for (let organizacao of newCurrentUser.organizacoes) {
        delete organizacao.__typename
    }
    for (let mensagem of newCurrentUser.currentTerritorio.mensagens) {
        delete mensagem.__typename
        if (mensagem.originIsDevice && !mensagem.id) {
            mensagem.dataEnvio = new Date()
        }
    }
    for (let tipoComunidade of newCurrentUser.currentTerritorio
        .tiposComunidade) {
        delete tipoComunidade.__typename
    }

    const { data } = await Notifications.getExpoPushTokenAsync()
    newCurrentUser.pushToken = data

    newCurrentUser.currentTerritorio.statusId = STATUS.ENVIADO_PENDENTE

    //client.clearStore();

    newCurrentUser.currentTerritorio.censosPreve.reduce((res, curr) => {
        console.log(`CENSO PREVE ${res}`, curr)

        return (res += 1)
    }, 0)

    return newCurrentUser
}

const sendAnexo = async (params) => {
    if (!params || !params.anexo || !params.fileUri) {
        return {}
    }
    //console.log('sendAnexo', params);
    const anexo = params.anexo
    const fileUri = params.fileUri
    var formData = new FormData()
    if (params.id && anexo.id) {
        formData.append('id', parseInt(anexo.id))
    }
    formData.append('arquivo', {
        uri: fileUri,
        name: basename({ path: fileUri }),
        type: anexo.mimetype
    })
    formData.append('nome', anexo.nome)
    formData.append('descricao', anexo.descricao ? anexo.descricao : '')
    if (params.territorioId) {
        formData.append('territorio', params.territorioId)
    }
    //console.log('sendAnexo');
    const token = await getToken()

    const firstRes = await fetch(ANEXO_UPLOAD_ENDPOINT, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: 'JWT ' + token
        },
        body: formData
    })
    //console.log(firstRes);
    if (firstRes.ok) {
        const newAnexo = await firstRes.json()
        return {
            ok: true,
            newAnexo: newAnexo,
            msg: 'anexo ' + anexo.nome + ' enviado com sucesso.'
        }
    } else {
        return {
            ok: false,
            msg:
                'Erro enviando o anexo ' +
                anexo.nome +
                ' (id: ' +
                anexo.id +
                ')'
        }
    }
}
/**
 * Envia todos os anexos novos
 * @returns {Promise<unknown[]>}
 */
const sendNewAnexos = async (currentUser, appFiles, territorioId) => {
    /*DEBUG*/ //console.log('entrou sendNewanexos');
    /*DEBUG*/ //console.log({currentUser, appFiles, territorioId});
    let erros = []
    const clonedCurrentUser = cloneCurrentUser(currentUser)
    let newAnexos = clonedCurrentUser.currentTerritorio.anexos
    const newAppFiles = cloneAppFiles(appFiles)
    for (let i in newAnexos) {
        const anexo = newAnexos[i]
        /*DEBUG*/ //console.log('Loop dos anexos do currentUser. i = ' + i, anexo);
        if (!anexo.arquivo && appFiles.anexos[anexo.id]) {
            const res = await sendAnexo({
                anexo: anexo,
                fileUri: appFiles.anexos[anexo.id].file,
                territorioId: territorioId
            })
            /*DEBUG*/ //console.log('resposta do sendAnexo', res);
            if (res.ok) {
                newAnexos[i] = {
                    ...anexo,
                    id: res.newAnexo.id.toString(),
                    arquivo: res.newAnexo.arquivo,
                    criacao: res.newAnexo.criacao,
                    thumb: res.newAnexo.thumb ? res.newAnexo.thumb : null
                }
                /*DEBUG*/ //console.log('newAnexos[i]', newAnexos[i]);
                newAppFiles.anexos[res.newAnexo.id] = {
                    ...newAppFiles.anexos[anexo.id]
                }
                delete newAppFiles.anexos[anexo.id]
            } else {
                console.log(`[erro ao enviar anexo] ${res.msg}`)
                erros.push(
                    '[código 3] Erro ao enviar anexos :( Por favor, tire print desta tela e envie para a equipe que administra o app.'
                )
            }
        } else if (!anexo.arquivo) {
            erros.push(
                'Envio de anexos: inconsistência de dados no arquivo ' +
                    anexo.id +
                    '. Removido do currentUser.'
            )
            newAnexos.splice(i, 1)
        }
    }

    let newAnexoAta = clonedCurrentUser.currentTerritorio.anexoAta
    if (appFiles.anexoAta.local && appFiles.anexoAta.file) {
        const res = await sendAnexo({
            anexo: newAnexoAta,
            fileUri: appFiles.anexoAta.file,
            territorioId: territorioId,
            id: true
        })
        /*DEBUG*/ //console.log('resposta do sendAnexo', res);
        if (res.ok) {
            newAnexoAta = {
                ...clonedCurrentUser.currentTerritorio.anexoAta,
                id: res.newAnexo.id.toString(),
                arquivo: res.newAnexo.arquivo,
                criacao: res.newAnexo.criacao,
                thumb: res.newAnexo.thumb ? res.newAnexo.thumb : null
            }
            newAppFiles.anexoAta.local = false
            /*DEBUG*/ //console.log('newAnexoAta', newAnexoAta);
        } else {
            console.log(`[erro ao enviar anexo] ${res.msg}`)
            erros.push(
                '[código 4] Erro ao enviar anexos :( Por favor, tire print desta tela e envie para a equipe que administra o app.'
            )
        }
    }

    return { newAnexos, newAppFiles, newAnexoAta, erros }
}

/**
 * Renomeia os anexos para o seu nome permanente, já de posse de id de usuária e de território
 * @returns appFiles
 */
export const renameAppFiles = async (currentUser, appFiles) => {
    // Se o usuário está mandando dados pela primeira vez, só agora recebemos o id de usuário e de território. Neste caso, os anexos precisam ser renomeados para seu nome definitivo:
    // "idUsuario_idTerritorio_HoraAtualEmMilisegundos.ext"
    //console.log('renameAppFiles');
    let renamedAppFiles = cloneAppFiles(appFiles)
    for (let anexoId in appFiles.anexos) {
        if (appFiles.anexos[anexoId].file) {
            const { newFileUri, newThumbUri } = await renameAnexoToPermanent(
                appFiles.anexos[anexoId].file,
                appFiles.anexos[anexoId].thumb,
                currentUser
            )
            renamedAppFiles.anexos[anexoId] = {
                file: newFileUri,
                thumb: newThumbUri
            }
        } else {
            // remove rubish: a local appFiles without file must be some error:
            //console.log('Removed anexoId = ' + anexoId);
            delete renamedAppFiles.anexos[anexoId]
        }
    }

    if (appFiles.anexoAta.file) {
        const { newFileUri, newThumbUri } = await renameAnexoToPermanent(
            appFiles.anexoAta.file,
            appFiles.anexoAta.thumb,
            currentUser
        )
        renamedAppFiles.anexoAta = {
            file: newFileUri,
            thumb: newThumbUri
        }
    }

    //console.log('renameAppFiles - fim', renamedAppFiles);

    return renamedAppFiles
}

/**
 * Envia dados (currentUser e anexos) para o servidor
 * @returns {Promise<*>}: Promise que resolve com um array: [currentUser, appFiles]
 */
export const enviaParaDashboard = async (currentUser, appFiles) => {
    let enviaParaDashboardErros = []
    //console.log('CURRENT USER BEFORE ', currentUser)

    // Envia os dados do currentUser ao Dashboard (mutation)
    // OBS1: Se a pessoa nunca mandou, é feito o signup e retornam os ids de usuária e do territorio
    // OBS2: Os dados do mutate voltam sem os anexos! Pois o dashboard só alimenta os anexos via envio dos arquivos, um a um (sendNewAnexos).
    const userData = await preparaDadosParaEnviar(currentUser)

    /*DEBUG*/ //console.log({currentUser, userData, appFiles});
    const result = await client.mutate({
        mutation: SEND_USER_DATA,

        variables: {
            currentUser: userData
        }
    })

    //console.log("AFTER MUTATE ", result)

    let newCurrentUser, newAppFiles, newTerritorios
    /*DEBUG*/ //console.log('Mutate result:', result);

    if (result.data?.updateAppData?.success) {
        newCurrentUser = cloneCurrentUser(result.data.updateAppData.currentUser)
        //console.log('AAAAAA', result.data.updateAppData.currentUser)
        /*DEBUG*/ //console.log('Mutate result - newCurrentUser:', newCurrentUser);

        // Atualização da lista de territórios:
        newTerritorios = await carregaListaTerritorios(newCurrentUser.id)

        // Envio e tratamento dos anexos, se houver:
        if (
            appFiles.anexoAta.local ||
            currentUser.currentTerritorio.anexos.length > 0
        ) {
            // Primeira vez que a usuária está mandando ao Dashboard:
            if (
                currentUser.id == null ||
                currentUser.currentTerritorio.id == null
            ) {
                appFiles = await renameAppFiles(newCurrentUser, appFiles)
            }

            // Envio dos anexos ao Dashboard:
            const sendNewAnexosRes = await sendNewAnexos(
                currentUser,
                appFiles,
                newCurrentUser.currentTerritorio.id
            )
            enviaParaDashboardErros = [
                ...enviaParaDashboardErros,
                sendNewAnexosRes.erros
            ]

            /*DEBUG*/ //console.log({sendNewAnexosRes});

            newCurrentUser.currentTerritorio.anexos = sendNewAnexosRes.newAnexos
            newCurrentUser.currentTerritorio.anexoAta =
                sendNewAnexosRes.newAnexoAta
            newAppFiles = sendNewAnexosRes.newAppFiles
        } else {
            newCurrentUser.currentTerritorio.anexoAta =
                CURRENT_USER.currentTerritorio.anexoAta
            newAppFiles = await limpaTodosAnexos()
        }

        // newCurrentUser.currentTerritorio.statusId = STATUS.ENVIADO_PENDENTE;

        // Transação com a internet encerrada.
        // Agora, é persistir localmente o resultado:
        /*DEBUG*/ //console.log('Persiste CurrentUser Local', newCurrentUser);
        ImageMaker.SendImages(client);
        const resPersisteCurrentUser = await persisteCurrentUser(newCurrentUser)
        /*DEBUG*/ //console.log({resPersisteCurrentUser});

        // Persiste settings localmente: o appFiles e o status syncedToDashboard=ENVIADO_PENDENTE
        //console.log('Persiste Settings Local');
        const resPersisteSettings = await persisteSettings({
            appFiles: newAppFiles,
            territorios: newTerritorios,
            syncedToDashboard: STATUS.ENVIADO_PENDENTE
        })
        /*DEBUG*/ //console.log({currentUser: resPersisteCurrentUser.newCurrentUser, resPersisteSettings});
        return {
            currentUser: resPersisteCurrentUser.newCurrentUser,
            settings: resPersisteSettings,
            token: result.data.updateAppData.token,
            erros: enviaParaDashboardErros.join(', ')
        }
    } else {
        console.log('Erro gravando dados!', result.data.updateAppData.errors)
        return { erros: result.data.updateAppData.errors.join(', ') }
    }
}

let preparingToSend = true

const unsubscribe = NetInfo.addEventListener(state => {
    if (state.isConnected && ImageMaker.seending && preparingToSend){
        preparingToSend = false
        Promise.all([
            readAreasDeUsoImgs(),
            readConflitosImgs(),
            readFormsImgs()
        ] ).then( 
            (values)=> {
                const [imagesToSendPot, imagesToSendVul] = values;
                ImageMaker.imagesToSendPot = (imagesToSendPot === null || imagesToSendPot === undefined) ? new Map() : new Map(Object.entries(imagesToSendPot));
                ImageMaker.imagesToSendVul = (imagesToSendVul === null|| imagesToSendVul === undefined) ? new Map() : new Map(Object.entries(imagesToSendVul));
                ImageMaker.__sendImages(client).then(() => preparingToSend = true);
            }
        ).catch((e)=> console.log(e));
    
    }
});

export default client
