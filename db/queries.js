import gql from 'graphql-tag'


export const updateImgAreaDeUso = gql`
    mutation updateImgAreaDeUso( $uuid: ID!, $img: String!) {
        updateImgAreaDeUso(uuid:$uuid, img:$img){
            ok
            url
        }
    }
`

export const updateImgConflito = gql`
    mutation updateImgConflito( $uuid: ID!, $img: String!) {
        updateImgConflito(uuid:$uuid, img:$img){
            ok
            url
        }
    }
`

export const updateImgForm = gql`
    mutation updateImgCensoPreve( $uuid: ID!, $img: String!) {
        updateImgCensoPreve(uuid:$uuid, img:$img){
            ok
            url
        }
    }
`


export const getConflitos = gql`
    {
        currentUser {
            __typename
            currentTerritorio {
                __typename
                conflitos {
                    __typename
                    id
                    nome
                    image
                    tipoConflito {
                        id
                        nome
                    }
                }
            }
        }
    }
`

export const getAreasDeUso = gql`
    {
        currentUser {
            __typename
            currentTerritorio {
                __typename
                areasDeUso {
                    __typename
                    id
                    nome
                    image
                    tipoAreaDeUso {
                        id
                        nome
                    }
                }
            }
        }
    }
`

/* REMOTE QUERIES */

const LISTA_TERRITORIOS_FIELDS = `
    id,
    nome,
    municipioReferenciaId,
`

export const getListaTerritoriosData = (idUsuaria) => gql`
    {
        territorios(idUsuaria:${idUsuaria}) {
            ${LISTA_TERRITORIOS_FIELDS}
        }
    }
`

const USUARIA_FIELDS = `
    __typename,
    id,
    username,
    fullName,
    codigoUsoCelular,
    cpf,
    organizacoes {
        __typename,
        nome
    },
`

const TERRITORIO_FIELDS = `
    __typename,
    id,
    nome,
    anoFundacao,
    qtdeFamilias,
    statusId,
    tiposComunidade {
        __typename,
        id
    },
    tipoProducaoId,
    modoDeVidaId,
    municipioReferenciaId,
    anexoAta {
        __typename,
        id,
        nome,
        descricao,
        mimetype,
        fileSize,
        arquivo,
        thumb,
        criacao
    },
    poligono,
    conflitos {
        __typename,
        id,
        nome,
        posicao,
        image,
        descricao,
        tipoConflitoId,
        tipoConflito{
            id,
            nome
        }
        
    },
    areasDeUso {
        __typename,
        id,
        nome,
        posicao,
        image,
        descricao,
        tipoAreaDeUsoId,
        tipoAreaDeUso{
            id,
            nome
        }
    },
    censosPreve {
        id,
        posicao,
        image,
        entrevistador,
        setor,
        endereco,
        telefone,
        numeroCasa,
        numMen18,
        num1855,
        num55Mais,
        numNe,
        numPets,
        matPredom,
        sustentacao,
        conserv,
        matTelhado,
        matCobre,
        idadeCasa,
        ultRef,
        numPisos,
        pisoMorador,
        matAndMora,
        evento {
            id,
            tipo,
            data,
            impacto,
            reparo
        }
    },
    anexos {
        __typename,
        id,
        nome,
        descricao,
        mimetype,
        fileSize,
        arquivo,
        thumb,
        criacao
    },
    mensagens {
        __typename,
        id,
        texto,
        extra,
        dataEnvio,
        dataRecebimento,
        originIsDevice
    },
    publico
`

export const getTerritorio = (idTerritorio) => gql`
    {
        currentUser {
            ${USUARIA_FIELDS},
            currentTerritorio(id:${idTerritorio}) {
                ${TERRITORIO_FIELDS}
            }
        }
    }
`

export const GET_USER_DATA_QUERY = `
    currentUser {
        ${USUARIA_FIELDS},
        currentTerritorio {
            ${TERRITORIO_FIELDS}
        }
    }
`

export const GET_USER_DATA = gql`{ ${GET_USER_DATA_QUERY} }`

// export const GET_MENSAGENS_QUERY = `
//     mensagens {
//         __typename
//         id,
//         remetente { id, fullName },
//         destinatario { id, fullName },
//         texto,
//         extra,
//         dataEnvio,
//         dataRecebimento
//    }
// `;

// export const GET_USER_DATA_COM_MENSAGENS = gql`{ ${GET_USER_DATA_QUERY}, ${GET_MENSAGENS_QUERY} }`;

// export const GET_MENSAGENS = gql`{ ${GET_MENSAGENS_QUERY} }`;

export const SEND_USER_DATA = gql`
    mutation updateAppData($currentUser: UsuariaInput){
        updateAppData (input: {currentUser: $currentUser}) {
            token,
            errors,
            success,
            currentUser {
                ${USUARIA_FIELDS},
                currentTerritorio {
                    ${TERRITORIO_FIELDS}
                }
            }
        }
    }
`

const GET_SETTINGS_QUERY = `
    settings {
        appIntro,
        syncedToDashboard,
        useGPS,
        offlineMap,
        mapType,
        editaPoligonos,
        appFiles,
        editing,
        territorios,
        unidadeArea
    }
`

export const GET_OTHER_CONFLITOS = (territorioId) => gql`{
    outrosConflitos(idTerritorio: ${territorioId}){
        __typename,
        id,
        nome,
        posicao,
        image,
        descricao,
        tipoConflitoId,
        tipoConflito{
            id,
            nome
        }
    }
  }`

export const GET_OTHER_AREAS_DE_USO = (territorioId) => gql`{
    outrosAreasDeUso(idTerritorio: ${territorioId}){
        __typename,
        id,
        nome,
        posicao,
        image,
        descricao,
        tipoAreaDeUsoId,
        tipoAreaDeUso{
            id,
            nome
        }
    }
  }`

export const GET_SETTINGS = gql`{ ${GET_SETTINGS_QUERY} }`
