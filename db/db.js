import AsyncStorage from '@react-native-async-storage/async-storage';


let __curent_user = {value:""};
let __areasDeUso = {value:""};
let __conflitos = {value:""};
let __conflitosImg = {value:""};
let __areasDeUsoImg = {value:""};
let __formsImg = {value:""}
const write = async (data, key,  obj) => {
    try {
        const jsonValue = JSON.stringify(data);
        obj.value = jsonValue; 
        await AsyncStorage.setItem(key, jsonValue);
      } catch (e) {
        console.log(e);
    }
    return null
}
const read = async(key ,obj ) => {
    if(obj.value == ""){
        try {
            const jsonValue = await AsyncStorage.getItem(key);
            if(jsonValue === null){
                return {}
            }
            obj.value = jsonValue
            return JSON.parse(jsonValue);
        } catch(e) {
            console.log(e)
            return null
        }
    }
    return JSON.parse(obj.value);
}
const writeDb = (currentUser) => write({data: { currentUser : currentUser}},'@current_user',__curent_user);
const readDb = () => read("@current_user",__curent_user);
const readAreasDeUso = () => read("@areasDeUso", __areasDeUso);
const writeAreasDeUso = (data ) => write(data,"@areasDeUso",__areasDeUso);
const readConflitos =  () => read("@conflitos", __conflitos);
const writeConflitos = (data) => write(data,"@conflitos", __conflitos)

const readConflitosImgs =  () => read("@conflitosImgs", __conflitosImg);
const writeConflitosImgs = (data) => write( data, "@conflitosImgs", __conflitosImg);
const readAreasDeUsoImgs =  () => read("@areasDeUsoImgs", __areasDeUsoImg);
const writeAreasDeUsoImgs = (data) => write( data, "@areasDeUsoImgs", __areasDeUsoImg);
const readFormsImgs = () => read("@formsImgs", __formsImg);
const writeFormsImgs = (data) => write( data, "@formsImgs", __formsImg);



export {
    writeDb,readDb, readAreasDeUso , writeAreasDeUso , readConflitos , writeConflitos, readConflitosImgs , 
        writeConflitosImgs, readAreasDeUsoImgs, writeAreasDeUsoImgs,readFormsImgs,writeFormsImgs
};


//export const writeDb = temp[0];
//export const readDb = temp[1];
//export const readAreasDeUso = temp[2];
//export const writeAreasDeUso = temp[3];
//export const readConflitos = temp[4];
//export const writeConflitos = temp [5];
//export const readConflitosImgs = temp [6];
//export const writeConflitosImgs = temp [7];
//export const readAreasDeUsoImgs = temp [8];
//export const writeAreasDeUsoImgs = temp [9];