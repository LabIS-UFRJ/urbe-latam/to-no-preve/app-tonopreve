import { translationsMapaScreen } from "./screens/locales/MapaScreen.locale"
import { dadosBasicosScreen } from "./screens/locales/DadosBasicosScreen.locale"
import { translationsEstadoSelect } from "./components/locales/EstadoSelect.locale"
import { anexoListaScreen } from "./screens/locales/AnexoListaScreen.locale"
import { configurationScreen } from "./screens/locales/ConfigurationScreen.locale"
import { anexaFormScreen } from "./screens/locales/AnexaFormScreen.locale"
import { editarTiposScreen } from "./screens/locales/EditarTiposScreen.locale"
import { loginScreen } from "./screens/locales/LoginScreen.locale"
import { mainScreen } from "./screens/locales/MainScreen.locale"
import { mensagensScreen } from './screens/locales/MensagensScreen.locale'
import { preRequisitosScreen } from './screens/locales/PreRequisitosScreen.locale'
import { sobreScreen } from './screens/locales/SobreScreen.locale'
import { TermosDeUsoScreen } from "./screens/locales/TermosDeUsoScreen.locale"
import { TipoComunidade } from "./components/locales/TipoComunidade.locale"
import { MunicipioSelect } from "./components/locales/MunicipioSelect.locale"
import { modoDeVida } from './components/locales/ModoDeVidaSelect.locale'
import { novaAreDeUso } from './components/locales/NovaAreaDeUso.locale'
import { novoTipoConflito } from './components/locales/NovoTipoConflito.locale'
import { tipoAreaDeUsoSelect } from './components/locales/TipoAreaDeUsoSelect.locale'
import { tipoConflitoSelect } from './components/locales/TipoConflitoSelect.locale'
import { tipoProducaoSelect } from './components/locales/TipoProducaoSelect.locale'
import {CadastroScreen} from "./screens/locales/CadastroScreen.locale"

export const Translations = {
  "pt-BR": {
    ...TermosDeUsoScreen["pt-BR"],
    ...anexoListaScreen["pt-BR"],
    ...configurationScreen["pt-BR"],
    ...dadosBasicosScreen["pt-BR"],
    ...translationsEstadoSelect["pt-BR"],
    ...translationsMapaScreen["pt-BR"],
    ...anexaFormScreen["pt-BR"],
    ...editarTiposScreen["pt-BR"],
    ...CadastroScreen["pt-BR"],
    ...loginScreen["pt-BR"],
    ...mainScreen["pt-BR"],
    ...mensagensScreen["pt-BR"],
    ...preRequisitosScreen["pt-BR"],
    ...sobreScreen["pt-BR"],
    ...modoDeVida["pt-BR"],
    ...novaAreDeUso["pt-BR"],
    ...novoTipoConflito["pt-BR"],
    ...tipoAreaDeUsoSelect["pt-BR"],
    ...tipoConflitoSelect["pt-BR"],
    ...MunicipioSelect["pt-BR"],
    ...TipoComunidade["pt-BR"],
    ...tipoProducaoSelect["pt-BR"]
  }
}
