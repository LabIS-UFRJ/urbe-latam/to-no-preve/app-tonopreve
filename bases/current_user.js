const CURRENT_USER = {
    __typename: 'Usuaria',
    id: null,
    username: null,
    fullName: null,
    codigoUsoCelular: null,
    cpf: null,
    organizacoes: [],
    currentTerritorio: {
        __typename: 'Territorio',
        id: null,
        nome: null,
        poligono: {
            type: 'MultiPolygon',
            coordinates: []
        },
        anoFundacao: null,
        qtdeFamilias: null,
        anexoAta: {
            __typename: 'anexoAta',
            id: null,
            nome: null,
            descricao: null,
            mimetype: null,
            fileSize: null,
            arquivo: null,
            thumb: null,
            criacao: null
        },
        statusId: null,
        tiposComunidade: [],
        tipoProducaoId: null,
        modoDeVidaId: null,
        municipioReferenciaId: null,
        conflitos: [],
        areasDeUso: [],
        anexos: [],
        mensagens: [],
        publico: false
    }
};

export default CURRENT_USER;
