const DADOS_BASICOS = {
    nome: {type: 'None'},
    anoFundacao: {type: 'Number'},
    qtdeFamilias: {type: 'Number'},
    municipioReferenciaId: {type: 'String'},
    modoDeVidaId: {type: 'None'},
    tipoProducaoId: {type: 'None'},
    tiposComunidade: {type: 'Array', __typename: 'tipoComunidade'},
    anexoAta: {type: 'None'},
    poligono: {type: 'None'}
};

export default DADOS_BASICOS;
