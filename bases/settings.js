import {MAP_TYPES} from 'react-native-maps';
import STATUS from './status';
import {UNIDADES_AREA} from '../maps/mapsUtils';

const SETTINGS = {
    appIntro: true,
    syncedToDashboard: STATUS.TELEFONE_NAO_REGISTRADO,
    useGPS: false,
    offlineMap: false,
    mapType: MAP_TYPES.HYBRID,
    editaPoligonos: true,
    appFiles: {
        anexos: {},
        anexoAta: {
            file: null,
            thumb: null,
            icone: null,
            local: null
        }
    },
    editing: null,
    territorios: {},
    unidadeArea: UNIDADES_AREA.HECTARE
};

export default SETTINGS;
