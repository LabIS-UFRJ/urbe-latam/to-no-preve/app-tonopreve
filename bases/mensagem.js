const MENSAGEM = {
    __typename: 'Mensagem',
    id: null,
    dataEnvio: null,
    dataRecebimento: null,
    originIsDevice: true,
    extra: null,
    texto: null
};

export default MENSAGEM;
