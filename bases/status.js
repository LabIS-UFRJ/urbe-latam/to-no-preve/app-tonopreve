import colors from '../assets/colors';
import tipos from './tipos';

export const STATUS = {
    NADA_PREENCHIDO: 1,
    SEMIPREENCHIDO: 2,
    ENVIADO_PENDENTE: 3,
    OK_TONOMAPA: 4,
    OK_MPF: 5,
    PREENCHIDO: 6,
    TELEFONE_NAO_REGISTRADO: 7
};
const tiposStatus = tipos.data.territorioStatus;
const pegaMensagem = (status) => {
    let mensagem = {};
    for (let i = 0; i < tiposStatus.length; i++) {
        if (tiposStatus[i].id == status) {
            mensagem = tiposStatus[i];
            break;
        }
    }
    return mensagem;
}

export const buildStatusMessage = (status) => {
    let statusMessage = pegaMensagem(status);
    switch (status) {
        case null:
        case STATUS.TELEFONE_NAO_REGISTRADO:
            statusMessage = {
                ...statusMessage,
                icon: "circle",
                color: colors.danger,
            };
            break;
        case STATUS.NADA_PREENCHIDO:
            statusMessage = {
                ...statusMessage,
                icon: "circle",
                color: colors.danger,
            };
            break;
        case STATUS.SEMIPREENCHIDO:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-2",
                color: colors.warning,
            };
            break;
        case STATUS.PREENCHIDO:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-4",
                color: colors.warning,
            };
            break;
        case STATUS.ENVIADO_PENDENTE:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-6",
                color: colors.warning,
            };
            break;
        case STATUS.OK_TONOMAPA:
            statusMessage = {
                ...statusMessage,
                icon: "circle",
                color: colors.primary,
            };
            break;
        case STATUS.OK_MPF:
            statusMessage = {
                ...statusMessage,
                icon: "checkbox-marked-circle",
                color: colors.primary,
            };
            break;
    }
    return statusMessage;
};

export default STATUS;
