import * as Alignments from "./alignments";
import colors from "../assets/colors";


export const alert = {
    ...Alignments.defaultMargin,
    alignSelf: "stretch",
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
    padding: 8,
};

export const alertPrimary = {
    ...alert,
    backgroundColor: colors.primary,
};

export const alertDanger = {
    ...alert,
    backgroundColor: colors.danger
};
