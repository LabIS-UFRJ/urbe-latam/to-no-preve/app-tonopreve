import * as Alignments from "./alignments";
import colors from '../assets/colors';
import metrics from "../utils/metrics";

export const textInputCompact = {
    backgroundColor: colors.background,
    width: '100%',
};
export const textInput = {
    ...textInputCompact,
    marginBottom: 8,
    marginTop: 8,
}
export const textInputLabel = {
    fontSize: metrics.tenWidth*1.6,
    color: colors.text
}

export const textInputLogin = {
    ...textInput,
    fontSize: metrics.tenWidth*1.8,
    backgroundColor: colors.primary
}
export const textInputLoginActive = {
    ...textInput,
    backgroundColor: colors.primary
}

export const imageInput = {
    backgroundColor: colors.background,
    width: '100%',
    paddingRight: '20%'
}

export const caption = {
    fontSize: metrics.tenWidth*1.8,
    color: colors.text,
    marginTop: 24
}

export const checkBoxes = {
    flexDirection: 'row',
    alignItems: 'center'
}
