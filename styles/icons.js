import { StyleSheet } from "react-native";

export const iconStyles = StyleSheet.create({
  camera: {
    color: "gray",
    marginRight: 15,
    paddingBottom: 5
  },
  gallery: {
    color: "gray",
  },
});
