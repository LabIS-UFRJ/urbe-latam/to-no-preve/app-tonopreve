
import metrics from "../utils/metrics";
export const pickerStyles = {
    inputIOS: {
        fontSize: metrics.tenWidth*1.6,
        paddingHorizontal: 10,
        paddingVertical: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
        marginTop: 30,
    },
    inputAndroid: {
        fontSize: metrics.tenWidth*1.6,
        paddingHorizontal: 10,
        paddingVertical: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
        marginTop: 30,
    },
};
