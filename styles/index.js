import * as Inputs from "./inputs";
import * as Alignments from "./alignments";
import * as Messages from "./messages";
import * as Layout from "./layout";
import * as LayoutMapa from "./layoutMapa";
import * as Anexos from "./anexos";
import { iconStyles as Icons } from "./icons";

export { Icons, Inputs, Alignments, Messages, Layout, LayoutMapa, Anexos };
