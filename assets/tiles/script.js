const fs = require("fs")

function getFiles(dir, files_) {
    files_ = files_ || []
    const files = fs.readdirSync(dir)
    for (const i in files) {
        const name = dir + "/" + files[i]
        if (fs.statSync(name).isDirectory()) {
            getFiles(name, files_)
        } else {
            files_.push(name)
        }
    }
    return files_
}

fs.writeFile(
    "../../tiles.js",
    'import { Asset } from "expo-asset"\nimport * as FileSystem from "expo-file-system"\nexport const tilesAddresses = [',
    (err) => {
        if (err) {
            throw err
        }
    }
)

const allFiles = getFiles(".").filter(
    (file) => file.includes(".png") && !file.includes("Zone")
)
for (let file = 0; file < allFiles.length; file++) {
    const address = allFiles[file].substring(1, allFiles[file].length)
    let targetString =
        "[() => Asset.loadAsync(require('./assets/tiles" +
        address +
        "')), `${FileSystem.cacheDirectory}tiles" +
        address +
        "`"
    if (!(file === allFiles.length - 1)) {
        targetString += "],\n"
    } else {
        targetString += "]]\n"
    }

    fs.appendFile("../../tiles.js", targetString, (err) => {
        if (err) {
            throw err
        }
    })
}

console.log("Done! :)")
