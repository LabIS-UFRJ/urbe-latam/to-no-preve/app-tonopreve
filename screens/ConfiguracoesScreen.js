import React, {useReducer, useState, useRef} from 'react';
import {TouchableHighlight, Clipboard, BackHandler, View, Alert} from "react-native";
import {RadioButton, IconButton, Button, Switch, Text, ActivityIndicator, Snackbar, Portal, Dialog} from "react-native-paper";
import {Layout} from "../styles";
import {useFocusEffect} from '@react-navigation/native';
import colors from "../assets/colors";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {carregaCurrentUser, carregaTudoLocal, persisteCurrentUser, persisteSettings} from "../db/api";
import {deepEqual} from "../utils/utils";
import {UNIDADES_AREA} from "../maps/mapsUtils";
import TELAS from "../bases/telas";
import STATUS from "../bases/status";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import metrics from "../utils/metrics";
import i18n from 'i18n-js';

let snackBarMessage = "";

export default function ConfiguracoesScreen({navigation, route}) {
    const SNACKBAR_MESSAGES = {
        CONFIGURACOES:   i18n.t("updatedConfigurations"),
        COMUNIDADE_ALTERADA: i18n.t("communityChanged"),
        CODIGO_SEGURANCA_COPIADO: i18n.t("securityCodeCopied")
    };
    const initialState = {
        settings: null,
        currentUser: null,
        indiceTerritorio: null,
        loading: false
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const [snackBarVisible, setSnackBarVisible] = useState(false);
    const [dialogListaTerritoriosVisible, setDialogListaTerritoriosVisible] = useState(false);
    const [dialogListaUnidadesVisible, setDialogListaUnidadesVisible] = useState(false);
    const [novoIndiceTerritorio, setNovoIndiceTerritorio] = useState(null);
    const dadosMudaram = useRef(false);

    if (!state.settings || route.params.atualizarDados) {
        if (route.params?.atualizarDados) {
            delete route.params.atualizarDados;
        }
        carregaTudoLocal().then((data) => {
            let newState = {};
            let mudouState = false;
            if (!deepEqual(state.settings, data.settings)) {
                newState.settings = data.settings;
                mudouState = true;
            };
            if (!deepEqual(state.currentUser, data.currentUser)) {
                let indiceTerritorio;
                for (let i in data.settings.territorios) {
                    if (data.settings.territorios[i].nome === data.currentUser.currentTerritorio.nome) {
                        indiceTerritorio = i;
                        break;
                    }
                }
                newState.currentUser = data.currentUser;
                newState.indiceTerritorio = indiceTerritorio;
                mudouState = true;
            };
            if (route.params?.dadosMudaram) {
                dadosMudaram.current = route.params.dadosMudaram;
                snackBarMessage = (route.params.snackBarMessage)
                    ? route.params.snackBarMessage
                    : SNACKBAR_MESSAGES.CONFIGURACOES;
                delete route.params.dadosMudaram;
                setSnackBarVisible(true);
            } else {
                dadosMudaram.current = false;
            }
            if (mudouState) {
                setState(newState);
            }
        });
    }

    if (route.params.editaComunidades) {
        delete route.params.editaComunidades;
        setDialogListaTerritoriosVisible(true);
    }

    const updateSettings = (params) => {
        persisteSettings(params.settings).then((newSettings) => {
            /*DEBUG*/ //console.log(params.settings, newSettings);
            setState({settings: newSettings});
            if (!params.noSnackBarMessage) {
                snackBarMessage = SNACKBAR_MESSAGES.CONFIGURACOES;
                setSnackBarVisible(true);
            }
        });
        dadosMudaram.current = true;
    }

    const exitScreen = () => {
        let navigateParams = {tela: TELAS.MAPA}
        if (dadosMudaram.current) {
            navigateParams = {
                ...navigateParams,
                atualizarDados: true
            };
        }
        
        navigation.navigate('Home', navigateParams);
        return true;
    }

    const TerritorioRadioList = () => {
        // Fix used exclusively for the case the user acts too quickly to exit configuracoesScreen:
        let territorios = [];
        for (let i in state.settings.territorios) {
            if (state.settings.territorios[i].id != null || parseInt(state.indiceTerritorio) == parseInt(i)) {
                territorios.push(state.settings.territorios[i]);
            }
        }
        // End fix.
        return (
            <>
            {state.indiceTerritorio != null
                ? <RadioButton.Group
                            value={'item' + state.indiceTerritorio}
                            onValueChange={value => {
                                if (value != state.indiceTerritorio) {
                                    setNovoIndiceTerritorio(value.replace('item',''));
                                }
                                setDialogListaTerritoriosVisible(false);
                            }}>
                    {territorios.map((territorio, indiceTerritorio) => (
                        <View style={Layout.row} key={'item' + indiceTerritorio}>
                            <Text style={Layout.text}>
                                {territorio.nome}
                            </Text>
                            <RadioButton value={'item' + indiceTerritorio} />
                        </View>
                    ))}
                </RadioButton.Group>
                : <View>
                    <Text style={Layout.textParagraph}>
                        {i18n.t("TerritorioRadioList_dataNotSend")}
                    </Text>
                </View>
            }
            </>
        );
    };

    //Android Back button
    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () =>
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [route])
    );

    /*DEBUG*/ //console.log('tonomapa: ConfiguracoesScreen', state);

    return (
        <>
        {(state.loading || !state.settings) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        {(!state.loading && state.settings) &&
            <View style={Layout.containerStretched}>
                <AppbarTonomapa navigation={navigation} title="Configurações" customGoBack={exitScreen}/>
                <View style={Layout.bodyConfiguracoes}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={200}>
                        <View style={Layout.innerBody}>
                            {state.currentUser.codigoUsoCelular &&
                                <View style={Layout.rowConfiguracoesComDesc}>
                                    <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                        Clipboard.setString(state.currentUser.codigoUsoCelular);
                                        snackBarMessage = SNACKBAR_MESSAGES.CODIGO_SEGURANCA_COPIADO;
                                        setSnackBarVisible(true);
                                    }}>
                                        <View style={Layout.row}>
                                            <Text style={Layout.text}>
                                                {i18n.t("securityCode")}
                                            </Text>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={Layout.text}>
                                                    {state.currentUser.codigoUsoCelular}
                                                </Text>
                                                <IconButton icon='content-copy' color={colors.primary} size={16}/>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                    <Text style={Layout.descConfiguracoes}>{i18n.t("securityCodeWarning")}</Text>
                                </View>
                            }
                            <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                setDialogListaTerritoriosVisible(true);
                            }}>
                                <View style={Layout.rowConfiguracoes}>
                                    <View style={Layout.rowConfiguracoesComDesc}>
                                        <Text style={Layout.text}>{i18n.t("communityBeenMapped")} </Text>
                                        <Text style={Layout.textPeq}>{state.currentUser.currentTerritorio.nome}</Text>
                                    </View>
                                    <IconButton icon="swap-horizontal-bold" size={32} color={colors.primary} />
                                </View>
                            </TouchableHighlight>

                            <View style={Layout.secaoConfiguracoes}>
                                <Text style={{fontSize: metrics.tenWidth*2.0, color: colors.primary}}>
                                {i18n.t("appBehavior")}
                                </Text>
                            </View>

                            <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                setDialogListaUnidadesVisible(true);
                            }}>
                                <View style={Layout.rowConfiguracoesComDesc}>
                                    <Text style={Layout.text}>{i18n.t("unitMeasureArea")}</Text>
                                    <Text style={Layout.textPeq}>{state.settings.unidadeArea.nome} ({state.settings.unidadeArea.sigla})</Text>
                                </View>
                            </TouchableHighlight>

                            <View style={Layout.rowConfiguracoes}>
                                <Text style={Layout.text}>{i18n.t("gpsUse")}</Text>
                                <Switch color={colors.primary} value={state.settings.useGPS} onValueChange={() => {
                                    state.settings.useGPS = !state.settings.useGPS;
                                    updateSettings({settings: {useGPS: state.settings.useGPS}});
                                }}/>
                            </View>

                            <View style={Layout.rowConfiguracoes}>
                                <Text style={Layout.text}>Mapa offline</Text>
                                <Switch color={colors.primary} value={!!state.settings.offlineMap} onValueChange={() => {
                                    state.settings.offlineMap = !state.settings.offlineMap;
                                    
                                    updateSettings({settings: { offlineMap: state.settings.offlineMap}});
                                }}/>
                            </View>

                            <View style={Layout.rowConfiguracoes}>
                                <Text style={Layout.text}>{ i18n.t("areaEdition") } </Text>
                                <Switch color={colors.primary} value={state.settings.editaPoligonos} onValueChange={() => {
                                    state.settings.editaPoligonos = !state.settings.editaPoligonos;
                                    updateSettings({settings: {editaPoligonos: state.settings.editaPoligonos}});
                                }}/>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
                <Snackbar visible={snackBarMessage && snackBarVisible == true} style={{position: 'absolute', bottom: 0}} duration={2000} onDismiss={() => {setSnackBarVisible(false)}}>
                    {snackBarMessage}
                </Snackbar>
                <Portal>
                    <Dialog visible={dialogListaTerritoriosVisible} onDismiss={() => {setDialogListaTerritoriosVisible(false)}}>
                        <Dialog.Title>{i18n.t("communitys")}</Dialog.Title>
                        <Dialog.Content>
                            <TerritorioRadioList/>
                        </Dialog.Content>
                        <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                            <Button onPress={() => {setDialogListaTerritoriosVisible(false)}}>Cancelar</Button>
                            <Button onPress={() => {
                                setDialogListaTerritoriosVisible(false);
                                if (state.settings.syncedToDashboard) {
                                    navigation.navigate('CadastroTerritorio');
                                } else {
                                    setNovoIndiceTerritorio('criarTerritorio');
                                }
                            }}> {i18n.t("addCommunity")} </Button>
                        </Dialog.Actions>
                    </Dialog>

                    <Dialog visible={dialogListaUnidadesVisible} onDismiss={() => {setDialogListaUnidadesVisible(false)}}>
                        <Dialog.Title>{i18n.t("unitMeasureArea")} </Dialog.Title>
                        <Dialog.Content>
                        <RadioButton.Group
                            value={state.settings.unidadeArea.sigla}
                            onValueChange={value => {
                                for (let unidadeArea of Object.values(UNIDADES_AREA)) {
                                    if (unidadeArea.sigla == value) {
                                        state.settings.unidadeArea = unidadeArea;
                                        updateSettings({settings: {unidadeArea: unidadeArea}});
                                        break;
                                    }
                                }
                                setDialogListaUnidadesVisible(false);
                            }}>
                            {Object.values(UNIDADES_AREA).map(unidadeArea => (
                                <View style={Layout.row} key={unidadeArea.sigla}>
                                    <Text style={Layout.text}>
                                        {unidadeArea.nome} ({unidadeArea.sigla})
                                    </Text>
                                    <RadioButton value={unidadeArea.sigla} />
                                </View>
                            ))}
                        </RadioButton.Group>
                        </Dialog.Content>
                        <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                            <Button onPress={() => {setDialogListaUnidadesVisible(false)}}>Cancelar</Button>
                        </Dialog.Actions>
                    </Dialog>

                    {novoIndiceTerritorio != null &&
                        <Dialog visible={novoIndiceTerritorio != null} onDismiss={() => {setNovoIndiceTerritorio(null)}}>
                            <Dialog.Title>{state.settings.syncedToDashboard ? i18n.t("confirmCommunityChange") : i18n.t("confirmCommunityChange") }</Dialog.Title>
                            <Dialog.Content>
                                {novoIndiceTerritorio === 'criarTerritorio' &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>{i18n.t("attention") }</Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("communityChanges__part1")}
                                            <Text style={Layout.bold}> {state.currentUser.currentTerritorio.nome} </Text>
                                            {i18n.t("communityChanges_part2")}
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>
                                                {i18n.t("createNewCommunityWarning")}
                                            </Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("createNewCommunityConnectionWarning")}
                                        </Text>
                                    </View>
                                }
                                {novoIndiceTerritorio != 'criarTerritorio' && state.settings.syncedToDashboard &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("confirmationCommunityChange")}
                                            <Text style={Layout.bold}> {state.settings.territorios[novoIndiceTerritorio].nome}</Text>
                                            ?
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("changeOnlyInternet")}
                                        </Text>
                                    </View>
                                }
                                {novoIndiceTerritorio != 'criarTerritorio' && !state.settings.syncedToDashboard &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}> {i18n.t("attention")} </Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("communityChanges__part1")}
                                            <Text style={Layout.bold}> {state.currentUser.currentTerritorio.nome} </Text>
                                            {i18n.t("communityChanges_part2")}
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>
                                                {i18n.t("changeCommunityWarning")}
                                            </Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            {i18n.t("changeCommunityConfirmation")}
                                            <Text style={Layout.bold}> {state.settings.territorios[novoIndiceTerritorio].nome}</Text>
                                            ? {i18n.t("internetRequirement")}
                                        </Text>
                                    </View>
                                }
                            </Dialog.Content>
                            <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                                <Button onPress={() => {setNovoIndiceTerritorio(null)}}> {i18n.t("cancel")} </Button>
                                <Button onPress={() => {
                                    if (novoIndiceTerritorio === 'criarTerritorio') {
                                        setNovoIndiceTerritorio(null);
                                        navigation.navigate('CadastroTerritorio');
                                    } else {
                                        const novoIdTerritorio = state.settings.territorios[novoIndiceTerritorio].id;
                                        setState({
                                            loading: true
                                        });
                                        carregaCurrentUser({
                                            fetchPolicy: 'network-only',
                                            idTerritorio: novoIdTerritorio
                                        }).then(({newCurrentUser, newSettings}) => {
                                            dadosMudaram.current = true;
                                            snackBarMessage = SNACKBAR_MESSAGES.COMUNIDADE_ALTERADA;
                                            // setSnackBarVisible(true);
                                            setState({
                                                loading: false,
                                                // currentUser: newCurrentUser,
                                                // settings: newSettings,
                                                // indiceTerritorio: novoIndiceTerritorio
                                            });
                                            setNovoIndiceTerritorio(null);
                                            navigation.navigate('Home', {tela: TELAS.MAPA, atualizarDados: true, snackBarMessage: i18n.t("youAreMapping")  + newCurrentUser.currentTerritorio.nome});
                                        }).catch(e => {
                                            console.log('erro pegando território. Internet?', e);
                                            // TODO: tratar este erro!
                                            setNovoIndiceTerritorio(null);
                                        });
                                    }
                                }}>{ i18n.t("agreeToChangeCommunity")} </Button>
                            </Dialog.Actions>
                        </Dialog>
                    }

                </Portal>
            </View>
        }
        </>
    );
}
