import React, { useEffect, useReducer, useState } from 'react'
import { View, ScrollView, BackHandler } from 'react-native'
import { Button, Text } from 'react-native-paper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Inputs, Layout, Icons } from '../styles'
import colors from '../assets/colors'
import AppbarTonomapa from '../partials/AppbarTonomapa'
import TELAS from '../bases/telas'
import { useFocusEffect } from '@react-navigation/native'
import RadioButton from 'react-native-paper/src/components/RadioButton/RadioButton'

//import appConfig from '../app.config';
import Constants from 'expo-constants'

export default function TiposDeMapeamentoScreen({ navigation, route }) {
    const initialStatae = {
        __typename: 'AreaDeUso',
        ...route.params.mapeamentoInfo
    }

    const [mapeamentoState, setMapeamentoState] = useState({ ...initialStatae })

    const redirecionaParaEdicao = (e) => {
        navigation.navigate('EditarTiposScreen', {
            ...mapeamentoState,
            vindoDe: 'TiposDeMapeamento'
        })
    }

    //Android Back button
    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                navigation.navigate('Home', {
                    tela: TELAS.MAPEAR_USOSECONFLITOS
                })
                return true
            }
            BackHandler.addEventListener('hardwareBackPress', onBackPress)
            return () =>
                BackHandler.removeEventListener(
                    'hardwareBackPress',
                    onBackPress
                )
        }, [route])
    )

    return (
        <View style={Layout.containerStretched}>
            <ScrollView>
                <AppbarTonomapa
                    navigation={navigation}
                    title={'Tipo de mapeamento'}
                    tela={TELAS.MAPEAR_USOSECONFLITOS}
                />
                <View style={Layout.body}>
                    <KeyboardAwareScrollView
                        keyboardShouldPersistTaps='handled'
                        enableOnAndroid={true}
                        extraScrollHeight={120}
                    >
                        <View style={{ ...Layout.bodyFlexEnd, paddingTop: 20 }}>
                            <RadioButton.Group
                                onValueChange={(typename) => {
                                    //setObjectState({ __typename: typename })
                                    console.log(typename)
                                    const newMapeamenoState = {
                                        ...mapeamentoState,
                                        __typename: typename
                                    }

                                    setMapeamentoState({ ...newMapeamenoState })
                                }}
                                value={mapeamentoState.__typename}
                            >
                                <View style={Inputs.checkBoxes}>
                                    <RadioButton value='AreaDeUso' />
                                    <Text>
                                        Local de{' '}
                                        {
                                            Constants.manifest.extra
                                                .translations
                                                .goodCategorySingular
                                        }
                                    </Text>
                                </View>
                                <View style={Inputs.checkBoxes}>
                                    <RadioButton value='Conflito' />
                                    <Text>
                                        Local de{' '}
                                        {
                                            Constants.manifest.extra
                                                .translations
                                                .badCategorySingular
                                        }
                                    </Text>
                                </View>
                                <View style={Inputs.checkBoxes}>
                                    <RadioButton value='Form' />
                                    <Text>
                                        Local de{' '}
                                        {
                                            Constants.manifest.extra
                                                .translations
                                                .formCategorySingular
                                        }
                                    </Text>
                                </View>
                            </RadioButton.Group>

                            <View style={Layout.buttonsBottomWrapperScroll}>
                                <View style={{ ...Layout.buttonRight }}>
                                    <Button
                                        mode='contained'
                                        style={Layout.buttonLeft}
                                        color={colors.secondary}
                                        onPress={redirecionaParaEdicao}
                                    >
                                        Avançar
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </ScrollView>
        </View>
    )
}
