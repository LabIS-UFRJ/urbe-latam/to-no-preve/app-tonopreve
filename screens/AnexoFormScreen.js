import React, {useReducer, useState, useRef} from 'react';
import {ActivityIndicator, Image, View, Alert, ScrollView, BackHandler} from "react-native";
import {Button, TextInput, Title, Text} from "react-native-paper";
import {useFocusEffect} from '@react-navigation/native';
import {Anexos, Inputs, Layout} from "../styles";
import * as DocumentPicker from 'expo-document-picker';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FontAwesome} from '@expo/vector-icons';
import Constants from 'expo-constants';
import colors from "../assets/colors";
import {pluralize, deepEqual, geraTmpId} from '../utils/utils';
import * as mime from "react-native-mime-types";
import {basename, isImage, isVideo, geraThumbDeVideo, iconeDoMimeType, formataTamanhoArquivo, apagaArquivos, fazAnexoLocalUri, fazNomeAnexo, persisteAnexo} from "../db/arquivos";
import i18n from 'i18n-js';

export default function AnexoFormScreen({route, navigation}) {
    const maxFileSize = Constants.manifest.extra.maxFileSize;

    const newAnexo = {
        __typename: 'Anexo',
        id: null,
        nome: null,
        descricao: null,
        mimetype: null,
        arquivo: null,
        thumb: null,
        criacao: null,
    };

    const initialLocalState = {
        currenUser: null,
        newAnexo: {...newAnexo},
        editing: false,
        file: null,
        fileRemoteUri: null,
        thumb: null,
        currentAnexo: null,
        icone: null,
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialLocalState);

    const [carregando, setCarregando] = useState(false);

    const exitScreen = () => {
        navigation.goBack();
        return true;
    };

    if (route.params.currentAnexo) {
        const newCurrentAnexo = {...route.params.currentAnexo};
        delete route.params.currentAnexo;
        const newState = {
            editing: true,
            currentAnexo: newCurrentAnexo,
            newAnexo: newCurrentAnexo,
            currentUser: route.params.currentUser,
            file: route.params.file,
            fileRemoteUri: route.params.fileRemoteUri,
            thumb: route.params.thumb,
            icone: iconeDoMimeType(newCurrentAnexo.mimetype, 'awesome'),
        };
        setState(newState);
    } else if (route.params.newAnexo) {
        delete route.params.newAnexo;
        const newId = geraTmpId();
        setState({
            editing: false,
            newAnexo: {
                ...newAnexo,
                id: newId
            },
            currentUser: route.params.currentUser,
            file: null,
            fileRemoteUri: null,
            thumb: null,
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            //Handle Android Back button:
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
            };
        }, [route])
    );

    const validaDados = () => {
        const errors = [];

        if (!state.newAnexo.nome || 0 === state.newAnexo.nome.trim().length) {
            errors.push([i18n.t("validaDadosError2part1"), i18n.t("validaDadosError2part2")]);
        }

        // Descrição não pode ser obrigatória!
        /*if (!state.descricao || 0 === state.descricao.trim().length) {
            errors.push(['descricao', '· Descrição do arquivo']);
        }*/

        if (!state.file && !state.thumb) {
            errors.push([i18n.t("validaDadosError4part1"), i18n.t("validaDadosError4part2")]);
        }

        if (state.newAnexo.fileSize > maxFileSize) {
            errors.push([i18n.t("validaDadosError3part1"), i18n.t("validaDadosError3part2") + formataTamanhoArquivo(maxFileSize) +  i18n.t("validaDadosError3part3")]);
        }

        if (errors.length > 0) {
            let message =  i18n.t("validaDadosMessage1");
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                i18n.t("validaDadosAlert") ,
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
            return false;
        }
        return true;
    };

    const getAnexo = async () => {
        const result = await DocumentPicker.getDocumentAsync({
            multiple: true,
            copyToCacheDirectory: false
        });

        if (result.type === 'success') {
            setCarregando(true);
            await apagaArquivos([state.file, state.thumb]);
            const res = await persisteAnexo({
                from: result.uri,
                currentUser: state.currentUser,
                filename: result.name
            });

            if (res.destinationUri) {
                const mimetype = mime.lookup(result.name);

                //TODO Generate image thumb locally. Now it will be the image itself
                let newState = {
                    newAnexo: {
                        ...state.newAnexo,
                        mimetype: mimetype,
                        fileSize: result.size,
                        arquivo: null,
                        thumb: null
                    },
                    file: res.destinationUri,
                    thumb: null,
                    icone: iconeDoMimeType(mimetype, 'awesome')
                };

                if (isVideo(mimetype)) {
                    const newThumbName = fazNomeAnexo(state.currentUser, basename({path: result.name, ext: 'jpg'}));
                    const newThumbUri = fazAnexoLocalUri(newThumbName, 'thumb');
                    const resThumbDeVideo = await geraThumbDeVideo(res.destinationUri, newThumbUri);
                    if (resThumbDeVideo.ok) {
                        newState.thumb = newThumbUri;
                    }
                }

                /*DEBUG*/ //console.log(newState);
                setState(newState);
            }
            setCarregando(false);
        } else {
            Alert.alert(
                i18n.t("getAnexoValidaDadosAlertPart1"),
                i18n.t("getAnexoValidaDadosAlertPart2"),
                [
                    {
                        text: 'Ok', onPress: () => {
                        }
                    }
                ],
                {cancelable: true},
            );
        }
    };

    const salvarFn = () => {
        if (validaDados()) {
            if (
                !deepEqual(state.newAnexo, state.currentAnexo) ||
                !('file' in route.params) ||
                state.file != route.params.file
            ) {
                navigation.navigate('AnexoList', {
                    newAnexo: state.newAnexo,
                    file: state.file,
                    thumb: state.thumb,
                });
            } else {
                navigation.goBack();
            }
        }
    };

    const apagarFn = () => {
        Alert.alert(
            i18n.t("apagarFnAnexoValidaDadosAlertPart1"),
            i18n.t("apagarFnAnexoValidaDadosAlertPart2"),
            [
                {
                    text: i18n.t("apagarFnAnexoValidaDadosAlertPart3"), onPress: () => {
                    }
                },
                {
                    text: i18n.t("apagarFnAnexoValidaDadosAlertPart4"), onPress: () => {
                        navigation.navigate('AnexoList', {apagar: state.newAnexo.id});
                    }
                },
            ],
            {cancelable: true},
        );
    };

    let thumbUri = null;
    const ehVideo = isVideo(state.newAnexo.mimetype);
    const ehImagem = isImage(state.newAnexo.mimetype);
    if (ehImagem || ehVideo) {
        thumbUri = (state.thumb) ? state.thumb : state.file;
    }

    return (
        <>
        {state.newAnexo &&
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={state.editing ? i18n.t("returnAnexaFormScreenPart1") : i18n.t("returnAnexaFormScreenPart2")} customGoBack={exitScreen}/>
            <View style={Layout.bodyFlexEnd}>
            <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}>
                <TextInput
                    label={i18n.t("returnAnexaFormScreenPart3")}
                    style={Inputs.textInput}
                    labelStyle={Inputs.textInputLabel}
                    value={state.newAnexo.nome}
                    onChangeText={(value) => {
                        setState({
                            newAnexo: {
                                ...state.newAnexo,
                                nome: value
                            }
                        });
                    }}
                />
                <TextInput
                    label={i18n.t("returnAnexaFormScreenPart4")}
                    style={Inputs.textInput}
                    labelStyle={Inputs.textInputLabel}
                    value={state.newAnexo.descricao}
                    onChangeText={(value) => {
                        setState({
                            newAnexo: {
                                ...state.newAnexo,
                                descricao: value
                            }
                        });
                    }}
                    multiline={true}
                />
                <View style={Layout.colLeft}>
                    {(state.file || state.fileRemoteUri) &&
                        <View style={{alignItems: 'flex-start'}}>
                            {thumbUri
                                ? <View style={Anexos.itemImageWrapper}>
                                    <Image source={{uri: thumbUri}} style={Anexos.itemImage}/>
                                    {ehVideo &&
                                        <FontAwesome name="play-circle-o" color={colors.white} size={40} style={Anexos.playIcon}/>
                                    }
                                </View>
                                : <View style={Anexos.formIconWrapper}>
                                        <FontAwesome name={state.icone} size={48} color={colors.text}/>
                                </View>
                            }
                            {state.newAnexo.fileSize &&
                                <Text>{i18n.t("returnAnexaFormScreenPart5")}{formataTamanhoArquivo(state.newAnexo.fileSize)}</Text>
                            }
                            {!state.file &&
                                <Text>{basename({path: state.fileRemoteUri})}</Text>
                            }
                            <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexo}>
                                {i18n.t("returnAnexaFormScreenPart6")}
                            </Button>
                        </View>
                    }
                    {(!state.file && !state.fileRemoteUri) &&
                        <View>
                            <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexo}>
                                {i18n.t("returnAnexaFormScreenPart7")}
                            </Button>
                        </View>
                    }
                </View>

            </KeyboardAwareScrollView>
            <View style={Layout.buttonsBottomWrapper}>
                <View style={(state.editing) ? {...Layout.row} : Layout.buttonRight}>
                    {(state.editing) &&
                        <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={apagarFn}>Remover</Button>
                    }
                    <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>Salvar</Button>
                    </View>
                </View>
            </View>
        </View>
        }
        {(!state.newAnexo || carregando) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        </>
    );
}
