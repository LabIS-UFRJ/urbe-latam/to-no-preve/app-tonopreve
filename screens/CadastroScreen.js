import React, {useReducer, useRef} from 'react';
import {Image, StyleSheet, View, Alert, ScrollView} from "react-native";
import {ActivityIndicator, IconButton, Button, Caption, Dialog, TextInput, Title, Text} from "react-native-paper";
import {Inputs, Layout} from "../styles";
import * as DocumentPicker from 'expo-document-picker';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {SafeAreaView} from "react-native-safe-area-context";
import MunicipioSelect from "../components/MunicipioSelect";
import EstadoSelect from "../components/EstadoSelect";
import {TextInputMask} from 'react-native-masked-text';
import {cpf} from 'cpf-cnpj-validator';
import {pickerStyles} from "../styles/pickers";
import Constants from 'expo-constants';
import colors from "../assets/colors";
import {AuthContext, carregaCurrentUser, carregaSettings, persisteCurrentUser, persisteSettings} from "../db/api";
import {geraTmpId} from '../utils/utils';
import STATUS from "../bases/status";
import CURRENT_USER from "../bases/current_user";
import i18n from 'i18n-js';

const pickerSelectStyles = StyleSheet.create(pickerStyles);



export default function CadastroScreen({route, navigation}) {
    const initialState = {
        currentUser: null,
        settings: null,
        municipioReferenciaId: 3303302,
        estadoId: 33,
        telefone: '',
        titulo: '',
        labelComunidade: '',
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const loading = useRef(true);
    const { signUp } = React.useContext(AuthContext);

    if (!route.params?.loaded) {
        loading.current = true;
        route.params = {
            ...route.params,
            loaded: true
        };
        // navigation.setParams({loaded: true});
    }

    // Load data from local storage
    if (loading.current) {
        loading.current = false;
        const titulosCadastro = {
            titulo: i18n.t('register'),
            labelComunidade: i18n.t('mappedCommunity'),
            labelSalvar: i18n.t('send')
        };
        if (route.name == 'Cadastro') {
            let newCurrentUser = CURRENT_USER;
            let newState = {...titulosCadastro};
            if (route.params?.username) {
                newCurrentUser.username = route.params.username;
                newState.telefone = route.params.username;
                delete route.params.username;
            }
            newState.currentUser = newCurrentUser;
            setState(newState);
        } else {
            carregaCurrentUser().then((currentUser) => {
                if (currentUser) {
                    let newState = {};
                    if (route.name == 'Cadastro') {
                        let estadoId = null;
                        let municipioReferenciaId = null;
                        if (!!currentUser.currentTerritorio.municipioReferenciaId) {
                            estadoId = parseInt(currentUser.currentTerritorio.municipioReferenciaId.toString().substring(0, 2));
                            municipioReferenciaId = parseInt(currentUser.currentTerritorio.municipioReferenciaId);
                        }
                        setState({
                            ...titulosCadastro,
                            currentUser: currentUser,
                            municipioReferenciaId: municipioReferenciaId,
                            estadoId: estadoId
                        });
                    } else {
                        carregaSettings().then(settings => {
                            currentUser.currentTerritorio = CURRENT_USER.currentTerritorio;
                            setState({
                                currentUser: currentUser,
                                settings: settings,
                                titulo: i18n.t('newCommunity'),
                                labelComunidade: i18n.t('communityName'),
                                labelSalvar: i18n.t('create')
                            });
                        });
                    }
                }
            }).catch((error) => {
                setState({
                    ...titulosCadastro,
                    currentUser: CURRENT_USER
                });
                console.log("Não foi criado este usuário localmente ainda.", error);
            });
        }
    }

    function validarCadastroBasico() {
        const errors = [];

        const is_br_cell = () => {
            if (
                state.currentUser.username.length === 13 &&
                state.currentUser.username.slice(0, 2) !== 55
            ) {
                return false;
            }
            if (state.currentUser.username.length < 10) {
                return false;
            }
            return true;
        };

        const is_col_cell = () => {
            if (
                state.currentUser.username.length === 12 &&
                state.currentUser.username.slice(0, 2) !== "57"
            ) {
                return false;
            }
            if (state.currentUser.username.length < 10) {
              return false;
            }
            return true;
        };


        if (route.name == 'Cadastro') {
            if (!state.currentUser.username) {
                errors.push([i18n.t('validarCadastroBasico_telephone'), `· ${i18n.t('validarCadastroBasico_yourTelephone')}`]);
            } else if (!is_br_cell() && !is_col_cell()) {
                errors.push([i18n.t('validarCadastroBasico_telephone'), `· ${i18n.t('validarCadastroBasico_yourTelephone')}`]);
            }

            if (!state.currentUser.fullName || 0 === state.currentUser.fullName.trim().length) {
                errors.push([i18n.t('validarCadastroBasico_name'), `· ${validarCadastroBasico_yourName}`]);
            }
        }
        if (route.name == 'CadastroTerritorio'){
            let comunidadeJaExiste = false;
            for (let territorio of state.settings.territorios) {
                if (territorio.id != null && territorio.nome === state.currentUser.currentTerritorio.nome) {
                    comunidadeJaExiste = true;
                    break;
                }
            }
            if (comunidadeJaExiste) {
                errors.push(['nomeComunidadeJaExiste', `· ${i18n.t('validarCadastroBasico_youAreAlreadyMapping')}`]);
            }
        }

        if (errors.length > 0) {
            let message = i18n.t('validarCadastroBasico_missingData');
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                i18n.t('validarCadastroBasico_alertMessage'),
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
            return false;
        }

        return true;
    }

    const salvarFn = () => {
        if (validarCadastroBasico()) {
            const currentUser = {
                ...state.currentUser,
                currentTerritorio: {
                  ...state.currentUser.currentTerritorio,
                  nome: "Preventorio",
                  municipioReferenciaId: state.municipioReferenciaId.toString(),
                  statusId: STATUS.TELEFONE_NAO_REGISTRADO,
                },
            };


            persisteCurrentUser({currentUser: currentUser}).then(() => {
                if (route.name == 'Cadastro') {
                    persisteSettings().then(() => {
                        signUp('new_user');
                    });
                } else {
                    state.settings.territorios.push(currentUser.currentTerritorio);
                    persisteSettings({territorios: state.settings.territorios}).then((res) => {
                        navigation.navigate('Home', {
                            atualizarDados: true,
                            snackBarMessage: i18n.t('salvarFn_youAreMapping')  + currentUser.currentTerritorio.nome
                        });
                    });
                }
            });
        }
    };

    return (
        <>
        {!state.currentUser &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>{i18n.t('loading')}</Text>
                </View>
            </View>
        }
        {state.currentUser &&
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={state.titulo} goBack={true} />
            <View style={Layout.body}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <SafeAreaView style={Layout.innerBody}>
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label={i18n.t('validarCadastroBasico_yourTelephone')}
                                value={state.telefone}
                                labelStyle={Inputs.textInputLabel}
                                style={Inputs.textInput}
                                onChangeText={text => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        username: text.replace(/[^0-9.]/g, ''),
                                    };
                                    setState({
                                        telefone: text,
                                        currentUser: newCurrentUser
                                    });
                                }}
                                render={props => (
                                  <TextInputMask
                                      {...props}
                                      type={'cel-phone'}
                                      keyboardType="phone-pad"
                                      options={{
                                          maskType: 'BRL',
                                          withDDD: true,
                                          dddMask: '(99) '
                                      }}
                                  />
                                )}
                            />
                        }
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label={i18n.t('validarCadastroBasico_yourName')}
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.currentUser.fullName}
                                onChangeText={(value) => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        fullName: value
                                    };
                                    setState({currentUser: newCurrentUser});
                                }}
                            />
                        }
                        <View style={Layout.buttonsBottomWrapper}>
                            <View style={(route.name == 'Cadastro') ? Layout.buttonRight : {...Layout.row}}>
                                {(route.name == 'CadastroTerritorio') &&
                                    <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={() => navigation.goBack()}>Cancelar</Button>
                                }
                                <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>{state.labelSalvar}</Button>
                            </View>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
        }
        </>
    );
    
}
