import React, {useReducer, useRef} from 'react';
import {Platform, BackHandler, StyleSheet, View, Alert} from "react-native";
import {Button, Text, TextInput, IconButton} from "react-native-paper";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import * as mime from "react-native-mime-types";
import XMLParser from 'react-xml-parser';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import EstadoSelect from "../components/EstadoSelect";
import MunicipioSelect from "../components/MunicipioSelect";
import ModoDeVidaSelect from "../components/ModoDeVidaSelect";
import TipoComunidadeSelect from "../components/TipoComunidadeSelect";
import TipoProducaoSelect from "../components/TipoProducaoSelect";
import {kml} from "../maps/togeojson";
import {getCurrentMensagemIndex, cloneCurrentUser, carregaTudoLocal, persisteCurrentUser, persisteSettings, cloneAppFiles} from "../db/api";
import {iconeDoMimeType, formataTamanhoArquivo, apagaArquivos, persisteAnexo} from "../db/arquivos";
import {pluralize, formataNumeroBR} from "../utils/utils";
import {Inputs, Layout} from "../styles";
import colors from "../assets/colors";
import TELAS from "../bases/telas";
import DADOS_BASICOS from '../bases/dadosbasicos';
import MENSAGEM from '../bases/mensagem';
import i18n from 'i18n-js';

export default function DadosBasicosScreen({navigation, route}) {
    const initialState = {
        id: null,
        nome: null,
        anoFundacao: null,
        qtdeFamilias: null,
        estadoId: null,
        municipioReferenciaId: null,
        modoDeVidaId: null,
        tiposComunidade: null,
        tipoProducaoId: null,
        poligono: null,
        poligonoArq: null,
        anexoAta: null,
        appFilesAnexoAta: null,
        currentUser: null,
        mensagem: null,
        settings: null,
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const populandoEstado = useRef(true);

    // Load data from local storage
    if (!state.currentUser || route.params.atualizarDados) {
        delete route.params.atualizarDados;
        populandoEstado.current = true;
        carregaTudoLocal().then(({currentUser, settings}) => {
            const newCurrentUser = cloneCurrentUser(currentUser);
            const newAppFiles = cloneAppFiles(settings.appFiles);
            /*DEBUG*/ console.log('dadosBasicos', newCurrentUser);
            let estadoId = null;
            let municipioReferenciaId = null;
            if (!!newCurrentUser.currentTerritorio.municipioReferenciaId) {
                estadoId = parseInt(newCurrentUser.currentTerritorio.municipioReferenciaId.toString().substring(0,2));
                municipioReferenciaId = parseInt(newCurrentUser.currentTerritorio.municipioReferenciaId);
            }
            const i = getCurrentMensagemIndex(newCurrentUser);
            const currentMensagemTexto = (i >= 0)
                ? newCurrentUser.currentTerritorio.mensagens[i].texto
                : null;
            let tiposComunidade = [];
            for (let tipoComunidade of newCurrentUser.currentTerritorio.tiposComunidade) {
                tiposComunidade.push(tipoComunidade.id);
            }
            const appFilesAnexoAta = newAppFiles.anexoAta;
            setState({
                ...newCurrentUser.currentTerritorio,
                estadoId: estadoId,
                municipioReferenciaId: municipioReferenciaId,
                anoFundacao: (newCurrentUser.currentTerritorio.anoFundacao)
                ? newCurrentUser.currentTerritorio.anoFundacao.toString()
                : null,
                qtdeFamilias: (newCurrentUser.currentTerritorio.qtdeFamilias)
                ? newCurrentUser.currentTerritorio.qtdeFamilias.toString()
                : null,
                mensagem: currentMensagemTexto,
                tiposComunidade: tiposComunidade,
                appFilesAnexoAta: appFilesAnexoAta,
                currentUser: newCurrentUser,
                settings: settings
            });
        }).catch(e => {
            console.log("Usuário não existe na base local!", e);
            // TODO: O que fazer num caso como estes? Tratar. O ideal é efetuar logout...
        });
    }

    const validarTerritorio = () => {
        const errors = [];
        if (!state.nome || 0 === state.nome.trim().length) {
            errors.push([i18n.t("validarTerritorio_name"), `· ${i18n.t("communityName")}`]);
        }

        if (!state.municipioReferenciaId) {
            errors.push(['municipioReferenciaId', `· ${i18n.t("validarTerritorio_County")}`]);
        }
        if (errors.length > 0) {

            let message = i18n.t("validarTerritorio_insufficientData");
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                i18n.t("validarTerritorio_incompleteData"),
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: false},
            );
            return false;
        }
        return true;
    };

    const salvarFn = async () => {
        if (validarTerritorio()) {
            for (let campo of Object.keys(DADOS_BASICOS)) {
                let valor;
                switch(DADOS_BASICOS[campo].type) {
                    case 'Number':
                        valor = state[campo]
                            ? parseInt(state[campo])
                            : null;
                        break;
                    case 'String':
                        valor = state[campo]
                            ? state[campo].toString()
                            : null;
                        break;
                    case 'Array':
                        valor = [];
                        for (let id of state[campo]) {
                            const item = {
                                id: id,
                                __typename: DADOS_BASICOS[campo].__typename
                            }
                            valor.push(item);
                        }
                        break;
                    case 'None':
                    default:
                        valor = state[campo];
                        break;
                }
                state.currentUser.currentTerritorio[campo] = valor;
            }

            const i = getCurrentMensagemIndex(state.currentUser);

            if (state.mensagem) {
                const newMensagem = {
                    ...MENSAGEM,
                    texto: state.mensagem
                };
                if (i >= 0) {
                    // Edit existing unsent message:
                    state.currentUser.currentTerritorio.mensagens[i] = newMensagem;
                } else {
                    // Add new unsent message:
                    state.currentUser.currentTerritorio.mensagens.push(newMensagem);
                }
            } else if (i >= 0) {
                // Remove unsent message:
                state.currentUser.currentTerritorio.mensagens.slice(i, 1);
            }

            setState({poligonoArq: null});

            if (state.appFilesAnexoAta.local) {
                await apagaArquivos([
                    state.settings.appFiles.anexoAta.file,
                    state.settings.appFiles.anexoAta.thumb
                ]);
                const newSettings = await persisteSettings({
                    appFiles: {
                        ...state.settings.appFiles,
                        anexoAta: state.appFilesAnexoAta
                    }
                });
            }

            const res = await persisteCurrentUser(state.currentUser);

            let params = {tela: TELAS.MAPA};
            if (res.dadosMudaram || state.appFilesAnexoAta.local) {
                params.snackBarMessage = i18n.t("salvarFn_communityUpdateMessage");
                params.atualizarDados = true;
            }

            navigation.navigate('Home', params);
        }
    };

    const onChangeEstado = (estadoId) => {
        if (populandoEstado.current) {
            populandoEstado.current = false;
        } else {
            setState({
                municipioReferenciaId: null,
                estadoId: estadoId
            });
        }
    };

    const getAnexoAta = async () => {
        const result = await DocumentPicker.getDocumentAsync({
            multiple: false,
            copyToCacheDirectory: false
        });
        if (result.type === 'success') {
            const res = await persisteAnexo({
                from: result.uri,
                currentUser: state.currentUser,
                filename: result.name
            });
            if (res.destinationUri) {
                const mimetype = mime.lookup(result.name);

                let newState = {
                    anexoAta: {
                        ...state.anexoAta,
                        nome: result.name,
                        mimetype: mimetype,
                        fileSize: result.size,
                        arquivo: null,
                        thumb: null
                    },
                    appFilesAnexoAta: {
                        ...state.appFilesAnexoAta,
                        file: res.destinationUri,
                        thumb: null,
                        icone: iconeDoMimeType(mimetype, 'awesome'),
                        local: true
                    }
                };

                setState(newState);
            }
        }
    };

    const showHelpAnexo = () => {
        Alert.alert(
            i18n.t("showHelpAnexo_communityMeetingRecord"),
            i18n.t("showHelpAnexo_communityRecordConditions") +
            "\n" +
            i18n.t("showHelpAnexo_sendDataConditions"),
            [
                {
                    text: 'OK', onPress: () => {
                    }
                },
            ],
            {cancelable: false},
        );
    };

    const openKmlImport = () => {
        Alert.alert(
            i18n.t("openKmlImport_importKMLFile"),
            i18n.t("openKmlImport_sendConditions"),
            [
                {
                    text: i18n.t("openKmlImport_cancel"), onPress: () => {}
                },
                {
                    text: i18n.t("openKmlImport_selectKMLFile"),
                    onPress: async () => {
                        const result = await DocumentPicker.getDocumentAsync({type: 'application/vnd.google-earth.kml+xml'});
                        if (result.type === 'success') {
                            const kmlContent = await FileSystem.readAsStringAsync(result.uri, {});
                            const xmlDoc = new XMLParser().parseFromString(kmlContent);
                            const json = kml(xmlDoc);

                            let polygons = [];
                            for (let element of json.features) {
                                if (element.geometry.type === 'Polygon') {
                                    let coordinates = [];
                                    for (let polygonPart of element.geometry.coordinates) {
                                        for (let i = 0; i < polygonPart.length - 1; i++) {
                                            let coordinate = polygonPart[i];
                                            coordinates.push({longitude: coordinate[0], latitude: coordinate[1]});
                                        }
                                    }
                                    polygons.push(coordinates);
                                }
                            }

                            //Agora ao formato graphQl
                            const multiPol = {
                                type: 'MultiPolygon',
                                coordinates: []
                            };

                            for (let polygon of polygons) {
                                let backPolygon = [[]];
                                for (let i in polygon) {
                                    backPolygon[0].push([polygon[i].longitude, polygon[i].latitude]);
                                }
                                backPolygon[0].push([polygon[0].longitude, polygon[0].latitude]);
                                multiPol.coordinates.push(backPolygon);
                            }

                            const tamanho = formataNumeroBR(result.size / 1024);

                            const poligonoArq = {
                                nome: result.name,
                                tamanho: tamanho
                            }

                            setState({
                                poligono: multiPol,
                                poligonoArq: poligonoArq
                            });
                        }
                    }
                },
            ],
            {cancelable: true},
        );
    };

    /*DEBUG*/ //console.log('------------RENDER----------');
    /*DEBUG*/ //console.log(state);

    return (
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={i18n.t("communityData")} />
            <View style={Layout.body}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={200}>
                    {state.currentUser &&
                    <View style={Layout.innerBody}>
                        <TextInput label={i18n.t("communityName")} style={Inputs.textInput} labelStyle={Inputs.textInputLabel} value={state.nome} onChangeText={(value) => setState({nome: value})}/>
                        <TextInput label={i18n.t("creationDate")} style={Inputs.textInput} labelStyle={Inputs.textInputLabel} keyboardType={'number-pad'} maxLength={4}
                                   value={state.anoFundacao} onChangeText={(value) => setState({anoFundacao: value})}/>
                        <EstadoSelect selectedValue={state.estadoId} onValueChange={onChangeEstado}/>
                        <MunicipioSelect estado={state.estadoId} selectedValue={state.municipioReferenciaId} onValueChange={(value) => setState({municipioReferenciaId: value})}/>
                        <TextInput
                            label={i18n.t("familiesQuantity")}
                            style={Inputs.textInput}
                            keyboardType="phone-pad"
                            value={state.qtdeFamilias}
                            onChangeText={(value) => setState({qtdeFamilias: value})}
                        />

                        <View style={{...Inputs.caption, alignItems: 'stretch'}}>
                            <TipoComunidadeSelect
                                selectedItems={state.tiposComunidade}
                                onValueChange={(values) => {
                                    setState({tiposComunidade: values})
                                }}
                            />
                        </View>

                        <View style={{...Layout.col, alignItems: 'flex-start'}}>
                            <Text style={Inputs.caption}>{i18n.t("recordCommunityMeeting")}</Text>
                            {state.anexoAta.nome
                                ? <View style={{alignItems: 'flex-start'}}>
                                    <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexoAta}>
                                        {i18n.t("changeRecordFile")}...
                                    </Button>
                                    <Text>{state.anexoAta.nome} ({formataTamanhoArquivo(state.anexoAta.fileSize)})</Text>
                                </View>
                                : <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexoAta}>
                                    {i18n.t("attachRecord")}...
                                </Button>
                            }
                        </View>

                        <View style={{...Layout.col, alignItems: 'flex-start'}}>
                            <Text style={Inputs.caption}> {i18n.t("importKMLPolygons")}</Text>
                            {state.poligonoArq != null
                                ? <View style={{alignItems: 'flex-start'}}>
                                    <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={openKmlImport}>
                                        {i18n.t("switchFile")}...
                                    </Button>
                                    <Text>{state.poligonoArq.nome} ({state.poligonoArq.tamanho}Kb) - {pluralize(state.poligono.coordinates.length, i18n.t("onePolygonImported"), state.poligono.coordinates.length + i18n.t("manyPolygonsImported"))}</Text>
                                </View>
                                : <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={openKmlImport}>
                                     {i18n.t("KMLFile")}...
                                </Button>
                            }
                        </View>

                        {state.currentUser.id &&
                            <TextInput
                            label={i18n.t("messageToAppTeam")}
                            style={Inputs.textInput}
                            labelStyle={Inputs.textInputLabel}
                            value={state.mensagem}
                            onChangeText={(value) => setState({mensagem: value})}
                            />
                        }

                        <View style={{...Layout.buttonRight, marginTop: 20}}>
                            <Button mode="contained" dark={true} onPress={salvarFn}>Salvar</Button>
                        </View>
                    </View>
                    }
                </KeyboardAwareScrollView>
            </View>
        </View>
    );
}
