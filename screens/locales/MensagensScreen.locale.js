const pt_br = {
    teamToNoMapa: "Equipe Tô no Prevê",
    messageScreenTitle: "Mensagens",
    convWithToNoMapaTeamAlertPart1: 'Diálogo com a equipe Tô no Prevê',
    convWithToNoMapaTeamAlertPart2: 'Aqui estão armazenadas as conversas com a equipe Tô no Prevê. Você pode enviar observações na tela "Dados da Comunidade".',
    convWithToNoMapaTeamAlertPart3: 'Entendi'
}

export const mensagensScreen = {
    'pt-BR': pt_br
}