// Alugmas frases estão divididas em traduções direnças importante notar na hora de trazudir ? e !
const pt_br = {
    updatedConfigurations: "Configurações atualizadas com sucesso",
    communityChanged : "Comunidade alterada com sucesso",
    securityCodeCopied: "Código de segurança copiado!",
    TerritorioRadioList_dataNotSend : "Você ainda não enviou dados para o Tô no Prevê. Quando tiver enviado, poderá adicionar novos territórios a serem mapeados aqui.",
    securityCode : "Código de Segurança",
    securityCodeWarning: "Não perca o seu código de segurança! Você precisará dele e do seu número de telefone se for reinstalar o aplicativo ou trocar de aparelho.",
    communityBeenMapped: "Comunidade sendo mapeada",
    appBehavior: "Comportamento do App",
    unitMeasureArea: "Unidade de área",
    gpsUse : "Uso do GPS",
    areaEdition : "Edição das áreas (polígonos)",
    communitys: "Comunidades", 
    addCommunity: "Adicionar comunidade",
    confirmCommunityChange : "Confirmar mudança de comunidade",
    attention: "Atenção!",
    communityChanges__part1: "Você tem mudanças da comunidade",
    communityChanges_part2 : "não enviadas ao Tô no Prevê!",
    createNewCommunityWarning :"Se você criar uma nova comunidade agora, perderá todas as alterações ainda não enviadas!",
    createNewCommunityConnectionWarning: "Tem certeza que quer criar uma nova comunidade a ser mapeada? Se sim, é preciso conexão à internet.",
    confirmationCommunityChange : "Tem certeza que quer mudar a comunidade para",
    changeOnlyInternet : "Esta mudança só é possível com acesso a internet!",
    changeCommunityWarning: "Se você mudar de comunidade agora, perderá todas as alterações ainda não enviadas!",
    changeCommunityConfirmation : "Tem certeza que quer mudar para a Comunidade",
    internetRequirement : "Se sim, é preciso conexão à internet!",
    cancel : "Cancelar",
    youAreMapping : 'Você está mapeando ',
    agreeToChangeCommunity : "Sim, mudar comunidade",
}

export const configurationScreen = {
    'pt-BR': pt_br
}