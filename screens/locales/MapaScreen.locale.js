const pt_br = {
    cancel: "Cancelar", 
    goToConfig: "Ir para as Configurações",
    tryAgain: "Sim, tentar de novo",
    updateConfigs: "Configurações atualizadas com sucesso",
    newMessage: "Novas mensagens da equipe Tô no Prevê",
    alertaErroExportaPDF_err0: "Erro ao gerar o relatório!",
    alertaErroExportaPDF_err1:
        "Este erro às vezes acontece por conta de memória do celular, e ao tentar de novo, pode dar certo. Deseja tentar mais uma vez?",
    ajustaMapaEExportaPDF_loading: "Gerando o relatório da comunidade...",
    persisteUsoOuConflito_message1Part1:
        "\n\nAtenção: Pode ser que estas mensagens sejam direcionadas para outra comunidade que você esteja mapeando, e não para a atual",
    persisteUsoOuConflito_message1Part2:
        "!\n\nNeste caso, vá em Configurações e mude a comunidade atual.",
    persisteUsoOuConflito_message2:
        "Você recebeu novas mensagens. Para vê-as, você deve enviar os dados atuais ao Tô no Prevê.",
    persisteUsoOuConflito_goTosendData: "Ir a Enviar Dados",
    persisteUsoOuConflito_showAllMessages: "Ver todas as mensagens",
    persisteUsoOuConflito_newMessageIs:
        "A nova mensagem é a seguinte:\n\n",
    persisteUsoOuConflito_message3Part1:
        "A equipe Tô no Prevê enviou uma mensagem para o território",
    persisteUsoOuConflito_message3Part2: "Você pode mudar para",
    persisteUsoOuConflito_message3Part3: 'em "Configurações"',
    alertAppSemPermissoesGPS_errPart0: "Aplicativo sem permissões",
    alertAppSemPermissoesGPS_errPart1:
        "O Tô no Prevê não pode acessar a localização do seu celular! Para poder usar o GPS, vá para as configurações do celular, e conceda permissão de localização para o Tô no Prevê.",
    locationAtivarAsync_loading: "Ativando o GPS...",
    apagarTodosPoligonosOuMarkers_title1Sing: "Apagar a área",
    apagarTodosPoligonosOuMarkers_title1Plur: "Apagar todas as áreas",
    apagarTodosPoligonosOuMarkers_message1Sing:
        "Deseja mesmo apagar a área que você mapeou para delimitar o território? Esta ação é definitiva.",
    apagarTodosPoligonosOuMarkers_message1Plur1: "Deseja mesmo apagar as ",
    apagarTodosPoligonosOuMarkers_message1Plur2:
        " áreas que você mapeou para delimitar o território? Esta ação é definitiva.",
    apagarTodosPoligonosOuMarkers_title2Sing:
        "Apagar o local de uso ou conflito",
    apagarTodosPoligonosOuMarkers_title2Plur:
        "Apagar todos os locais de uso e conflitos",
    apagarTodosPoligonosOuMarkers_message2Sing:
        "Deseja mesmo apagar o local de uso ou conflito que você registrou? Esta ação é definitiva.",
    apagarTodosPoligonosOuMarkers_message2Plur1: "Deseja mesmo apagar os ",
    apagarTodosPoligonosOuMarkers_message1Plur2:
        " locais de uso e conflitos que você registrou? Esta ação é definitiva.",
    apagarTodosPoligonosOuMarkers_alertTitle: "Não, continuar mapeando",
    apagarTodosPoligonosOuMarkers_alertText: "Sim, apagar",
    apagarTodosPoligonosOuMarkers_snackBarMessagePlur1:
        "Área apagada com sucesso!",
    apagarTodosPoligonosOuMarkers_snackBarMessagePlur2:
        " áreas apagadas com sucesso!",
    apagarTodosPoligonosOuMarkers_snackBarMessage2Sing: "ou",
    apagarTodosPoligonosOuMarkers_snackBarMessage2Sing2:
        "apagado com sucesso!",
    apagarTodosPoligonosOuMarkers_snackBarMessage2Plur2:
        "apagados com sucesso!",
    apagarPoligonoEmEdicao_info1: "Cancelar a demarcação desta área",
    apagarPoligonoEmEdicao_info2:
        "Você deseja cancelar o cadastro de nova área que está em edição?",
    apagarPoligonoEmEdicao_text1: "Não, continuar mapeando",
    apagarPoligonoEmEdicao_text2: "Sim",
    apagarPoligonoEmEdicao_snackBarMessage1:
        "A edição da área foi cancelada com sucesso!",
    apagarPoligonoEmEdicao_snackBarMessage2:
        "A edição da área foi cancelada com sucesso!",
    mapearTerritorioModoGPSAddPonto_alertText1: "Ponto já adicionado",
    mapearTerritorioModoGPSAddPonto_alertText2:
        "Você já adicionou este ponto.",
    mapearTerritorioModoGPSAddPonto_snackBarMessage:
        "Sua posição foi adicionada com sucesso",
    mapearUsoConflitoModoGPSAddPonto_alertMessage1: "Ponto já adicionado",
    mapearUsoConflitoModoGPSAddPonto_alertMessage2:
        "Você já adicionou este ponto.",
    mapMovedOrTouched_MTstatusMapeamentoTxt1:
        "Toque no mapa ou adicione sua posição atual para delimitar a área",
    mapMovedOrTouched_MTstatusMapeamentoTxt2:
        "Toque no mapa para delimitar a área (Deve ter mais que 3 pontos)",
    mapMovedOrTouched_MUCstatusMapeamentoTxt1part1:
        "Toque no mapa ou adicione sua posição atual para registrar um",
    mapMovedOrTouched_MUCstatusMapeamentoTxt1part2: "ou",
    mapMovedOrTouched_MUCstatusMapeamentoTxt2part1:
        "Toque no mapa para registrar um",
    mapMovedOrTouched_MUCstatusMapeamentoMessage1Part1:
        "Você já registrou ",
    mapMovedOrTouched_MUCstatusMapeamentoMessage1Part2: "ou",
    mapMovedOrTouched_MUCstatusMapeamentoMessage1Part3: "e",
    mapMovedOrTouched_TEstatusMapeamentoAppBarTitle:
        "Sincronizar os dados com o Tô no Prevê",
    mapTypeStyles_label1: "Mapa",
    mapTypeStyles_label2: "Satélite",
    AppIntroSlider_skipLabel: "PULAR",
    DialogTitlePortal: "Boas vindas ao Tô no Prevê!",
    DialogContentPortal:
        "Você deseja conhecer como funciona o aplicativo?",
    DialogPortalOption1: "Sim!",
    DialogPortalOption2: "Não",
    alertApagarArea_Part1: "Apagar área",
    alertApagarArea_Part2: "Tem certeza que quer apagar esta área de ",
    alertApagarArea_Part3: "? Esta operação não tem volta",
    alertApagarArea_Op1: "Não",
    alertApagarArea_Op2: "Sim, quero apagar esta área",
    alertApagarArea_alertSnackBarMessage:
        "Área da comunidade apagada com sucesso!",
    alertApagarArea_editaPoligonosText1: "Cancelar",
    alertApagarArea_editaPoligonosText2: "Apagar a área",
    alertApagarArea_editaPoligonosText3: "Editar a área",
    addVerticeMapearTerritorio: "Adicionar vértice de nova área",
    alertApagarTerritoriop1: "Editar ou apagar área",
    alertApagarTerritoriop2: "Você pode voltar a editar esta área (",
    alertApagarTerritoriop3: ") ou apagá-la. O que deseja fazer?",
    calloutApagarVerticeAlertP1: "Apagar vértice",
    calloutApagarVerticeAlertP2:
        "Tem certeza que quer apagar este ponto da área da comunidade?",
    calloutApagarVerticeAlertOp1: "Não",
    calloutApagarVerticeAlertOp2: "Sim, apagar",
    calloutVerticeID: "Vértice nº ",
    calloutVerticeText1: "Último vértice",
    calloutVerticeText2: "Apagar",
    gpsAlertp1: "Posição por GPS ativada",
    gpsAlertp2:
        "Você pode adicionar pontos no mapa a partir da sua posição atual, usando a funcionalidade de localização do seu celular. Deseja desativar esta opção?",
    gpsAlertText1: "Não quero",
    gpsAlertText2: "Sim, quero desativar o GPS",
    gpsAlert2p1: "Posição por GPS desativada",
    gpsAlert2p2:
        "Você pode adicionar pontos no mapa a partir da sua posição atual, usando a funcionalidade de localização do seu celular. Esta função está desativada. Quer ativá-la?",
    gpsAlert2op1: "Não quero",
    gpsAlert2op2: "Sim, quero ativar GPS",
    mapTerrPositionButton: "Adicionar posição atual",
    mapearButtonsContainerRowOp1: "Cancelar",
    mapearButtonsContainerRowOp2: "Apagar tudo",
    mapTerrSnackBarMessage: "Área da comunidade salva com sucesso!",
    mapTerrEditing1: "Salvar",
    mapTerrEditing2: "Feito!",
    portalDialogTelaEnviar: "Sincronizar os dados com o Tô no Prevê",
    portalDialogTelaEnviarEditingPart1:
        "Ops: você tem uma área sendo editada.",
    portalDialogTelaEnviarEditingPart2:
        'Por favor, vá em "mapear território" para salvar esta área ou então cancelar a edição.',
    portalDialogTelaEnviarSyncServerPart1:
        "Dados sendo sincronizados com o servidor:",
    portalDialogTelaEnviarSyncServerPart2:
        "Você conseguirá ver agora o mapeamento feito por outros usuários.",
    portalDialogEnviarDadosDialogoPart1:
        "As informações foram enviadas com sucesso para o Tô no Prevê.",
    portalDialogEnviarDadosDialogoPart2:
        "Obrigado pelo seu mapeamento !",
    portalDialogEnviarDadosDialogoPart3: "Ele está agora disponivel no site da plataforma !",
    portalDialogEnviarDadosDialogoErroPart1: "Dados não enviados.",
    portalDialogEnviarDadosDialogoErroPart2:
        "Infelizmente, houve algum problema no envio dos dados para a plataforma Tô no Prevê.",
    portalDialogEnviarDadosDialogoErroPart3: "Mensagem de erro:",
    portalDialogEnviarDadosDialogoErroPart4:
        "Por favor, se a internet está boa, entre em contato com nossa equipe de apoio e compartilhe o texto do erro acima.",
    portalDialogEnviarDadosCancelButton: "Cancelar",
    portalDialogEnviarDadosSendButton: "Enviar",
    portalDialogEnviarDadosExpReport: "Exportar relatório",
    stateCarregando: "Carregando...",
    alertaSemInternet: "Alerta! Você não está conectado à internet!",
    alertaSemInternetConteudo: "Você está usando um mapa local, algumas regiões não estarão visíveis."
}

const es_co = {};

export const translationsMapaScreen = {
  "pt-BR": pt_br,
};
