
const pt_br = {
    communityData: "Dados da Comunidade",
    communityName: "Nome da Comunidade",
    creationDate: "Ano de Criação",
    familiesQuantity: "Quantidade de familías",
    recordCommunityMeeting: "Ata de reunião da comunidade",
    changeRecordFile: "Trocar arquivo de ata",
    attachRecord: "Anexar ata",
    importKMLPolygons: "Opcional: Importar polígonos KML",
    switchFile: "Trocar arquivo",
    KMLFile: "Arquivo KML",
    messageToAppTeam: "Opcional: Mensagem à equipe Tô no Prevê",
    onePolygonImported: "1 polígono importado",
    manyPolygonsImported: " polígonos importados",
    validarTerritorio_name: "nome",
    validarTerritorio_County: "Município",    
    validarTerritorio_insufficientData: "Os seguintes dados precisam ser fornecidos:\n\n",
    validarTerritorio_incompleteData: "Dados incompletos",
    salvarFn_communityUpdateMessage: "As informações da comunidade foram atualizadas!",
    showHelpAnexo_communityMeetingRecord: "Ata de reunião",
    showHelpAnexo_communityRecordConditions: "É necessário que sua comunidade faça uma reunião e autorize você e até 5 outras pessoas a mapear seu território através do app Tô no Prevê.\n",
    showHelpAnexo_sendDataConditions: "Realize a reunião, faça a ata e coloque também a lista de presença, fotografe e envie por aqui. Sem a ata autorizando o mapeamento, seu cadastro não será aprovado.",
    openKmlImport_importKMLFile: "Importar arquivo KML",
    openKmlImport_sendConditions:"Escolha o arquivo KML com os polígonos que delimitam o território. Esta função é avançada e não é necessária: você pode registrar as áreas da sua comunidade diretamente no app! Se você não sabe o que é KML, ignore este campo.",
    openKmlImport_cancel:"Cancelar",
    openKmlImport_selectKMLFile:"Selecionar arquivo KML"
}

export const dadosBasicosScreen = {
    'pt-BR': pt_br
}