//Não traduzi as questões relativas ao CPF visto que não apareceram no final da localização
const pt_br = {
    register: 'Cadastro',
    mappedCommunity :'Comunidade que será mapeada',
    send: 'Enviar',
    newCommunity: 'Nova comunidade',
    communityName: 'Nome da comunidade',
    create: 'Criar',
    validarCadastroBasico_telephone: "telefone",
    validarCadastroBasico_yourTelephone: "Seu telefone",
    validarCadastroBasico_name: "nome",
    validarCadastroBasico_yourName: "Seu nome",
    validarCadastroBasico_county: "Município",
    validarCadastroBasico_nameMapCommunity: "Nome da comunidade que vai ser mapeada",
    validarCadastroBasico_youAreAlreadyMapping: "Você já está mapeando uma comunidade com este nome. Por favor, escolha outro nome",
    validarCadastroBasico_missingData: "Os seguintes dados precisam ser fornecidos ou alterados:\n\n",
    validarCadastroBasico_alertMessage : "Dados incompletos ou repetidos",
    salvarFn_youAreMapping : 'Você agora está mapeando a comunidade ',
    loading: "Carregando...",
    referenceCounty: "Município de Referência"
}   

export const CadastroScreen = {
    'pt-BR': pt_br
}