const pt_br = {
    LABELS_USO_1: "adicionado com sucesso!",
    LABELS_USO_2: "alterado com sucesso!",
    LABELS_USO_3: "Ponto apagado com sucesso!",
    LABELS_USO_4: 'Descrição',
    LABELS_USO_5: 'Informações adicionais',
    LABELS_USO_6part1: 'Tipo de',
    LABELS_USO_6part2: 'deste local',
    LABELS_CONFLITO_1: 'Marcar',
    LABELS_CONFLITO_2: 'Editar',
    LABELS_CONFLITO_3: 'adicionado com sucesso!',
    LABELS_CONFLITO_4: 'alterado com sucesso!',
    LABELS_CONFLITO_5: "Ponto apagado com sucesso!",
    LABELS_CONFLITO_6: 'Descrição',
    LABELS_CONFLITO_7: 'Observações',
    LABELS_CONFLITO_8part1: "Tipo de",
    validarUsoOuConflito_name: 'nome',
    validarUsoOuConflito_error1: " está muito grande. Favor reduzir para menos de 45 letras",
    validarUsoOuConflito_field: 'tipo',
    validarUsoOuConflito_error2: "Os seguintes dados precisam ser fornecidos:\n\n",
    validarUsoOuConflito_error2part2: 'Dados incompletos',
    apagarFn_titleAlertPart1: 'Tem certeza que quer apagar ',
    apagarFn_titleAlertPart2: 'este local de uso?',
    apagarFn_titleAlertPart3: 'este conflito?',
    apagarFn_titleAlertMessage1: 'Você não poderá desfazer esta ação. Tem certeza?',
    apagarFn_titleAlertOption1: 'Não',
    apagarFn_titleAlertOption2: 'Sim, quero apagar',
    editarUsoConflitoRender1: 'Local de'
}

export const editarTiposScreen = {
    'pt-BR': pt_br
}