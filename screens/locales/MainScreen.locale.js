const pt_br = {
    drawnItemLabelSettings: "Configurações",
    drawnItemLabelAboutUs: "Sobre o Tô no Prevê",
    drawnItemLabelComData: "Dados da comunidade",
    drawnItemLabelMapTerr: "Mapear o território",
    drawnItemLabelCategories: 'e',
    drawnItemLabelFilesAndImages: "Arquivos e imagens",
    drawnItemLabelSendToTonoMapa: "Sincronizar",
    drawnItemLabelShareReport: "Compartilhar relatório",
    drawnItemLabelMessages: "Mensagens",
    alert1Title: 'Território ainda não cadastrado no Tô no Prevê',
    alertTxt: 'Você só poderá receber e enviar mensagens à equipe Tô no Prevê após ter enviado os dados pela primeira vez para o Tô no Prevê.',
    sendData: 'Enviar Dados',
    drawnItemLabelInit: "Início",

}

export const mainScreen = {
    'pt-BR': pt_br
}