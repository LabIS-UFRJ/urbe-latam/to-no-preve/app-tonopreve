
const pt_br = {
    downloadingTerms : "Baixando os Termos de Uso...",
    termsOfServiceTitle : "Termos de Uso - Tô no Prevê",
    return : "Voltar"
}   

export const TermosDeUsoScreen = {
    'pt-BR': pt_br
}