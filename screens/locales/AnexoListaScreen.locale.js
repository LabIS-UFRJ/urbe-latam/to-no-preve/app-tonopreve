
const pt_br = {
    communityAttachments: "Fotos, boletins, relatórios, documentos e outros arquivos anexos com mais informações sobre a comunidade",
    loading : "Carregando",
    item_FileSize: "Tamanho",
    apagarFn_fileDeleted: "Arquivo apagado com sucesso",
    fileUpdateSuccess : "Arquivo atualizado com sucesso",
    fileAddedSuccess:"Arquivo adicionado com sucesso",
    mostraStats_file: " arquivo",
    mostraStats_files: " arquivos",
    mostraStats_andConjunction : " e ",
    mostraStats_image:  " imagem",
    mostraStats_images: " imagems",
    onVisualizarItem_fileSavedSuccess: "Arquivo salvo no seu celular com sucesso",
    onVisualizarItem_phoneFileError: "Seu celular não tem nenhum programa para abrir este tipo de arquivo." ,
    onVisualizarItem_fileDownloadError: "Não foi possível baixar este arquivo. Por favor, verifique sua conexão à internet.",
    onApagarItem_deleteFile: "Apagar arquivo",
    onApagarItem_confirmDesireToDeleteFile : "Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?",
    onApagarItem_maintainFile : "Não, manter",
    onApagarItem_confirmDelete : "Sim, apagar",
}

export const anexoListaScreen = {
    'pt-BR': pt_br
}