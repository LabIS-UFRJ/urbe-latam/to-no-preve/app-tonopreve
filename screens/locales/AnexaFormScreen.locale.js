const pt_br = {
    validaDadosMessageError1: "Os seguintes dados precisam ser fornecidos:\n\n",
    validaDadosError2part1: 'nome',
    validaDadosError2part2: '· Nome do arquivo',
    validaDadosError3part1: 'file',
    validaDadosError3part2: '· O arquivo enviado é maior que ',
    validaDadosError3part3: '. Por favor, envie um arquivo menor.',
    validaDadosError4part1: 'file',
    validaDadosError4part2: '· Nenhum arquivo anexo',
    validaDadosAlert: 'Dados incompletos',
    getAnexoValidaDadosAlertPart1: 'Operação falhou!',
    getAnexoValidaDadosAlertPart2: "Não foi possível concluir a operação. Por favor, tente novamente, com este ou outro arquivo. Se o erro persistir, ente por favor em contato com a equipe do Tô no Prevê",
    apagarFnAnexoValidaDadosAlertPart1: 'Apagar arquivo',
    apagarFnAnexoValidaDadosAlertPart2: "Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?",
    apagarFnAnexoValidaDadosAlertPart3: 'Não, manter',
    apagarFnAnexoValidaDadosAlertPart4: 'Sim, apagar',
    returnAnexaFormScreenPart1: "Editar arquivo ou imagem",
    returnAnexaFormScreenPart2: "Novo arquivo ou imagem",
    returnAnexaFormScreenPart3: "Nome do arquivo/imagem",
    returnAnexaFormScreenPart4: "Descrição (recomendado, mas opcional)",
    returnAnexaFormScreenPart5: 'Tamanho: ',
    returnAnexaFormScreenPart6: 'Trocar arquivo...',
    returnAnexaFormScreenPart7: 'Arquivo...'
}

export const anexaFormScreen = {
    'pt-BR': pt_br
}