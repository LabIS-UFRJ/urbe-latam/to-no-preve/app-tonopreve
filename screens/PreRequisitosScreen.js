import React from 'react';
import {Image, StyleSheet, View, ScrollView} from "react-native";
import {Button, Paragraph, Surface, Text, Title} from "react-native-paper";
import {Alignments, Layout, Messages} from "../styles";
import {SafeAreaView} from "react-native-safe-area-context";
import TextoSobre from '../partials/TextoSobre';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import colors from '../assets/colors';
import i18n from 'i18n-js'

export default function PreRequisitosScreen({navigation}) {
    const telefoneNaoCadastrado = false;
    return (
        <View style={Layout.containerCentered}>
            <AppbarTonomapa navigation={navigation} title={i18n.t('preRequisitosTitleScreen')} goBack={true} />
            <View style={Layout.body}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <SafeAreaView style={Layout.innerBody}>
                        <Image source={require('../assets/images/logo/main/tonomapa_logo.png')} style={Layout.logoImageCadastro}
                                   resizeMode="contain"/>
                        {telefoneNaoCadastrado &&
                            <Button color={colors.warning} style={{...Layout.buttonCenter, marginTop:8, marginBottom:20}} mode="outlined">
                                {i18n.t('phoneIsNotRegistered')}
                            </Button>
                        }

                        <TextoSobre />

                        <Button mode="contained" dark={true} style={Layout.buttonCenter} onPress={() => {
                                navigation.navigate('Cadastro');
                            }}>{i18n.t('wannaRegister')}</Button>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
    );
}
