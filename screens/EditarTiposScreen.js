import React, { useReducer, useState } from 'react'
import {
    Alert,
    StyleSheet,
    View,
    ScrollView,
    BackHandler,
    Image
} from 'react-native'
import { Button, Text, TextInput } from 'react-native-paper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Inputs, Layout, Icons } from '../styles'
import colors from '../assets/colors'
import AppbarTonomapa from '../partials/AppbarTonomapa'
import TELAS from '../bases/telas'
import { useFocusEffect } from '@react-navigation/native'
import { NovoTipoConflito } from '../components/NovoTipoConflito'
import NovaAreaDeUso from '../components/NovaAreaDeUso'
import TipoConflitoSelect from '../components/TipoConflitoSelect'
import TipoAreaDeUsoSelect from '../components/TipoAreaDeUsoSelect'
import { Form } from '../components/Form/Form'
import { findLabel } from '../components/Form/fields'
import tipos from '../bases/tipos'
import * as ImagePicker from 'expo-image-picker'
import { AntDesign, FontAwesome } from '@expo/vector-icons'
import lodash from 'lodash'

//import appConfig from '../app.config';
import Constants from 'expo-constants'
import { consoleSandbox } from '@sentry/utils'

const styles = StyleSheet.create({
    buttonContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        marginBottom: 20,
        marginTop: 20
    },
    ImageSection: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 23
    },
    PickImageSection: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    ImageContainer: {
        width: 320,
        height: 200
    }
})

export default function EditarTiposScreen({ navigation, route }) {
    let initialObjectState = {
        id: null,
        __typename: 'AreaDeUso',
        nome: null,
        descricao: null,
        tipoConflitoId: null,
        tipoAreaDeUsoId: null,
        image: null,
        form: {
            id: null,
            __typename: 'Form',
            entrevistador: null,
            setor: null,
            endereco: null,
            telefone: null,
            numeroCasa: null,
            numMen18: null,
            num1855: null,
            num55Mais: null,
            numNe: null,
            numPets: null,
            matPredom: null,
            sustentacao: null,
            conserv: null,
            matTelhado: null,
            matCobre: null,
            idadeCasa: null,
            ultRef: null,
            numPisos: null,
            pisoMorador: null,
            matAndMora: null,
            evento: {
                id: null,
                tipo: null,
                data: null,
                impacto: null,
                reparo: null
            },
            posicao: {
                ...route.params.posicao
            },
            image: null
        }
    }
    const [imgErr, setImgErr] = useState(false);
    switch (route.params.__typename) {
        case 'Form': {
            initialObjectState.__typename = route.params.__typename
            initialObjectState.vindoDe = route.params.vindoDe
            console.log('NO SWITCH', route.params)
            delete route.params.vindoDe
            initialObjectState.form = {
                ...initialObjectState.form,
                ...route.params
            }

            if (route.params.id) {
                initialObjectState.id = route.params.id
            }

            break
        }
        case 'AreaDeUso':
        case 'Conflito': {
            initialObjectState = {
                ...initialObjectState,
                ...route.params
            }
            break
        }

        default: {
            break
        }
    }

    const LABELS_USO = {
        title: `Marcar ${Constants.manifest.extra.translations.goodCategorySingular}`,
        editTitle: `Editar ${Constants.manifest.extra.translations.goodCategorySingular}`,
        snackBarMessage: `${Constants.manifest.extra.translations.goodCategorySingular} adicionado com sucesso!`,
        snackBarMessageEdited: `${Constants.manifest.extra.translations.goodCategorySingular} alterado com sucesso!`,
        snackBarMessageDelete: 'Ponto apagado com sucesso!',
        nome: 'Descrição',
        descricao: 'Informações adicionais',
        tipo: `Tipo de ${Constants.manifest.extra.translations.goodCategorySingular} deste local`
    }

    const LABELS_CONFLITO = {
        title: `Marcar ${Constants.manifest.extra.translations.badCategorySingular}`,
        editTitle: `Editar ${Constants.manifest.extra.translations.badCategorySingular}`,
        snackBarMessage: `${Constants.manifest.extra.translations.badCategorySingular} adicionado com sucesso!`,
        snackBarMessageEdited: `${Constants.manifest.extra.translations.badCategorySingular} alterado com sucesso!`,
        snackBarMessageDelete: 'Ponto apagado com sucesso!',
        nome: 'Descrição',
        descricao: 'Observações',
        tipo: `Tipo de ${Constants.manifest.extra.translations.badCategorySingular}`
    }

    const LABELS_FORM = {
        title: `Marcar ${Constants.manifest.extra.translations.formCategorySingular}`,
        editTitle: `Editar ${Constants.manifest.extra.translations.formCategorySingular}`,
        snackBarMessage: `${Constants.manifest.extra.translations.formCategorySingular} adicionada com sucesso!`,
        snackBarMessageEdited: `${Constants.manifest.extra.translations.formCategorySingular} alterada com sucesso!`,
        snackBarMessageDelete: 'Ponto apagado com sucesso!',
        tipo: `Tipo de ${Constants.manifest.extra.translations.formCategorySingular}`
    }

    const reducer = (state, newState) => {
        return { ...state, ...newState }
    }

    const [objectState, setObjectState] = useReducer(
        reducer,
        initialObjectState
    )

    const emEdicao = objectState.id !== null
    const labels = (() => {
        switch (objectState.__typename) {
            case 'Conflito': {
                return LABELS_CONFLITO
            }
            case 'AreaDeUso': {
                return LABELS_USO
            }
            default: {
                return LABELS_FORM
            }
        }
    })()

    const title = emEdicao ? labels.editTitle : labels.title
    const msg = emEdicao ? labels.snackBarMessageEdited : labels.snackBarMessage

    //Android Back button
    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                navigation.navigate(objectState.vindoDe, {
                    ...objectState.mapeamentoInfo
                })
                return true
            }
            BackHandler.addEventListener('hardwareBackPress', onBackPress)
            return () =>
                BackHandler.removeEventListener(
                    'hardwareBackPress',
                    onBackPress
                )
        }, [route])
    )

    const customGoBack = () => {
        navigation.navigate(objectState.vindoDe, {
            ...objectState.mapeamentoInfo
        })
    }

    const validaForm = () => {
        const errors = Object.keys(objectState.form).reduce((res, curr) => {
            if (curr !== 'id') {
                if (
                    objectState.form[curr] === null ||
                    objectState.form[curr] === undefined ||
                    objectState.form[curr].toString().trim() === ''
                ) {
                    const _label = findLabel(curr)
                    if (_label) {
                        res.push(['nome', `· ${_label}`])
                    }
                }
            }

            return res
        }, [])

        if (errors.length > 0) {
            let message = 'Os seguintes dados precisam ser fornecidos:\n\n'
            for (let erro of errors) {
                message += erro[1] + '\n'
            }

            Alert.alert(
                'Dados incompletos',
                message,
                [
                    {
                        text: 'OK',
                        onPress: async () => {}
                    }
                ],
                { cancelable: false }
            )
            return false
        }

        return true
    }

    const validarUsoOuConflito = () => {
        const errors = []
        if (!objectState.nome || 0 === objectState.nome.trim().length) {
            errors.push(['nome', '· ' + labels.nome])
        } else if (objectState.nome.length > 45) {
            errors.push([
                'nome',
                '· ' +
                    labels.nome +
                    ' está muito grande. Favor reduzir para menos de 45 letras'
            ])
        }

        const campo =
            objectState.__typename === 'AreaDeUso'
                ? 'tipoAreaDeUsoId'
                : 'tipoConflitoId'
        if (!objectState[campo]) {
            errors.push(['tipo', '· ' + labels.tipo])
        }
        if (errors.length > 0) {
            let message = 'Os seguintes dados precisam ser fornecidos:\n\n'
            for (let erro of errors) {
                message += erro[1] + '\n'
            }

            Alert.alert(
                'Dados incompletos',
                message,
                [
                    {
                        text: 'OK',
                        onPress: async () => {}
                    }
                ],
                { cancelable: false }
            )
            return false
        }

        return true
    }

    const apagarFn = () => {
        const mapeamento = {
            ...objectState
        }
        let titleAlert =
            'Tem certeza que quer apagar ' +
            (mapeamento.__typename === 'AreaDeUso'
                ? 'este local de uso?'
                : 'este conflito?')
        Alert.alert(
            titleAlert,
            'Você não poderá desfazer esta ação. Tem certeza?',
            [
                {
                    text: 'Não'
                },
                {
                    text: 'Sim, quero apagar',
                    onPress: async () => {
                        switch (mapeamento.__typename) {
                            case 'Conflito': {
                                delete mapeamento.tipoConflitoId

                                navigation.navigate('Home', {
                                    tela: TELAS.MAPEAR_USOSECONFLITOS,
                                    apagarUsoOuConflito: true,
                                    usoOuConflitoEditado: { ...mapeamento },
                                    snackBarMessage:
                                        labels.snackBarMessageDelete
                                })

                                break
                            }
                            case 'AreaDeUso': {
                                delete mapeamento.tipoAreaDeUsoId

                                navigation.navigate('Home', {
                                    tela: TELAS.MAPEAR_USOSECONFLITOS,
                                    apagarUsoOuConflito: true,
                                    usoOuConflitoEditado: {
                                        ...mapeamento
                                    },
                                    snackBarMessage:
                                        labels.snackBarMessageDelete
                                })
                                break
                            }
                            case 'Form': {
                                navigation.navigate('Home', {
                                    tela: TELAS.MAPEAR_USOSECONFLITOS,
                                    apagarFormEditado: true,
                                    formEditado: {
                                        ...mapeamento.form
                                    },
                                    snackBarMessage:
                                        labels.snackBarMessageDelete
                                })
                                break
                            }
                            default: {
                                break
                            }
                        }
                    }
                }
            ],
            { cancelable: true }
        )
    }

    const salvarFn = () => {
        switch (objectState.__typename) {
            case 'AreaDeUso':
            case 'Conflito': {
                if (validarUsoOuConflito()) {
                    const usoOuConflitoEditado = {
                        ...objectState
                    }

                    delete usoOuConflitoEditado.vindoDe
                    delete usoOuConflitoEditado.form

                    if (usoOuConflitoEditado.__typename === 'AreaDeUso') {
                        delete usoOuConflitoEditado.tipoConflitoId
                    } else {
                        delete usoOuConflitoEditado.tipoAreaDeUsoId
                    }

                    const finalUsoOuConflito = lodash.isEqual(
                        objectState,
                        initialObjectState
                    )
                        ? navigation.navigate('Home')
                        : navigation.navigate('Home', {
                              usoOuConflitoEditado,
                              tela: TELAS.MAPEAR_USOSECONFLITOS,
                              snackBarMessage: msg
                          })
                }
                break
            }
            default: {
                if (validaForm()) {
                    const formEditado = {
                        ...objectState.form
                    }

                    lodash.isEqual(objectState, initialObjectState)
                        ? navigation.navigate('Home')
                        : navigation.navigate('Home', {
                              formEditado,
                              tela: TELAS.MAPEAR_USOSECONFLITOS,
                              snackBarMessage: msg
                          })
                }

                break
            }
        }
    }

    const cancelarFn = () => {
        navigation.navigate('Home')
    }

    const pickImage = async (actionType) => {
        let result = null
        let objPickerOptions = {
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1
        }
        switch (actionType) {
            case 'camera':
                result = await ImagePicker.launchCameraAsync(objPickerOptions)
                break
            case 'gallery':
                result = await ImagePicker.launchImageLibraryAsync(
                    objPickerOptions
                )
                break
        }

        if (!result?.cancelled) {
            if (objectState.__typename === 'Form') {
                setObjectState({
                    ...objectState,
                    form: { ...objectState.form, image: result.uri }
                })
            } else {
                setObjectState({
                    ...objectState,
                    image: result.uri
                })
            }
        }
    }

    const resolveImageAddress = () => {
        switch (objectState.__typename) {
            case 'Form': {
                if (objectState.form.image  && objectState.form.image.includes('file')) {
                    return  objectState.form.image;
                }
                return `${Constants.manifest.extra.dashboardEndpoint}/media/censos/${objectState.form.id}.jpeg`
            }
            case "AreaDeUso":{
                if (objectState.image && objectState.image.includes('file')) {
                    return objectState?.image
                        
                }
                return `${Constants.manifest.extra.dashboardEndpoint}/media/pots/${objectState.id}.jpeg`
            }
            case "Conflito":{
                if (objectState.image && objectState.image.includes('file')) {
                    return objectState?.image
                        
                }
                return `${Constants.manifest.extra.dashboardEndpoint}/media/vuls/${objectState.id}.jpeg`
            }

            default: {
                throw "Unexpected type"
            }
        }
    }

    return (
        <View style={Layout.containerStretched}>
            <ScrollView>
                <AppbarTonomapa
                    navigation={navigation}
                    title={title}
                    tela={TELAS.MAPEAR_USOSECONFLITOS}
                    customGoBack={customGoBack}
                />
                <View style={Layout.body}>
                    <KeyboardAwareScrollView
                        keyboardShouldPersistTaps='handled'
                        enableOnAndroid={true}
                        extraScrollHeight={120}
                    >
                        {objectState.__typename === 'AreaDeUso' ||
                        objectState.__typename === 'Conflito' ? (
                            <View
                                style={{
                                    ...Layout.bodyFlexEnd,
                                    paddingTop: 20
                                }}
                            >
                                {objectState.__typename == 'Conflito' ? (
                                    <>
                                        <TipoConflitoSelect
                                            selectedValue={
                                                objectState.tipoConflitoId
                                            }
                                            onValueChange={(id, nome) =>
                                                setObjectState({
                                                    tipoConflitoId: id,
                                                    tipoConflito: {
                                                        id: id,
                                                        nome: nome
                                                    }
                                                })
                                            }
                                        />
                                        {Constants.manifest.extra
                                            .newTypeCategory &&
                                            objectState.tipoConflitoId ==
                                                tipos.data.tiposConflito
                                                    .length && (
                                                <NovoTipoConflito
                                                    selectedValue={
                                                        objectState.tipoConflitoId
                                                    }
                                                    onValueChange={(value) =>
                                                        setObjectState({
                                                            ...value
                                                        })
                                                    }
                                                />
                                            )}
                                    </>
                                ) : (
                                    <>
                                        <TipoAreaDeUsoSelect
                                            selectedValue={
                                                objectState.tipoAreaDeUsoId
                                            }
                                            onValueChange={(id, nome) => {
                                                setObjectState({
                                                    tipoAreaDeUsoId: id,
                                                    tipoAreaDeUso: {
                                                        id: id,
                                                        nome: nome
                                                    }
                                                })
                                            }}
                                        />
                                        {Constants.manifest.extra
                                            .newTypeCategory &&
                                            objectState.tipoAreaDeUsoId ==
                                                tipos.data.tiposAreaDeUso
                                                    .length && (
                                                <NovaAreaDeUso
                                                    selectedValue={
                                                        objectState.tipoAreaDeUsoId
                                                    }
                                                    onValueChange={(value) =>
                                                        setObjectState({
                                                            ...value
                                                        })
                                                    }
                                                />
                                            )}
                                    </>
                                )}

                                <TextInput
                                    label={labels.nome + ' (até 45 letras)'}
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={objectState.nome}
                                    onChangeText={(novoNome) =>
                                        setObjectState({ nome: novoNome })
                                    }
                                />

                                <TextInput
                                    label={labels.descricao}
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={objectState.descricao}
                                    onChangeText={(novaDescricao) =>
                                        setObjectState({
                                            descricao: novaDescricao
                                        })
                                    }
                                    multiline={true}
                                />

                                <View style={styles.ImageSection}>
                                    {route.params?.usoOuConflito?.image ||
                                    !imgErr ? (
                                        <Image
                                            style={styles.ImageContainer}
                                            onError={({ nativeEvent: { error } })=> {
                                                //console.log(error)
                                                setImgErr(true)
                                            }}
                                            source={{
                                                uri: resolveImageAddress()
                                            }}
                                        />
                                    ) : (
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                color: 'gray'
                                            }}
                                        >
                                            Selecione uma imagem
                                        </Text>
                                    )}
                                    <View
                                        style={styles.PickImageSection}
                                        flexDirection={'row'}
                                    >
                                        <AntDesign
                                            name='camerao'
                                            size={50}
                                            style={Icons.camera}
                                            onTouchStart={async () =>{
                                                await pickImage('camera')
                                                setImgErr(false)
                                            }
                                            }
                                        />
                                        <FontAwesome
                                            name='photo'
                                            size={40}
                                            style={Icons.gallery}
                                            onTouchStart={async () =>{
                                                await pickImage('gallery')
                                                setImgErr(false)
                                            }
                                            }
                                        />
                                    </View>
                                </View>

                                <View style={Layout.buttonsBottomWrapperScroll}>
                                    <View
                                        style={
                                            emEdicao
                                                ? { ...Layout.row }
                                                : Layout.buttonRight
                                        }
                                    >
                                        {emEdicao && (
                                            <Button
                                                mode='contained'
                                                style={Layout.buttonLeft}
                                                color={colors.danger}
                                                onPress={apagarFn}
                                            >
                                                Apagar
                                            </Button>
                                        )}
                                        <Button
                                            mode='contained'
                                            dark={true}
                                            style={Layout.buttonRight}
                                            color={colors.primary}
                                            onPress={salvarFn}
                                        >
                                            Salvar
                                        </Button>
                                    </View>
                                </View>
                            </View>
                        ) : (
                            <View>
                                <View style={styles.ImageSection}>
                                    {!imgErr ? (
                                        <Image
                                            style={styles.ImageContainer}
                                            onError={({ nativeEvent: { error } })=> {
                                                setImgErr(true)
                                            }}
                                            source={{
                                                uri: (() => {
                                                    const uri = resolveImageAddress()
                                                    return uri;
                                                })()
                                            }}
                                        />
                                    ) : (
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                color: 'gray'
                                            }}
                                        >
                                            Selecione uma imagem
                                        </Text>
                                    )}
                                    <View
                                        style={styles.PickImageSection}
                                        flexDirection={'row'}
                                    >
                                        <AntDesign
                                            name='camerao'
                                            size={50}
                                            style={Icons.camera}
                                            onTouchStart={async () =>{
                                                await pickImage('camera')
                                                setImgErr(false)
                                            }
                                            }
                                        />
                                        <FontAwesome
                                            name='photo'
                                            size={40}
                                            style={Icons.gallery}
                                            onTouchStart={async () =>{
                                                await pickImage('gallery')
                                                setImgErr(false)
                                            }
                                            }
                                        />
                                    </View>
                                </View>
                                <Form
                                    formValue={objectState.form}
                                    onSave={(newForm) => {
                                        console.log("o new form vem com imge", newForm)
                                        setObjectState({
                                            ...objectState,
                                            form: {
                                                ...newForm,
                                                image: objectState.form.image
                                            }
                                        })
                                    }}
                                    shouldClose={() => {
                                        salvarFn()
                                    }}
                                    onDelete={apagarFn}
                                    editMode={emEdicao}
                                />
                            </View>
                        )}
                    </KeyboardAwareScrollView>
                </View>
            </ScrollView>
        </View>
    )
}
