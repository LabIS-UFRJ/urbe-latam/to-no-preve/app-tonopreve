import React, { useState, useReducer, useRef, useEffect } from 'react'
import {
    AppState,
    BackHandler,
    StyleSheet,
    View,
    Text,
    Dimensions,
    Alert,
    Image,
    PixelRatio
} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import MapView, {
    PROVIDER_GOOGLE,
    MAP_TYPES,
    Polygon,
    ProviderPropType,
    Marker,
    Callout
} from 'react-native-maps'
import {
    Avatar,
    Appbar,
    Button,
    IconButton,
    Snackbar,
    Portal,
    Dialog,
    ActivityIndicator,
    Banner
} from 'react-native-paper'
import AppbarTonomapa from '../partials/AppbarTonomapa'
import { SafeAreaView } from 'react-native-safe-area-context'
import colors from '../assets/colors'
import { Layout, LayoutMapa } from '../styles'
import * as Location from 'expo-location'
import {
    AuthContext,
    persisteCurrentUser,
    carregaSettings,
    persisteSettings,
    carregaTudoLocal,
    enviaParaDashboard,
    validaDados,
    cloneCurrentUser,
    carregaListaTerritorios,
    otherUsersInfo
} from '../db/api'
import {
    pluralize,
    deepEqual,
    geraTmpId,
    resgataEstadoMunicipioDoId
} from '../utils/utils'
import { exportarTerritorio } from '../utils/exportarTerritorio'
import AppIntroSlider from 'react-native-app-intro-slider'
import TELAS from '../bases/telas'
import { STATUS, buildStatusMessage } from '../bases/status'
import { geraIconeTipoUsoOuConflito } from '../bases/icones'
import { useFocusEffect } from '@react-navigation/native'
import tipos from '../bases/tipos'
import {
    slideStyles,
    SLIDES,
    renderSlide,
    SlideWidget
} from '../bases/intro_slides'
import { calculaAreaPoligono, formataArea } from '../maps/mapsUtils'
import metrics from '../utils/metrics'
import Constants from 'expo-constants'
import i18n from 'i18n-js'

import * as FileSystem from 'expo-file-system'

import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height

const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
let id = 0

const GEOLOCATION_OPTIONS = {
    accuracy: Location.Accuracy.Highest,
    distanceInterval: 5,
    timeInterval: 1000
}

const ICONE_TIPO = {
    locationMark: require('../assets/images/gps/escuro/noun_Gps_Add.png'),
    locationMarkClaro: require('../assets/images/gps/claro/noun_Gps_Add.png'),
    verticeMovel: require('../assets/images/vertices/escuro/30/vertice.png'),
    verticeMovelClaro: require('../assets/images/vertices/claro/30/vertice.png'),
    vertice: require('../assets/images/vertices/escuro/30/vertice.png'),
    verticeClaro: require('../assets/images/vertices/claro/30/vertice.png')
}
const LOGO_IMAGE = {
    colorida: require('../assets/images/logo/colorida/80/logo.png'),
    branca: require('../assets/images/logo/branca/80/logo.png')
}

let snackBarMessage = ''
let locationMonitoramentoUsuaria = null

export default function MapaScreen({
    provider = null,
    navigation = null,
    route
}) {
    // /*DEBUG*/ console.log('Home', route.params);
    const [otherUserMarkers, setOtherUserMarkers] = useState([])
    const [otherUserMarkersTipo, setOtherUserMarkersTipo] = useState([])
    const [formMarkers, setFormMarkers] = useState([])
    //console.log(decodeURIComponent(decodeURI(FileSystem.documentDirectory))  + "assets/tiles/{z}/{x}/{y}.png");

    const initialState = {
        region: null,
        polygons: [],
        markers: [],
        markersTipoUsoConflito: [],
        editingMarker: null,
        creatingHole: false,
        modo: null,
        tela: TELAS.MAPA,
        locationMarkerCoordinate: null,
        currentUser: null,
        enviarDadosDialogo: {
            botaoEnviar: true,
            botaoCancelar: true,
            botaoOk: null,
            etapa: 'situacao',
            situacao: [],
            erro: null
        },
        carregando: null,
        preparandoShot: null,
        statusMessage: null,
        mapType: null,
        settings: {
            editing: false
        },
        introDialog: true
    }
    const enviarDadosDialogoErro = {
        ...initialState.enviarDadosDialogo,
        botaoEnviar: null,
        botaoCancelar: null,
        botaoOk: true,
        etapa: 'erro'
    }
    const enviarDadosDialogoCarregando = {
        ...enviarDadosDialogoErro,
        botaoOk: null,
        etapa: 'carregando',
        situacao: 'carregando'
    }
    const reducer = (state, newState) => {
        return { ...state, ...newState }
    }
    const [state, setState] = useReducer(reducer, initialState)

    const calloutVisible = useRef(false)
    const locationMarker = useRef()
    const locationMonitoramentoUsuariaAtivo = useRef(false)
    const statusChanged = useRef(true)
    const map = useRef(null)
    const viewShotRef = useRef(null)
    const mapLoaded = useRef(false)
    const mapMarker = []
    const mapPolygon = []
    let mapEditingPolygon = useRef()
    const mapEditingPolygonEdge = []
    const [snackBarVisible, setSnackBarVisible] = useState(false)
    const { signUp, signOut } = React.useContext(AuthContext)
    const edgePadding = {
        top: metrics.tenWidth * 18,
        bottom: metrics.tenWidth * 6,
        left: metrics.tenWidth * 4,
        right: metrics.tenWidth * 4
    }
    const edgePaddingAdjusted =
        Platform.OS === 'android'
            ? {
                  top: PixelRatio.getPixelSizeForLayoutSize(edgePadding.top),
                  bottom: PixelRatio.getPixelSizeForLayoutSize(
                      edgePadding.bottom
                  ),
                  left: PixelRatio.getPixelSizeForLayoutSize(edgePadding.left),
                  right: PixelRatio.getPixelSizeForLayoutSize(edgePadding.right)
              }
            : edgePadding

    const updateSettings = (params) => {
        // /*DEBUG*/ console.log('entered updateSettings');
        persisteSettings(params.settings).then((newSettings) => {
            setState({
                settings: newSettings
            })
            if (!params.noSnackBarMessage) {
                snackBarMessage = i18n.t('updateConfigs')
                setSnackBarVisible(true)
            }
        })
    }

    useEffect(() => {
        if (
            state.tela === TELAS.ENVIAR &&
            state.enviarDadosDialogo.etapa == 'situacao' &&
            state.settings.syncedToDashboard &&
            state.settings.syncedToDashboard != STATUS.TELEFONE_NAO_REGISTRADO
        ) {
            const res = otherUsersInfo(state.currentUser)
            res.then((val) => otherUserDataToMap(val))
        }
    }, [state])

    const otherUserDataToMap = (otherUserData) => {
        //const offset = 1000;
        const offset = 0;
        //Trata erro
        if (
            otherUserData === false ||
            (otherUserData.areasDeUso.length === 0 &&
                otherUserData.conflitos.length === 0)
        ) {
            return
        }

        const { areasDeUso, conflitos } = otherUserData

        //console.log(areasDeUso,conflitos);
        let markers = []
        let todasCoords = { latitude: [], longitude: [] }
        let markersTipoUsoConflito = []
        let coordMap = new Map()
        const currConflitos = state.currentUser.currentTerritorio.conflitos
        const currAreas = state.currentUser.currentTerritorio.areasDeUso
        //console.log(currConflitos,currAreas);

        const helper = (elem) => {
            //console.log(elem)
            todasCoords.latitude.push(elem.posicao.coordinates[1])
            todasCoords.longitude.push(elem.posicao.coordinates[0])
            coordMap.set(elem.posicao.coordinates.toString(), true)
        }
        currConflitos.forEach(helper)
        currAreas.forEach(helper)
        // Generate map markers - conflitos
        for (let conflito of conflitos) {
            if (
                conflito.posicao &&
                !coordMap.has(conflito.posicao.coordinates.toString())
            ) {
                //const iconeConflitoId = (conflito.tipoConflitoId > tipos.data.tiposConflito.length ) ? tipos.data.tiposConflito.length : conflito.tipoConflitoId;
                coordMap.set(conflito.posicao.coordinates.toString(), true)
                const iconeConflitoId = tipos.data.tiposConflito.length
                let icone = geraIconeTipoUsoOuConflito(
                    'conflito',
                    iconeConflitoId
                )
                let newConflito = {
                    id: offset + markers.length + 1,
                    title: conflito.nome,
                    description: conflito.descricao,
                    coordinate: {
                        latitude: conflito.posicao.coordinates[1],
                        longitude: conflito.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: conflito,
                    isFromOtherUser: true
                }
                todasCoords.latitude.push(conflito.posicao.coordinates[1])
                todasCoords.longitude.push(conflito.posicao.coordinates[0])
                markers.push(newConflito)
                if (!conflito.tipoConflito) {
                    markersTipoUsoConflito.push(
                        getNameTipoUsoOuConflito(newConflito)
                    )
                } else {
                    markersTipoUsoConflito.push(conflito.tipoConflito.nome)
                }
            }
        }

        // Generate map markers - locais de uso
        for (let areaDeUso of areasDeUso) {
            if (
                areaDeUso.posicao &&
                !coordMap.has(areaDeUso.posicao.coordinates.toString())
            ) {
                //const iconeAreaDeUsoId = (areaDeUso.tipoAreaDeUsoId > tipos.data.tiposAreaDeUso.length ) ? tipos.data.tiposAreaDeUso.length : areaDeUso.tipoAreaDeUsoId;
                coordMap.set(areaDeUso.posicao.coordinates.toString(), true)
                const iconeAreaDeUsoId = tipos.data.tiposAreaDeUso.length
                let icone = geraIconeTipoUsoOuConflito(
                    'areaDeUso',
                    iconeAreaDeUsoId
                )
                let newAreaDeUso = {
                    id: offset + markers.length + 1,
                    title: areaDeUso.nome,
                    description: areaDeUso.descricao,
                    coordinate: {
                        latitude: areaDeUso.posicao.coordinates[1],
                        longitude: areaDeUso.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: areaDeUso,
                    isFromOtherUser: true
                }
                todasCoords.latitude.push(areaDeUso.posicao.coordinates[1])
                todasCoords.longitude.push(areaDeUso.posicao.coordinates[0])
                markers.push(newAreaDeUso)
                if (!areaDeUso.tipoAreaDeUso) {
                    markersTipoUsoConflito.push(
                        getNameTipoUsoOuConflito(newAreaDeUso)
                    )
                } else {
                    markersTipoUsoConflito.push(areaDeUso.tipoAreaDeUso.nome)
                }
            }
        }
        coordMap = null
        setOtherUserMarkers(markers)
        setOtherUserMarkersTipo(markersTipoUsoConflito)

        const { municipio } = resgataEstadoMunicipioDoId(
            state.currentUser.currentTerritorio.municipioReferenciaId
        )
        if (markers.length > 0) {
            let diffCoords = {
                latitude:
                    Math.max(...todasCoords.latitude) -
                    Math.min(...todasCoords.latitude),
                longitude:
                    Math.max(...todasCoords.longitude) -
                    Math.min(...todasCoords.longitude)
            }
            state.region = {
                latitude:
                    Math.min(...todasCoords.latitude) + diffCoords.latitude / 2,
                longitude:
                    Math.min(...todasCoords.longitude) +
                    diffCoords.longitude / 2,
                latitudeDelta: diffCoords.latitude + LATITUDE_DELTA / 4,
                longitudeDelta: diffCoords.longitude + LONGITUDE_DELTA / 4
            }
        } else {
            state.region = {
                latitude: municipio.latitude,
                longitude: municipio.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
        }
        setState({
            region: state.region
        })
        map.current.animateToRegion(state.region, 2000)
    }

    const getAllMapElementsCoords = () => {
        let coords = []
        for (let i = 0; i < state.polygons.length; i++) {
            coords = coords.concat(state.polygons[i].coordinates)
        }
        for (let i = 0; i < state.markers.length; i++) {
            coords.push(state.markers[i].coordinate)
        }
        if (
            state.settings.editing &&
            state.settings.editing.coordinates.length >= 3
        ) {
            coords = coords.concat(state.settings.editing.coordinates)
        }
        return coords
    }

    const areaPoligonos = () => {
        if (state.polygons.length) {
            return state.polygons.reduce(
                (area, current) => area + current.area,
                0
            )
        }
        return null
    }

    const mapFitToElements = () => {
        // /*DEBUG*/ console.log('mapFitToElements');
        if (
            map &&
            (state.polygons.length ||
                state.markers.length ||
                state.settings.editing?.coordinates?.length >= 3)
        ) {
            if (
                state.polygons.length == 0 &&
                state.markers.length == 1 &&
                (!state.settings.editing ||
                    state.settings.editing.coordinates.length < 3)
            ) {
                map.current.animateCamera({
                    center: state.markers[0].coordinate
                })
            } else {
                map.current.fitToCoordinates(getAllMapElementsCoords(), {
                    edgePadding: edgePaddingAdjusted
                })
            }
        } else {
            map.current.animateToRegion(state.region, 2000)
        }
    }

    const alertaErroExportaPDF = () => {
        Alert.alert(
            i18n.t('alertaErroExportaPDF_err0'),
            i18n.t('alertaErroExportaPDF_err1'),
            [
                {
                    text: i18n.t('cancel'),
                    onPress: async () => {}
                },
                {
                    text: i18n.t('tryAgain'),
                    onPress: async () => {
                        ajustaMapaEExportaPDF()
                    }
                }
            ],
            { cancelable: true }
        )
    }

    const ajustaMapaEExportaPDF = () => {
        const finishLoadingState = {
            carregando: null,
            preparandoShot: false
        }
        setState({
            carregando: i18n.t('ajustaMapaEExportaPDF_loading'),
            preparandoShot: true
        })
        mapFitToElements()
        const params = {
            currentUser: state.currentUser,
            areaTxt: formataArea(areaPoligonos(), state.settings.unidadeArea),
            statusMessage: state.statusMessage,
            uri: null
        }
        const resultType = Platform.OS === 'ios' ? 'base64' : 'file'
        setTimeout(() => {
            setState(finishLoadingState)
        }, 30000)
        setTimeout(() => {
            map.current
                .takeSnapshot({
                    format: Platform.OS === 'ios' ? 'png' : 'jpg',
                    quality: 0.6, // image quality: 0..1 (only relevant for jpg, default: 1)
                    result: 'base64' // result types: 'file', 'base64' (default: 'file')
                })
                .then((uri) => {
                    params.uri = uri
                    exportarTerritorio(params)
                        .then(() => {
                            setState(finishLoadingState)
                        })
                        .catch((e) => {
                            setState(finishLoadingState)
                            alertaErroExportaPDF()
                        })
                })
                .catch((e) => {
                    exportarTerritorio(params)
                        .then(() => {
                            setState(finishLoadingState)
                        })
                        .catch((e) => {
                            console.log('Falha na geração do PDF sem o mapa', e)
                            setState(finishLoadingState)
                            alertaErroExportaPDF()
                        })
                })
        }, 3000)

        // setTimeout(() => {
        //     captureRef(map, {
        //         format: "jpg",
        //         quality: 0.5
        //     }).then(
        //         uri => {
        //             params.uri = uri;
        //             exportarTerritorio(params).then(() => {
        //                 setState(finishLoadingState);
        //             }).catch(e => {
        //                 console.log('Falha na geração do PDF', e);
        //                 setState(finishLoadingState);
        //             });
        //         },
        //         error => {
        //             console.error("Falha na geração do mapa", error);
        //             exportarTerritorio(params).then(() => {
        //                 setState(finishLoadingState);
        //             });
        //         }
        //     );
        // }, 3000);
    }

    const carregaDados = () => {
        // /*DEBUG*/ console.log("carregadados");
        carregaTudoLocal()
            .then((dadosLocais) => {
                // /*DEBUG*/ console.log('carregaDados', JSON.stringify(dadosLocais));
                // Only for already existing devices with version before 0.93 and already logged in:
                // TODO: remove this after a while:
                if (dadosLocais.settings.territorios.length == 0) {
                    carregaListaTerritorios(dadosLocais.currentUser.id).then(
                        (territorios) => {
                            persisteSettings({ territorios: territorios })
                        }
                    )
                } else {
                    setState({ settings: dadosLocais.settings })
                    gqlTerritorioToMapa({
                        currentUser: dadosLocais.currentUser
                    })
                }
            })
            .catch((error) => {
                console.log('Erro ao pegar os dados:', error)
                // signOut();
            })
    }

    /**
     * Converte dados do mapa para graphQL e os persiste na base local
     * Parâmetros:
     *   - currentUser - currentUser changed
     *   - polygons - polygons changed
     *   - markers - markers changed
     *   - snackBarMessage
     */
    const persisteGraphqlData = (params) => {
        // /*DEBUG*/ console.log('Entrou persisteGraphqlData');
        // /*DEBUG*/ console.log(params);
        if (
            !('currentUser' in params) &&
            !('polygons' in params) &&
            !('markers' in params)
        ) {
            return
        }

        let newState = {}
        const currentUser =
            'currentUser' in params
                ? cloneCurrentUser(params.currentUser)
                : cloneCurrentUser(state.currentUser)

        if ('polygons' in params) {
            //Traduz state.polygons para o formato aceito pelo backend
            const polygons = params.polygons
            const multiPol = {
                type: 'MultiPolygon',
                coordinates: []
            }

            for (let polygon of polygons) {
                let backPolygon = [[]]
                for (let i in polygon.coordinates) {
                    backPolygon[0].push([
                        polygon.coordinates[i].longitude,
                        polygon.coordinates[i].latitude
                    ])
                }
                backPolygon[0].push([
                    polygon.coordinates[0].longitude,
                    polygon.coordinates[0].latitude
                ])
                multiPol.coordinates.push(backPolygon)
            }

            currentUser.currentTerritorio.poligono = multiPol

            newState = {
                polygons: polygons
            }
        }

        if ('markers' in params) {
            newState.markers = params.markers
        }
        console.log(currentUser);
        persisteCurrentUser(currentUser).then((res) => {
            if (res.dadosMudaram) {
                // Update settings if necessary:

                let newSettings = {}
                if (res.newSettings) {
                    newSettings = res.newSettings
                }
                if ('settings' in params) {
                    newSettings = {
                        ...newSettings,
                        ...params.settings
                    }
                }
                if ('settings' in params || res.newSettings) {
                    updateSettings({
                        settings: newSettings,
                        noSnackBarMessage: true
                    })
                }

                //newState.currentUser = res.newCurrentUser;
                newState.currentUser = null // obriga o carregamento e ajustes no status
                if ('newState' in params) {
                    newState = {
                        ...newState,
                        ...params.newState
                    }
                }
                if('formMarkers' in params){
                    setFormMarkers(params.formMarkers)
                }
                setState(newState)

                // Show snackbar, if necessary:
                if ('snackBarMessage' in params) {
                    snackBarMessage = params.snackBarMessage
                    setSnackBarVisible(true)
                }
            }
        })
    }

    const persisteFormEditado = () => {
        const formEditado = { ...(route.params.formEditado ?? {}) }

        formEditado.evento = Object.entries(formEditado.evento).reduce(
            (res, curr) => {
                if (curr[1]) {
                    res = true
                }

                return res
            },
            false
        )
            ? formEditado.evento
            : null

        if (!formEditado.evento) {
            delete formEditado.evento
        }

        delete formEditado.vindoDe
        const apagarFormEditado = route.params.apagarFormEditado
        delete route.params.formEditado
        delete route.params.apagarFormEditado


        const newMarkersList =
            state.markers?.filter((marker) => (marker.coordinate.latitude !== formEditado.posicao.coordinates[1]) 
            && (marker.coordinate.longitude !== formEditado.posicao.coordinates[0])  ) ||
            []

        if (!apagarFormEditado) {
            newMarkersList.push({
                //id: formEditado.id,
                title: `${formEditado.setor} ${formEditado.numeroCasa}`,
                description: '',
                coordinate: {
                    latitude: formEditado.posicao.coordinates[1],
                    longitude: formEditado.posicao.coordinates[0]
                },
                icone: geraIconeTipoUsoOuConflito('areaDeUso', 6),
                object: { ...formEditado }
            })
        }

        let newCurrentUser = cloneCurrentUser(state.currentUser)

        if (!newCurrentUser.currentTerritorio.censosPreve) {
            newCurrentUser.currentTerritorio.censosPreve = []
        }

        let formsEditados = newCurrentUser.currentTerritorio.censosPreve.filter((form) => (form.posicao.coordinates[1] !== formEditado.posicao.coordinates[1]) 
            && (form.posicao.coordinates[0] !== formEditado.posicao.coordinates[0])  )
        
        if (!apagarFormEditado) {
            formsEditados.push({ ...formEditado })
        }
        console.log("+==================")
        formsEditados = formsEditados.map((form,i) =>{
            console.log(form)
            if(form.id === null)
                return { ...form, id:geraTmpId(i) }
            return form
        });

        console.log("alohaaaaaaaaa", formsEditados);

        newCurrentUser.currentTerritorio.censosPreve = [...formsEditados]
        let persisteGraphqlDataParams = {
            currentUser: { ...newCurrentUser },
            formMarkers: newMarkersList 
        }

        if ('snackBarMessage' in route.params) {
            persisteGraphqlDataParams = {
                ...persisteGraphqlDataParams,
                snackBarMessage: route.params.snackBarMessage
            }
            delete route.params.snackBarMessage
        }

        persisteGraphqlData(persisteGraphqlDataParams)
    }

    const persisteUsoOuConflito = () => {
        const usoOuConflitoEditado = route.params.usoOuConflitoEditado
        delete usoOuConflitoEditado.vindoDe
        delete route.params.usoOuConflitoEditado
        // /*DEBUG*/ console.log('usoOuConflitoEditado');

        let newMarkersList = []
        let newCurrentUser = cloneCurrentUser(state.currentUser)
        let usosEConflitosEditados = {
            areasDeUso: newCurrentUser.currentTerritorio.areasDeUso,
            conflitos: newCurrentUser.currentTerritorio.conflitos
        }
        state.markers.forEach((marker) =>
            newMarkersList.push({
                ...marker,
                coordinate: { ...marker.coordinate }
            })
        )
        let stopLoop = false
        if (usoOuConflitoEditado.id) {
            for (let indiceTerritorio in usosEConflitosEditados) {
                for (let usoOuConflitoKey in usosEConflitosEditados[
                    indiceTerritorio
                ]) {
                    if (
                        usosEConflitosEditados[indiceTerritorio][
                            usoOuConflitoKey
                        ].id === usoOuConflitoEditado.id
                    ) {
                        if (route.params.apagarUsoOuConflito) {
                            usosEConflitosEditados[indiceTerritorio].splice(
                                usoOuConflitoKey,
                                1
                            )
                        } else if (
                            indiceTerritorio == 'conflitos' &&
                            usoOuConflitoEditado.__typename == 'AreaDeUso'
                        ) {
                            usosEConflitosEditados.conflitos.splice(
                                usoOuConflitoKey,
                                1
                            )
                            usosEConflitosEditados.areasDeUso.push(
                                usoOuConflitoEditado
                            )
                        } else if (
                            indiceTerritorio == 'areasDeUso' &&
                            usoOuConflitoEditado.__typename == 'Conflito'
                        ) {
                            usosEConflitosEditados.areasDeUso.splice(
                                usoOuConflitoKey,
                                1
                            )
                            usosEConflitosEditados.conflitos.push(
                                usoOuConflitoEditado
                            )
                        } else {
                            usosEConflitosEditados[indiceTerritorio][
                                usoOuConflitoKey
                            ] = usoOuConflitoEditado
                        }
                        stopLoop = true
                        break
                    }
                }
                if (stopLoop) {
                    break
                }
            }

            //Atualiza ou apaga marker
            for (let i = 0; i < newMarkersList.length; i++) {
                let marker = newMarkersList[i]
                if (marker.object.id == usoOuConflitoEditado.id) {
                    if (route.params.apagarUsoOuConflito) {
                        newMarkersList.splice(i, 1)
                    } else if (!('usoOuConflitoMoveu' in route.params)) {
                        newMarkersList[i].title = usoOuConflitoEditado.nome
                        newMarkersList[i].description =
                            usoOuConflitoEditado.descricao
                        newMarkersList[i].object = usoOuConflitoEditado
                    } else {
                        newMarkersList[i].object = usoOuConflitoEditado
                        newMarkersList[i].coordinate.latitude =
                            usoOuConflitoEditado.posicao.coordinates[1]
                        newMarkersList[i].coordinate.longitude =
                            usoOuConflitoEditado.posicao.coordinates[0]
                    }
                    break
                }
            }
            delete route.params.apagarUsoOuConflito
        } else {
            usoOuConflitoEditado.id = geraTmpId(newMarkersList.length + 1)
            const indiceTerritorio =
                usoOuConflitoEditado.__typename === 'AreaDeUso'
                    ? 'areasDeUso'
                    : 'conflitos'

            usosEConflitosEditados[indiceTerritorio].push(usoOuConflitoEditado)

            newMarkersList.push({
                id: newMarkersList.length + 1,
                title: usoOuConflitoEditado.nome,
                description: usoOuConflitoEditado.descricao,
                coordinate: {
                    latitude: usoOuConflitoEditado.posicao.coordinates[1],
                    longitude: usoOuConflitoEditado.posicao.coordinates[0]
                },
                object: usoOuConflitoEditado
            })
        }

        let persisteGraphqlDataParams = {
            currentUser: newCurrentUser,
            markers: newMarkersList
        }

        if ('snackBarMessage' in route.params) {
            persisteGraphqlDataParams = {
                ...persisteGraphqlDataParams,
                snackBarMessage: route.params.snackBarMessage
            }
            delete route.params.snackBarMessage
        }
        persisteGraphqlData(persisteGraphqlDataParams)
    }

    ///////////////////////////////
    //   PROCESSA ROUTE.PARAMS  //
    //////////////////////////////

    if (route.params.firstRun && 'editing' in state.settings) {
        if (state.settings.editing != null) {
            //route.params.tela = TELAS.MAPEAR_TERRITORIO
        }
        delete route.params.firstRun
    }
    if (route.params.appIntro) {
        delete route.params.appIntro
        updateSettings({
            settings: { appIntro: true },
            noSnackBarMessage: true
        })
    }
    if (route.params.tela && state.tela != route.params.tela) {
        setState({ tela: route.params.tela })
    }
    if (route.params.gotNotification) {
        const gotNotification = { ...route.params.gotNotification }
        delete route.params.gotNotification
        if (gotNotification.fetchPolicy == 'network-only') {
            const complemento =
                state.settings.territorios.length > 1
                    ? i18n.t('persisteUsoOuConflito_message1Part1') +
                      state.currentUser.currentTerritorio.nome +
                      i18n.t('persisteUsoOuConflito_message1Part2')
                    : ''
            Alert.alert(
                i18n.t('newMessage'),
                i18n.t('persisteUsoOuConflito_message2') + complemento,
                [
                    {
                        text: i18n.t('persisteUsoOuConflito_sendData'),
                        onPress: async () => {
                            navigation.navigate('Home', { tela: TELAS.ENVIAR })
                        }
                    },
                    {
                        text: 'Ok',
                        onPress: async () => {}
                    }
                ],
                { cancelable: true }
            )
        } else if (
            gotNotification.territorioId ==
            state.currentUser.currentTerritorio.id
        ) {
            Alert.alert(
                i18n.t('persisteUsoOuConflito_goTosendData'),
                i18n.t('persisteUsoOuConflito_newMessageIs') +
                    gotNotification.texto,
                [
                    {
                        text: i18n.t('persisteUsoOuConflito_showAllMessages'),
                        onPress: async () => {
                            setState({ currentUser: null })
                            navigation.navigate('Mensagens', {
                                atualizarDados: true
                            })
                        }
                    },
                    {
                        text: 'Ok',
                        onPress: async () => {
                            setState({ currentUser: null })
                        }
                    }
                ],
                { cancelable: false }
            )
        } else {
            let territorioNome
            for (let territorio of state.settings.territorios) {
                if (
                    parseInt(territorio.id) ==
                    parseInt(gotNotification.territorioId)
                ) {
                    territorioNome = territorio.nome
                    break
                }
            }
            Alert.alert(
                i18n.t('newMessage'),
                `
i18n.t('persisteUsoOuConflito_message3Part1') ${territorioNome}:

${gotNotification.texto}

i18n.t('persisteUsoOuConflito_message3Part2') ${territorioNome} i18n.t('persisteUsoOuConflito_message3Part3').
`,
                [
                    {
                        text: i18n.t('goToConfig'),
                        onPress: async () => {
                            navigation.navigate('Configuracoes', {
                                atualizarDados: true
                            })
                        }
                    },
                    {
                        text: 'Ok',
                        onPress: async () => {}
                    }
                ],
                { cancelable: true }
            )
        }
    }
    if (route.params.ajustaMapaEExportaPDF) {
        delete route.params.ajustaMapaEExportaPDF
        ajustaMapaEExportaPDF()
    }
    if (route.params.atualizarDados) {
        delete route.params.atualizarDados
        setState({ currentUser: null })
    }

    const tipoDeMapeamento = route.params.usoOuConflitoEditado
        ? 1
        : route.params.formEditado
        ? 2
        : route.params.snackBarMessage
        ? 3
        : null

    switch (tipoDeMapeamento) {
        case 1: {
            persisteUsoOuConflito()
            break
        }
        case 2: {
            persisteFormEditado()
            break
        }
        case 3: {
            snackBarMessage = route.params.snackBarMessage
            delete route.params.snackBarMessage
            setSnackBarVisible(true)
            break
        }
        default: {
            break
        }
    }

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                if (route.params.tela != TELAS.MAPA) {
                    navigation.navigate('Home', { tela: TELAS.MAPA })
                    return true
                } else {
                    navigation.goBack()
                }
            }
            BackHandler.addEventListener('hardwareBackPress', onBackPress)

            // Handle GPS disable/enable depending on TELA and useGPS setting
            const useGPS =
                state.settings.useGPS &&
                [TELAS.MAPEAR_TERRITORIO, TELAS.MAPEAR_USOSECONFLITOS].indexOf(
                    route.params.tela
                ) > -1
            const onChangeAppState = (appState) => {
                if (useGPS) {
                    if (appState.match(/inactive|background/)) {
                        locationDesativar()
                    } else if (state.settings.useGPS) {
                        locationAtivar()
                    }
                }
            }
            AppState.addEventListener('change', onChangeAppState)

            if (!locationMonitoramentoUsuariaAtivo.current && useGPS) {
                locationAtivar()
            }
            return () => {
                if (locationMonitoramentoUsuariaAtivo.current && useGPS) {
                    locationDesativar()
                }
                AppState.removeEventListener('change', onChangeAppState)
                BackHandler.removeEventListener(
                    'hardwareBackPress',
                    onBackPress
                )
            }
        }, [route, state.settings.useGPS, locationMonitoramentoUsuariaAtivo])
    )

    // Load data from local storage
    if (state.currentUser == null) {
        carregaDados()
        statusChanged.current = true
    } else if (statusChanged.current == true) {
        statusChanged.current = false

        let mudouStatusId = false

        const situacaoArray = validaDados({
            currentUser: state.currentUser,
            settings: state.settings,
            markers: state.markers,
            poligonos: state.polygons,
            syncedToDashboard: state.settings.syncedToDashboard
        })

        if (
            !state.enviarDadosDialogo.situacao ||
            !deepEqual(
                state.enviarDadosDialogo.situacao,
                situacaoArray.situacaoArray
            )
        ) {
            state.enviarDadosDialogo.situacao = situacaoArray.situacaoArray
            mudouStatusId = true
        }

        if (
            !state.settings.syncedToDashboard &&
            state.currentUser.currentTerritorio.statusId !=
                situacaoArray.statusId
        ) {
            state.currentUser.currentTerritorio.statusId =
                situacaoArray.statusId
            mudouStatusId = true
        } else if (
            state.settings.syncedToDashboard &&
            state.currentUser.currentTerritorio.statusId !=
                state.settings.syncedToDashboard
        ) {
            state.currentUser.currentTerritorio.statusId =
                state.settings.syncedToDashboard
            mudouStatusId = true
        }

        if (mudouStatusId) {
            state.statusMessage = buildStatusMessage(
                state.currentUser.currentTerritorio.statusId
            )

            // /*DEBUG*/ console.log('Mudou status para: ' + state.currentUser.currentTerritorio.statusId);
            setState({
                enviarDadosDialogo: state.enviarDadosDialogo,
                currentUser: state.currentUser,
                statusMessage: state.statusMessage
            })
        }

        if (mapLoaded.current) {
            mapFitToElements()
        }
    }

    const getNameTipoUsoOuConflito = (marker) => {
        let nameTipoUsoOuConflito
        if (marker.object?.__typename == 'Conflito') {
            for (let i = 0; i < tipos.data.tiposConflito.length; i++) {
                if (
                    marker.object.tipoConflitoId ==
                    tipos.data.tiposConflito[i].id
                ) {
                    nameTipoUsoOuConflito = tipos.data.tiposConflito[i].nome
                    break
                }
            }
        } else {
            for (let i = 0; i < tipos.data.tiposAreaDeUso.length; i++) {
                if (
                    marker.object.tipoAreaDeUsoId ==
                    tipos.data.tiposAreaDeUso[i].id
                ) {
                    nameTipoUsoOuConflito = tipos.data.tiposAreaDeUso[i].nome
                    break
                }
            }
        }
        return nameTipoUsoOuConflito
    }

    // Translate polygons and markers to map
    const gqlTerritorioToMapa = (params) => {
        if (!('currentUser' in params)) {
            return
        }
        const currentUser = params.currentUser
        //console.log("flag gql");
        //console.log(currentUser);
        let polygons = []
        let markers = []
        let todasCoords = { latitude: [], longitude: [] }

        // Generate map polygons:
        if (currentUser.currentTerritorio.poligono) {
            for (let poly of currentUser.currentTerritorio.poligono
                .coordinates) {
                let newPolygon = {
                    id: polygons.length + 1,
                    coordinates: [],
                    holes: [],
                    area: 0
                }
                for (let i = 0; i < poly[0].length - 1; i++) {
                    newPolygon.coordinates.push({
                        latitude: poly[0][i][1],
                        longitude: poly[0][i][0]
                    })
                    todasCoords.latitude.push(poly[0][i][1])
                    todasCoords.longitude.push(poly[0][i][0])
                }
                newPolygon.area = calculaAreaPoligono(
                    newPolygon.coordinates,
                    state.settings.unidadeArea
                )
                polygons.push(newPolygon)
            }
        }


        let markersTipoUsoConflito = []
        // Generate map markers - conflitos
        for (let conflito of currentUser.currentTerritorio.conflitos) {
            if (conflito.posicao) {
                //const iconeConflitoId = (conflito.tipoConflitoId > tipos.data.tiposConflito.length ) ? tipos.data.tiposConflito.length : conflito.tipoConflitoId;
                const iconeConflitoId = tipos.data.tiposConflito.length
                let icone = geraIconeTipoUsoOuConflito(
                    'conflito',
                    iconeConflitoId
                )
                let newConflito = {
                    id: markers.length + 1,
                    title: conflito.nome,
                    description: conflito.descricao,
                    coordinate: {
                        latitude: conflito.posicao.coordinates[1],
                        longitude: conflito.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: conflito
                }
                todasCoords.latitude.push(conflito.posicao.coordinates[1])
                todasCoords.longitude.push(conflito.posicao.coordinates[0])
                markers.push(newConflito)
                mapMarker.push(null)
                if (!conflito.tipoConflito) {
                    markersTipoUsoConflito.push(
                        getNameTipoUsoOuConflito(newConflito)
                    )
                } else {
                    markersTipoUsoConflito.push(conflito.tipoConflito.nome)
                }
            }
        }

        // Generate map markers - locais de uso
        for (let areaDeUso of currentUser.currentTerritorio.areasDeUso) {
            if (areaDeUso.posicao) {
                //const iconeAreaDeUsoId = (areaDeUso.tipoAreaDeUsoId > tipos.data.tiposAreaDeUso.length ) ? tipos.data.tiposAreaDeUso.length : areaDeUso.tipoAreaDeUsoId;
                const iconeAreaDeUsoId = tipos.data.tiposAreaDeUso.length
                let icone = geraIconeTipoUsoOuConflito(
                    'areaDeUso',
                    iconeAreaDeUsoId
                )
                let newAreaDeUso = {
                    id: markers.length + 1,
                    title: areaDeUso.nome,
                    description: areaDeUso.descricao,
                    coordinate: {
                        latitude: areaDeUso.posicao.coordinates[1],
                        longitude: areaDeUso.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: areaDeUso
                }
                todasCoords.latitude.push(areaDeUso.posicao.coordinates[1])
                todasCoords.longitude.push(areaDeUso.posicao.coordinates[0])
                markers.push(newAreaDeUso)
                mapMarker.push(null)
                if (!areaDeUso.tipoAreaDeUso) {
                    markersTipoUsoConflito.push(
                        getNameTipoUsoOuConflito(newAreaDeUso)
                    )
                } else {
                    markersTipoUsoConflito.push(areaDeUso.tipoAreaDeUso.nome)
                }
            }
        }



        const newFormMarkers = []
        if (currentUser.currentTerritorio.censosPreve) {
            for (let form of currentUser.currentTerritorio.censosPreve) {
                if(form.posicao){//teste
                    let newForm = {
                        id: markers.length + 1,
                        title: `${form.setor} - ${form.numeroCasa}`,
                        description: '',
                        coordinate: {
                            latitude: form.posicao.coordinates[1],
                            longitude: form.posicao.coordinates[0]
                        },
                        icone: geraIconeTipoUsoOuConflito('areaDeUso', 6),
                        object: { ...form }
                    }
                    newFormMarkers.push(newForm)
                    todasCoords.latitude.push(form.posicao.coordinates[1])
                    todasCoords.longitude.push(form.posicao.coordinates[0])
                }
            }
        }

        const { municipio } = resgataEstadoMunicipioDoId(
            currentUser.currentTerritorio.municipioReferenciaId
        )
        if (polygons.length > 0 || markers.length > 0) {
            let diffCoords = {
                latitude:
                    Math.max(...todasCoords.latitude) -
                    Math.min(...todasCoords.latitude),
                longitude:
                    Math.max(...todasCoords.longitude) -
                    Math.min(...todasCoords.longitude)
            }
            state.region = {
                latitude:
                    Math.min(...todasCoords.latitude) + diffCoords.latitude / 2,
                longitude:
                    Math.min(...todasCoords.longitude) +
                    diffCoords.longitude / 2,
                latitudeDelta: diffCoords.latitude + LATITUDE_DELTA / 4,
                longitudeDelta: diffCoords.longitude + LONGITUDE_DELTA / 4
            }
        } else {
            state.region = {
                latitude: municipio.latitude,
                longitude: municipio.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
        }

        state.statusMessage = buildStatusMessage(
            currentUser.currentTerritorio.statusId
        )
        setFormMarkers(newFormMarkers);
        setState({
            region: state.region,
            statusMessage: state.statusMessage,
            currentUser: currentUser,
            polygons,
            markers: markers,
            markersTipoUsoConflito
        })
    }

    ////////////////////////////////
    // TELA MAPEAR_TERRITORIO
    ///////////////////////////////
    const qtdePoligonos = () => {
        let qtde = 0
        if (state.currentUser?.currentTerritorio?.poligono?.coordinates) {
            qtde =
                state.currentUser.currentTerritorio.poligono.coordinates.length
        }
        return qtde
    }

    ////////////////////////////
    //     gps
    ///////////////////////////

    const locationDesativar = () => {
        if (locationMonitoramentoUsuariaAtivo.current) {
            if (locationMonitoramentoUsuaria) {
                locationMonitoramentoUsuaria.remove()
            }
            locationMonitoramentoUsuariaAtivo.current = false
        }
        setState({ locationMarkerCoordinate: null })
    }
    const alertAppSemPermissoesGPS = () => {
        setState({ carregando: null })
        updateSettings({ settings: { useGPS: false } })
        Alert.alert(
            i18n.t('alertAppSemPermissoesGPS_errPart0'),
            i18n.t('alertAppSemPermissoesGPS_errPart1'),
            [
                {
                    text: 'Ok',
                    onPress: async () => {}
                }
            ],
            { cancelable: true }
        )
    }
    const locationAtivarAsync = async () => {
        // /*DEBUG*/ console.log('locationAtivar', locationMonitoraUsuaria.current);
        if (!locationMonitoramentoUsuariaAtivo.current && !state.carregando) {
            locationMonitoramentoUsuariaAtivo.current = true
            setState({ carregando: i18n.t('locationAtivarAsync_loading') })
            try {
                let { status } =
                    await Location.requestForegroundPermissionsAsync()
                if (status === 'granted') {
                    locationMonitoraUsuaria()
                    await locationPegaPosicaoUsuaria({ moveMapa: true })
                    updateSettings({ settings: { useGPS: true } })
                } else {
                    alertAppSemPermissoesGPS()
                }
            } catch (e) {
                alertAppSemPermissoesGPS()
            }
        } else if (!state.locationMarkerCoordinate) {
            location = await locationPegaPosicaoUsuaria()
            setState({
                locationMarkerCoordinate: location.coords,
                carregando: null
            })
        }
    }
    const locationAtivar = () => {
        locationAtivarAsync().catch((e) => {
            alertAppSemPermissoesGPS()
        })
    }
    const locationMonitoraUsuaria = () => {
        // /*DEBUG*/ console.log('locationMonitoraUsuaria');
        Location.watchPositionAsync(GEOLOCATION_OPTIONS, locationChanged).then(
            (res) => {
                locationMonitoramentoUsuaria = res
            }
        )
    }
    const locationPegaPosicaoUsuaria = async (params) => {
        const location = await Location.getCurrentPositionAsync({
            accuracy: Location.Accuracy.Highest
        })
        let newState = { carregando: null }
        if (location && params?.moveMapa) {
            const newCoords = {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            }
            if (!state.locationMarkerCoordinate) {
                newState.locationMarkerCoordinate = newCoords
            }
            map.current.animateCamera({ center: newCoords })
        }
        setState(newState)
        return location
    }
    const locationChanged = (location) => {
        // /*DEBUG*/ console.log('locationChanged');
        if (locationMarker?.current?.props?.coordinate) {
            setState({
                locationMarkerCoordinate: location.coords
            })
        }
    }

    const apagarTodosPoligonosOuMarkers = () => {
        let title, message, qtde
        if (state.tela == TELAS.MAPEAR_TERRITORIO) {
            qtde = qtdePoligonos()
            title = pluralize(
                qtde,
                i18n.t('apagarTodosPoligonosOuMarkers_title1Sing'),
                i18n.t('apagarTodosPoligonosOuMarkers_title1Plur')
            )
            message = pluralize(
                qtde,
                i18n.t('apagarTodosPoligonosOuMarkers_message1Sing'),
                i18n.t('apagarTodosPoligonosOuMarkers_message1Plur1') +
                    qtde +
                    i18n.t('apagarTodosPoligonosOuMarkers_message1Plur2')
            )
        } else {
            qtde = state.markers.length
            title = pluralize(
                qtde,
                i18n.t('apagarTodosPoligonosOuMarkers_title2Sing'),
                i18n.t('apagarTodosPoligonosOuMarkers_title2Plur')
            )
            message = pluralize(
                qtde,
                i18n.t('apagarTodosPoligonosOuMarkers_message2Sing'),
                i18n.t('apagarTodosPoligonosOuMarkers_message2Plur1') +
                    qtde +
                    i18n.t('apagarTodosPoligonosOuMarkers_message1Plu')
            )
        }
        Alert.alert(
            title,
            message,
            [
                {
                    text: i18n.t('apagarTodosPoligonosOuMarkers_alertTitle'),
                    onPress: async () => {}
                },
                {
                    text: i18n.t('apagarTodosPoligonosOuMarkers_alertText'),
                    onPress: async () => {
                        if (state.tela == TELAS.MAPEAR_TERRITORIO) {
                            persisteGraphqlData({
                                polygons: [],
                                snackBarMessage: pluralize(
                                    qtde,
                                    i18n.t(
                                        'apagarTodosPoligonosOuMarkers_snackBarMessagePlur'
                                    ),
                                    qtde +
                                        i18n.t(
                                            'apagarTodosPoligonosOuMarkers_snackBarMessagePlur2'
                                        )
                                )
                            })
                        } else {
                            let newCurrentUser = cloneCurrentUser(
                                state.currentUser
                            )
                            newCurrentUser.currentTerritorio.conflitos = []
                            newCurrentUser.currentTerritorio.areasDeUso = []
                            persisteGraphqlData({
                                currentUser: newCurrentUser,
                                markers: [],
                                snackBarMessage: pluralize(
                                    qtde,
                                    `${
                                        Constants.manifest.extra.translations
                                            .goodCategorySingular
                                    } ${i18n.t(
                                        'apagarTodosPoligonosOuMarkers_snackBarMessage2Sing'
                                    )} ${
                                        Constants.manifest.extra.translations
                                            .badCategorySingular
                                    } ${i18n.t(
                                        'apagarTodosPoligonosOuMarkers_snackBarMessage2Sing2'
                                    )}`,
                                    qtde +
                                        ` ${
                                            Constants.manifest.extra
                                                .translations.goodCategoryPlural
                                        } ${i18n.t(
                                            'apagarTodosPoligonosOuMarkers_snackBarMessage2Sing'
                                        )} ${
                                            Constants.manifest.extra
                                                .translations.badCategoryPlural
                                        } ${i18n.t(
                                            'apagarTodosPoligonosOuMarkers_snackBarMessage2Plur2'
                                        )}`
                                )
                            })
                        }
                    }
                }
            ],
            { cancelable: false }
        )
    }

    const apagarPoligonoEmEdicao = () => {
        if (state.settings.editing) {
            Alert.alert(
                i18n.t('apagarPoligonoEmEdicao_info1'),
                i18n.t('apagarPoligonoEmEdicao_info2'),
                [
                    {
                        text: i18n.t('apagarPoligonoEmEdicao_text1'),
                        onPress: async () => {}
                    },
                    {
                        text: i18n.t('apagarPoligonoEmEdicao_text2'),
                        onPress: async () => {
                            if (state.settings.editing.originalPolygon) {
                                persisteGraphqlData({
                                    polygons: [
                                        ...state.polygons,
                                        state.settings.editing.originalPolygon
                                    ],
                                    settings: { editing: null },
                                    snackBarMessage: i18n.t(
                                        'apagarPoligonoEmEdicao_snackBarMessage1'
                                    )
                                })
                            } else {
                                snackBarMessage = i18n.t(
                                    'apagarPoligonoEmEdicao_snackBarMessage2'
                                )
                                setSnackBarVisible(true)
                                updateSettings({
                                    settings: { editing: null },
                                    noSnackBarMessage: true
                                })
                            }
                        }
                    }
                ],
                { cancelable: true }
            )
        }
    }

    // Adiciona vértice ao polígono:
    const adicionaVerticePoligono = (e) => {
        let newEditing
        if (!state.settings.editing) {
            newEditing = {
                editing: {
                    id: state.polygons.length + 1,
                    coordinates: [e.nativeEvent.coordinate],
                    holes: [],
                    area: 0
                }
            }
        } else if (!state.creatingHole) {
            const newCoords = [
                ...state.settings.editing.coordinates,
                e.nativeEvent.coordinate
            ]
            const newArea = calculaAreaPoligono(
                newCoords,
                state.settings.unidadeArea
            )
            newEditing = {
                editing: {
                    ...state.settings.editing,
                    coordinates: newCoords,
                    area: newArea
                }
            }
        } else {
            const holes = [...state.settings.editing.holes]
            holes[holes.length - 1] = [
                ...holes[holes.length - 1],
                e.nativeEvent.coordinate
            ]
            newEditing = {
                editing: {
                    ...state.settings.editing,
                    id: state.polygons.length + 1,
                    coordinates: [...state.settings.editing.coordinates],
                    holes
                }
            }
        }
        updateSettings({ settings: newEditing, noSnackBarMessage: true })
    }

    // Adicionar vértice GPS ao polígono
    const mapearTerritorioModoGPSAddPonto = (coordsGPS) => {
        if (!state.settings.editing) {
            const newEditing = {
                editing: {
                    id: state.polygons.length + 1,
                    coordinates: [coordsGPS],
                    holes: [],
                    area: 0
                }
            }
            updateSettings({ settings: newEditing, noSnackBarMessage: true })
        } else {
            const lastCoord =
                state.settings.editing.coordinates[
                    state.settings.editing.coordinates.length - 1
                ]
            if (
                lastCoord.latitude == coordsGPS.latitude &&
                lastCoord.longitude == coordsGPS.longitude
            ) {
                Alert.alert(
                    i18n.t('mapearTerritorioModoGPSAddPonto_alertText1'),
                    i18n.t('mapearTerritorioModoGPSAddPonto_alertText2'),
                    [
                        {
                            text: 'Ok'
                        }
                    ],
                    { cancelable: true }
                )
            } else {
                snackBarMessage = i18n.t(
                    'mapearTerritorioModoGPSAddPonto_snackBarMessage'
                )
                setSnackBarVisible(true)
                const newCoords = [
                    ...state.settings.editing.coordinates,
                    coordsGPS
                ]
                const newArea = calculaAreaPoligono(
                    newCoords,
                    state.settings.unidadeArea
                )
                const newEditing = {
                    editing: {
                        ...state.settings.editing,
                        coordinates: newCoords,
                        area: newArea
                    }
                }
                updateSettings({
                    settings: newEditing,
                    noSnackBarMessage: true
                })
            }
        }
    }

    const marcarPontoSalvar = (ponto) => {
        const { markers } = state
        const marker = {
            id: markers.length + 1,
            ...ponto
        }
        setState({
            markers: [...markers, ponto]
        })
    }

    /* Marca ponto de uso ou conflito */
    const marcarPonto = (e) => {
        navigation.navigate('TiposDeMapeamento', {
            mapeamentoInfo: {
                posicao: {
                    type: 'Point',
                    coordinates: [
                        e.nativeEvent.coordinate.longitude,
                        e.nativeEvent.coordinate.latitude
                    ]
                }
            }
        })
    }

    const editarPonto = (mapeamento, index = null) => {
        if (index !== null) {
            mapMarker[index].hideCallout()
            calloutVisible.current = false
        }

        switch (mapeamento.__typename) {
            case 'Conflito':
            case 'AreaDeUso': {
                navigation.navigate('EditarTiposScreen', {
                    ...mapeamento,
                    vindoDe: 'Home'
                })
                break
            }
            default: {
                const _mapeamento = { ...mapeamento, __typename: 'Form' }
                navigation.navigate('EditarTiposScreen', {
                    ..._mapeamento,
                    vindoDe: 'Home'
                })
                break
            }
        }
    }

    // Marcar ponto ou conflito usando GPS
    const mapearUsoConflitoModoGPSAddPonto = (coordsGPS) => {
        const lastCoord = state.markers[state.markers.length - 1]?.coordinate
        if (
            lastCoord &&
            lastCoord.latitude == coordsGPS.latitude &&
            lastCoord.longitude == coordsGPS.longitude
        ) {
            Alert.alert(
                i18n.t('mapearUsoConflitoModoGPSAddPonto_alertMessage1'),
                i18.t('mapearUsoConflitoModoGPSAddPonto_alertMessage2'),
                [
                    {
                        text: 'Ok'
                    }
                ],
                { cancelable: true }
            )
        } else {
            navigation.navigate('TiposDeMapeamento', {
                mapeamentoInfo: {
                    posicao: {
                        type: 'Point',
                        coordinates: [coordsGPS.longitude, coordsGPS.latitude]
                    }
                }
            })
        }
    }

    ///////////////////////////////
    // Generic utils:
    ///////////////////////////////

    const mapMovedOrTouched = (e) => {
        if (!calloutVisible.current) {
            if (state.tela === TELAS.MAPEAR_TERRITORIO) {
                adicionaVerticePoligono(e)
            } else if (state.tela === TELAS.MAPEAR_USOSECONFLITOS) {
                marcarPonto(e)
            }
        } else if (calloutVisible.current) {
            calloutVisible.current = false
        }
    }

    const mapOptions = {
        scrollEnabled: true
    }

    let statusMapeamentoTxt = ''
    let appBarTitle = null
    switch (state.tela) {
        case TELAS.MAPEAR_TERRITORIO:
            appBarTitle = 'Mapear o território'
            if (
                !state.settings.editing?.coordinates?.length &&
                qtdePoligonos() == 0
            ) {
                statusMapeamentoTxt = state.locationMarkerCoordinate
                    ? i18n.t('mapMovedOrTouched_MTstatusMapeamentoTxt1')
                    : i18n.t('mapMovedOrTouched_MTstatusMapeamentoTxt2')
            } else {
                if (qtdePoligonos() > 0) {
                    let areaTxt =
                        ' (' +
                        formataArea(
                            areaPoligonos(),
                            state.settings.unidadeArea
                        ) +
                        '). '
                    statusMapeamentoTxt =
                        qtdePoligonos() +
                        pluralize(
                            qtdePoligonos(),
                            ' área salva',
                            ' áreas salvas'
                        ) +
                        areaTxt
                }
                if (state.settings.editing?.coordinates?.length) {
                    //statusMapeamentoTxt += "Marcando nova área: " + state.settings.editing.coordinates.length + pluralize(state.settings.editing.coordinates.length, " vértice."," vértices.");
                    statusMapeamentoTxt += 'Nova área em edição'
                    if (state.settings.editing.coordinates.length >= 3) {
                        let area = calculaAreaPoligono(
                            state.settings.editing.coordinates,
                            state.settings.unidadeArea,
                            true
                        )
                        statusMapeamentoTxt += ' (' + area + ')'
                    }
                }
            }
            break
        case TELAS.MAPEAR_USOSECONFLITOS:
            appBarTitle = `${Constants.manifest.extra.translations.goodCategoryPlural} e ${Constants.manifest.extra.translations.badCategoryPlural}`
            if (!state.markers.length) {
                statusMapeamentoTxt = state.locationMarkerCoordinate
                    ? `${i18n.t(
                          'mapMovedOrTouched_MUCstatusMapeamentoTxt1part1'
                      )} ${
                          Constants.manifest.extra.translations
                              .goodCategorySingular
                      } ${i18n.t(
                          'mapMovedOrTouched_MUCstatusMapeamentoTxt1part2'
                      )} ${
                          Constants.manifest.extra.translations
                              .badCategorySingular
                      }`
                    : `${i18n.t(
                          'mapMovedOrTouched_MUCstatusMapeamentoTxt2part1'
                      )} ${
                          Constants.manifest.extra.translations
                              .goodCategorySingular
                      } ${i18n.t(
                          'mapMovedOrTouched_MUCstatusMapeamentoTxt1part2'
                      )} ${
                          Constants.manifest.extra.translations
                              .badCategorySingular
                      }`
            } else {
                statusMapeamentoTxt =
                    i18n.t(
                        'mapMovedOrTouched_MUCstatusMapeamentoMessage1Part1'
                    ) +
                    state.markers.length +
                    pluralize(
                        state.markers.length,
                        ` ${
                            Constants.manifest.extra.translations
                                .goodCategorySingular
                        } ${i18n.t(
                            'mapMovedOrTouched_MUCstatusMapeamentoMessage1Part2'
                        )} ${
                            Constants.manifest.extra.translations
                                .badCategorySingular
                        }`,
                        ` ${
                            Constants.manifest.extra.translations
                                .goodCategoryPlural
                        } ${i18n.t(
                            'mapMovedOrTouched_MUCstatusMapeamentoMessage1Part3'
                        )} ${
                            Constants.manifest.extra.translations
                                .badCategoryPlural
                        }`
                    )
            }
            break
        case TELAS.ENVIAR:
            appBarTitle = i18n.t(
                'mapMovedOrTouched_TEstatusMapeamentoAppBarTitle'
            )
    }

    const iconeTipo = (marker) => {
        return marker.object?.__typename === 'Conflito'
            ? ICONE_TIPO.conflito
            : ICONE_TIPO.areaDeUso
    }

    let mapTypeStyles = {
        label: i18n.t('mapTypeStyles_label1'),
        polygonFillColor: colors.polygonBackgroundHybrid,
        polygonEditingStrokeColor: colors.background,
        backgroundMarkerView: 'white',
        logoImageToShow: LOGO_IMAGE.branca,
        markerIconToShow: ICONE_TIPO.locationMarkClaro,
        iconButtonStyle: LayoutMapa.mapearStatusButtonClaro
    }
    if (
        Constants.manifest.extra.xyzRTiles.using ||
        state.settings.mapType != MAP_TYPES.HYBRID
    ) {
        mapTypeStyles = {
            label: i18n.t('mapTypeStyles_label2'),
            polygonFillColor: colors.polygonBackgroundStandard,
            polygonEditingStrokeColor: colors.text,
            backgroundMarkerView: 'black',
            logoImageToShow: LOGO_IMAGE.colorida,
            markerIconToShow: ICONE_TIPO.locationMark,
            iconButtonStyle: LayoutMapa.mapearStatusButton
        }
    }
    /*
    const mapTypeLabel = (state.settings.mapType == MAP_TYPES.HYBRID)
        ? "Mapa"
        : "Satélite";
    const mapTypePolygonFillColor =
        Constants.manifest.extra.xyzRTiles.using
        ? colors.polygonBackgroundStandard
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? colors.polygonBackgroundHybrid
        : colors.polygonBackgroundStandard;
    const mapTypePolygonEditingStrokeColor =
        Constants.manifest.extra.xyzRTiles.using
        ? colors.text
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? colors.background
        : colors.text;
    const mapTypeBackgroundMarkerView =
        Constants.manifest.extra.xyzRTiles.using
        ? "black"
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? "white"
        : "black";
    const mapTypeLogoImageToShow =
        Constants.manifest.extra.xyzRTiles.using
        ? LOGO_IMAGE.colorida
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? LOGO_IMAGE.branca
        : LOGO_IMAGE.colorida;
    const mapTypeMarkerIconToShow =
        Constants.manifest.extra.xyzRTiles.using
        ? ICONE_TIPO.locationMark
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? ICONE_TIPO.locationMarkClaro
        : ICONE_TIPO.locationMark;
    const mapTypeIconButtonStyle =
        Constants.manifest.extra.xyzRTiles.using
        ? LayoutMapa.mapearStatusButton
        : state.settings.mapType == MAP_TYPES.HYBRID
        ? LayoutMapa.mapearStatusButtonClaro
        : LayoutMapa.mapearStatusButton;
    */

    const renderSlideNext = () => {
        return <SlideWidget type='Next' />
    }
    const renderSlidePrev = () => {
        return <SlideWidget type='Prev' />
    }
    const renderSlideDone = () => {
        return <SlideWidget type='Done' />
    }

    // /*DEBUG*/ console.log(state);

    //////////////////////
    // RENDER!
    /////////////////////

    return (
        <>
            {state.settings.appIntro && (
                <>
                    <Portal>
                        <Dialog
                            visible={state.introDialog}
                            onDismiss={() => {
                                setState({ introDialog: false })
                            }}
                        >
                            <Dialog.Title>
                                {i18n.t('DialogTitlePortal')}
                            </Dialog.Title>
                            <Dialog.Content>
                                <View style={Layout.col}>
                                    <Text style={Layout.textParagraph}>
                                        {i18n.t('DialogContentPortal')}
                                    </Text>
                                </View>
                            </Dialog.Content>
                            <Dialog.Actions
                                style={{ justifyContent: 'space-between' }}
                            >
                                <Button
                                    onPress={() => {
                                        updateSettings({
                                            settings: { appIntro: false },
                                            noSnackBarMessage: true
                                        })
                                    }}
                                >
                                    {i18n.t('DialogPortalOption2')}
                                </Button>
                                <Button
                                    onPress={() => {
                                        setState({ introDialog: false })
                                    }}
                                >
                                    {i18n.t('DialogPortalOption1')}
                                </Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>

                    <AppIntroSlider
                        renderItem={renderSlide}
                        data={SLIDES}
                        renderNextButton={renderSlideNext}
                        renderPrevButton={renderSlidePrev}
                        renderDoneButton={renderSlideDone}
                        showSkipButton={true}
                        skipLabel={i18n.t('AppIntroSlider_skipLabel')}
                        dotStyle={{ backgroundColor: '#666' }}
                        onDone={() => {
                            updateSettings({
                                settings: { appIntro: false },
                                noSnackBarMessage: true
                            })
                        }}
                    />
                </>
            )}
            {!state.settings.appIntro && (
                <View
                    style={
                        state.preparandoShot && 1 != 1
                            ? styles.containerShot
                            : styles.container
                    }
                >
                    <MapView
                        ref={map}
                        provider={
                            Constants.manifest.extra.xyzRTiles.using
                                ? null
                                : PROVIDER_GOOGLE
                        }
                        style={LayoutMapa.map}
                        mapType={
                            Constants.manifest.extra.xyzRTiles.using
                                ? MAP_TYPES.NONE
                                : state.settings.mapType
                        }
                        initialRegion={state.region}
                        onLayout={() => {
                            if (state.polygons.length || state.markers.length) {
                                mapFitToElements()
                            }
                            mapLoaded.current = true
                        }}
                        zoomControlEnabled={false}
                        onPress={(e) => mapMovedOrTouched(e)}
                        {...mapOptions}
                    >
                        {Constants.manifest.extra.xyzRTiles.using ? (
                            // renders only if the app is configured to use an XYZ raster til
                            <MapView.UrlTile
                                urlTemplate={
                                    !!state.settings.offlineMap
                                        ? `${FileSystem.cacheDirectory}tiles/{z}/{x}/{y}.png`
                                        : Constants.manifest.extra.xyzRTiles
                                              .urlTile
                                }
                                maximumZ={19}
                                flipY={false}
                            />
                        ) : null}

                        {state.locationMarkerCoordinate &&
                            !state.preparandoShot && (
                                <Marker
                                    key='locationMarker'
                                    ref={locationMarker}
                                    tracksViewChanges={false}
                                    coordinate={state.locationMarkerCoordinate}
                                    image={mapTypeStyles.markerIconToShow}
                                    onPress={() => {
                                        calloutVisible.current = true
                                    }}
                                >
                                    <Callout
                                        onPress={(props) => {
                                            let coordsGPS =
                                                state.locationMarkerCoordinate
                                            locationMarker.current.hideCallout()
                                            if (
                                                state.tela ==
                                                TELAS.MAPEAR_TERRITORIO
                                            ) {
                                                mapearTerritorioModoGPSAddPonto(
                                                    coordsGPS
                                                )
                                            } else {
                                                mapearUsoConflitoModoGPSAddPonto(
                                                    coordsGPS
                                                )
                                            }
                                        }}
                                    >
                                        <View
                                            style={[
                                                Layout.col,
                                                { width: metrics.tenWidth * 16 }
                                            ]}
                                        >
                                            <Text
                                                style={
                                                    Layout.calloutActionCenter
                                                }
                                            >
                                                Clique para adicionar
                                            </Text>
                                            <Text
                                                style={
                                                    Layout.calloutActionCenter
                                                }
                                            >
                                                sua posição
                                            </Text>
                                        </View>
                                    </Callout>
                                </Marker>
                            )}
                        {state.polygons.map((polygon, index) => (
                            <Polygon
                                key={polygon.id}
                                ref={(ref) => (mapPolygon[index] = ref)}
                                coordinates={polygon.coordinates}
                                holes={polygon.holes}
                                strokeColor={colors.warning}
                                fillColor={mapTypeStyles.polygonFillColor}
                                strokeWidth={3}
                                tappable={
                                    state.tela != TELAS.MAPEAR_USOSECONFLITOS
                                }
                                zIndex={2}
                                onPress={(e) => {
                                    calloutVisible.current = true
                                    const alertApagarArea = () => {
                                        Alert.alert(
                                            i18n.t('alertApagarArea_Part1'),
                                            i18n.t('alertApagarArea_Part2') +
                                                formataArea(
                                                    polygon.area,
                                                    state.settings.unidadeArea
                                                ) +
                                                i18n.t('alertApagarArea_Part3'),
                                            [
                                                {
                                                    text: i18n.t(
                                                        'alertApagarArea_Op1'
                                                    )
                                                },
                                                {
                                                    text: i18n.t(
                                                        'alertApagarArea_Op2'
                                                    ),
                                                    onPress: async () => {
                                                        state.polygons.splice(
                                                            index,
                                                            1
                                                        )
                                                        persisteGraphqlData({
                                                            polygons:
                                                                state.polygons,
                                                            snackBarMessage:
                                                                i18n.t(
                                                                    'alertApagarArea_alertSnackBarMessage'
                                                                )
                                                        })
                                                    }
                                                }
                                            ],
                                            { cancelable: true }
                                        )
                                    }
                                    if (state.settings.editaPoligonos) {
                                        let buttons = [
                                            {
                                                text: i18n.t(
                                                    'alertApagarArea_editaPoligonosText1'
                                                )
                                            },
                                            {
                                                text: i18n.t(
                                                    'alertApagarArea_editaPoligonosText2'
                                                ),
                                                onPress: async () => {
                                                    alertApagarArea()
                                                }
                                            },
                                            {
                                                text: i18n.t(
                                                    'alertApagarArea_editaPoligonosText3'
                                                ),
                                                onPress: async () => {
                                                    let polygon =
                                                        state.polygons.splice(
                                                            index,
                                                            1
                                                        )
                                                    persisteGraphqlData({
                                                        polygons:
                                                            state.polygons,
                                                        settings: {
                                                            editing: {
                                                                id: polygon[0]
                                                                    .id,
                                                                coordinates:
                                                                    polygon[0]
                                                                        .coordinates,
                                                                holes: polygon[0]
                                                                    .holes,
                                                                area: polygon[0]
                                                                    .area,
                                                                originalPolygon:
                                                                    polygon[0]
                                                            }
                                                        }
                                                    })
                                                    navigation.navigate(
                                                        'Home',
                                                        {
                                                            tela: TELAS.MAPEAR_TERRITORIO
                                                        }
                                                    )
                                                }
                                            }
                                        ]
                                        if (
                                            state.tela ==
                                            TELAS.MAPEAR_TERRITORIO
                                        ) {
                                            buttons.push({
                                                text: i18n.t(
                                                    'addVerticeMapearTerritorio'
                                                ),
                                                onPress: async () => {
                                                    adicionaVerticePoligono(e)
                                                }
                                            })
                                        }
                                        Alert.alert(
                                            i18n.t('alertApagarTerritoriop1'),
                                            i18n.t('alertApagarTerritoriop2') +
                                                formataArea(
                                                    polygon.area,
                                                    state.settings.unidadeArea
                                                ) +
                                                i18n.t(
                                                    'alertApagarTerritoriop3'
                                                ),
                                            buttons,
                                            { cancelable: true }
                                        )
                                    } else {
                                        alertApagarArea()
                                    }
                                }}
                            />
                        ))}
                        {!state.preparandoShot &&
                            [...state.markers, ...formMarkers ,...otherUserMarkers].map(
                                (marker, index) => (
                                    <Marker
                                        key={index}
                                        ref={(ref) => (mapMarker[index] = ref)}
                                        coordinate={marker.coordinate}
                                        title={marker.title}
                                        description={marker.description}
                                        onPress={() => {
                                            calloutVisible.current = true
                                        }}
                                        image={marker.icone}
                                        tracksViewChanges={false}
                                        draggable
                                        onDragEnd={(e) => {
                                            let usoOuConflitoEditado = {
                                                ...state.markers[index].object,
                                                posicao: {
                                                    ...state.markers[index]
                                                        .object.posicao,
                                                    coordinates: [
                                                        e.nativeEvent.coordinate
                                                            .longitude,
                                                        e.nativeEvent.coordinate
                                                            .latitude
                                                    ]
                                                }
                                            }
                                            navigation.navigate('Home', {
                                                usoOuConflitoEditado:
                                                    usoOuConflitoEditado,
                                                tela: state.tela,
                                                usoOuConflitoMoveu: true,
                                                snackBarMessage: (() => {
                                                    switch (
                                                        marker.object
                                                            ?.__typename
                                                    ) {
                                                        case 'Conflito': {
                                                            return `${Constants.manifest.extra.translations.badCategorySingular} movido com sucesso`
                                                        }

                                                        case 'AreaDeUso': {
                                                            return `${Constants.manifest.extra.translations.goodCategorySingular} movido com sucesso`
                                                        }

                                                        case 'Form': {
                                                            return `${Constants.manifest.extra.translations.formCategorySingular} movido com sucesso`
                                                        }
                                                        default: {
                                                            break
                                                        }
                                                    }
                                                })()
                                            })
                                        }}
                                    >
                                        {marker.isFromOtherUser ? (
                                            <Callout>
                                                <View
                                                    style={[
                                                        Layout.col,
                                                        {
                                                            width:
                                                                metrics.tenWidth *
                                                                16
                                                        }
                                                    ]}
                                                >
                                                    <Text
                                                        style={
                                                            Layout.calloutTitle
                                                        }
                                                    >
                                                        {marker.title}
                                                    </Text>
                                                    <Text
                                                        style={
                                                            Layout.calloutDescription
                                                        }
                                                    >
                                                        <>
                                                            {(() => {
                                                                switch (
                                                                    marker
                                                                        .object
                                                                        .__typename
                                                                ) {
                                                                    case 'Conflito': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .badCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }

                                                                    case 'AreaDeUso': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .goodCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }

                                                                    case 'Form': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .formCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }
                                                                }
                                                            })()}
                                                            <Text>
                                                                {
                                                                    [
                                                                        ...state.markersTipoUsoConflito,
                                                                        ...otherUserMarkersTipo
                                                                    ][index]
                                                                }
                                                            </Text>
                                                        </>
                                                    </Text>
                                                </View>
                                            </Callout>
                                        ) : (
                                            <Callout
                                                onPress={() => {
                                                    editarPonto(
                                                        marker.object,
                                                        index
                                                    )
                                                }}
                                            >
                                                <View
                                                    style={[
                                                        Layout.col,
                                                        {
                                                            width:
                                                                metrics.tenWidth *
                                                                16
                                                        }
                                                    ]}
                                                >
                                                    <Text
                                                        style={
                                                            Layout.calloutTitle
                                                        }
                                                    >
                                                        {marker.title}
                                                    </Text>
                                                    <Text
                                                        style={
                                                            Layout.calloutDescription
                                                        }
                                                    >
                                                        <>
                                                            {(() => {
                                                                switch (
                                                                    marker
                                                                        .object
                                                                        ?.__typename ||
                                                                    ''
                                                                ) {
                                                                    case 'Conflito': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .badCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }

                                                                    case 'AreaDeUso': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .goodCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }

                                                                    case 'Form': {
                                                                        return (
                                                                            <Text>
                                                                                {
                                                                                    Constants
                                                                                        .manifest
                                                                                        .extra
                                                                                        .translations
                                                                                        .formCategorySingular
                                                                                }

                                                                                :{' '}
                                                                            </Text>
                                                                        )
                                                                    }

                                                                    default: {
                                                                        break
                                                                    }
                                                                }
                                                            })()}
                                                            <Text>
                                                                {
                                                                    [
                                                                        ...state.markersTipoUsoConflito,
                                                                        ...otherUserMarkersTipo
                                                                    ][index]
                                                                }
                                                            </Text>
                                                        </>
                                                    </Text>
                                                    <Text
                                                        style={
                                                            Layout.calloutAction
                                                        }
                                                    >
                                                        Editar ou apagar
                                                    </Text>
                                                </View>
                                            </Callout>
                                        )}
                                    </Marker>
                                )
                            )}
                        {state.preparandoShot &&
                            [...state.markers, ...formMarkers,...otherUserMarkers].map(
                                (marker, index) => (
                                    <Marker
                                        key={marker.id}
                                        ref={(ref) => (mapMarker[index] = ref)}
                                        coordinate={marker.coordinate}
                                        image={marker.icone}
                                    ></Marker>
                                )
                            )}
                        {state.settings.editing && (
                            <>
                                <Polygon
                                    key='mapEditingPolygon'
                                    ref={mapEditingPolygon}
                                    coordinates={
                                        state.settings.editing.coordinates
                                    }
                                    holes={state.settings.editing.holes}
                                    strokeColor={
                                        mapTypeStyles.polygonEditingStrokeColor
                                    }
                                    fillColor='rgba(255,0,0,0.5)'
                                    strokeWidth={3}
                                    tappable={state.tela == TELAS.MAPA}
                                    zIndex={2}
                                    onPress={() => {
                                        navigation.navigate('Home', {
                                            tela: TELAS.MAPEAR_TERRITORIO
                                        })
                                    }}
                                />
                                {!state.settings.editaPoligonos &&
                                    state.tela == TELAS.MAPEAR_TERRITORIO &&
                                    state.settings.editing.coordinates.map(
                                        (coord, index) => (
                                            <Marker
                                                key={index}
                                                coordinate={coord}
                                                centerOffset={{
                                                    x: 0.5,
                                                    y: 0.5
                                                }}
                                                anchor={{ x: 0.5, y: 0.5 }}
                                            >
                                                {(index == 0 ||
                                                    index ==
                                                        state.settings.editing
                                                            .coordinates
                                                            .length -
                                                            1) && (
                                                    <MaterialCommunityIcons
                                                        name='circle'
                                                        size={
                                                            metrics.tenWidth * 2
                                                        }
                                                        style={{
                                                            paddingTop: 0,
                                                            paddingLeft: 0,
                                                            color:
                                                                index == 0
                                                                    ? colors.primary
                                                                    : colors.danger
                                                        }}
                                                    />
                                                )}
                                            </Marker>
                                        )
                                    )}
                                {state.settings.editaPoligonos &&
                                    state.tela == TELAS.MAPEAR_TERRITORIO &&
                                    state.settings.editing.coordinates.map(
                                        (coord, index) => (
                                            <Marker
                                                key={index}
                                                ref={(ref) =>
                                                    (mapEditingPolygonEdge[
                                                        index
                                                    ] = ref)
                                                }
                                                coordinate={coord}
                                                onPress={() => {
                                                    calloutVisible.current = true
                                                }}
                                                centerOffset={{
                                                    x: 0.5,
                                                    y: 0.5
                                                }}
                                                anchor={{ x: 0.5, y: 0.5 }}
                                                draggable
                                                onDragEnd={(e) => {
                                                    let coords = [
                                                        ...state.settings
                                                            .editing.coordinates
                                                    ]
                                                    coords[index] =
                                                        e.nativeEvent.coordinate
                                                    const newEditing = {
                                                        editing: {
                                                            ...state.settings
                                                                .editing,
                                                            coordinates: coords
                                                        }
                                                    }
                                                    updateSettings({
                                                        settings: newEditing,
                                                        noSnackBarMessage: true
                                                    })
                                                }}
                                            >
                                                <View
                                                    style={[
                                                        Layout.vertice,
                                                        {
                                                            backgroundColor:
                                                                mapTypeStyles.backgroundMarkerView
                                                        }
                                                    ]}
                                                >
                                                    {(index == 0 ||
                                                        index ==
                                                            state.settings
                                                                .editing
                                                                .coordinates
                                                                .length -
                                                                1) && (
                                                        <MaterialCommunityIcons
                                                            name='circle'
                                                            size={
                                                                metrics.tenWidth *
                                                                2
                                                            }
                                                            style={{
                                                                paddingTop: 0,
                                                                paddingLeft: 0,
                                                                color:
                                                                    index == 0
                                                                        ? colors.primary
                                                                        : colors.danger
                                                            }}
                                                        />
                                                    )}
                                                </View>
                                                <Callout
                                                    onPress={() => {
                                                        Alert.alert(
                                                            i18n.t(
                                                                'calloutApagarVerticeAlertP1'
                                                            ),
                                                            i18n.t(
                                                                'calloutApagarVerticeAlertP2'
                                                            ),
                                                            [
                                                                {
                                                                    text: i18n.t(
                                                                        'calloutApagarVerticeAlertOp1'
                                                                    )
                                                                },
                                                                {
                                                                    text: i18n.t(
                                                                        'calloutApagarVerticeAlertOp2'
                                                                    ),
                                                                    onPress:
                                                                        async () => {
                                                                            mapEditingPolygonEdge[
                                                                                index
                                                                            ].hideCallout()
                                                                            calloutVisible.current = false
                                                                            let coords =
                                                                                [
                                                                                    ...state
                                                                                        .settings
                                                                                        .editing
                                                                                        .coordinates
                                                                                ]
                                                                            coords.splice(
                                                                                index,
                                                                                1
                                                                            )
                                                                            const newEditing =
                                                                                {
                                                                                    editing:
                                                                                        {
                                                                                            ...state
                                                                                                .settings
                                                                                                .editing,
                                                                                            coordinates:
                                                                                                coords
                                                                                        }
                                                                                }
                                                                            updateSettings(
                                                                                {
                                                                                    settings:
                                                                                        newEditing,
                                                                                    noSnackBarMessage: true
                                                                                }
                                                                            )
                                                                        }
                                                                }
                                                            ],
                                                            { cancelable: true }
                                                        )
                                                    }}
                                                >
                                                    <View
                                                        style={{
                                                            ...Layout.col,
                                                            minWidth:
                                                                metrics.tenWidth *
                                                                10
                                                        }}
                                                    >
                                                        <Text
                                                            style={{
                                                                ...Layout.calloutTitle,
                                                                flex: 1
                                                            }}
                                                        >
                                                            {i18n.t(
                                                                'calloutVerticeID'
                                                            ) +
                                                                (index + 1)}
                                                        </Text>
                                                        {index ==
                                                            state.settings
                                                                .editing
                                                                .coordinates
                                                                .length -
                                                                1 && (
                                                            <Text
                                                                style={
                                                                    Layout.calloutDescription
                                                                }
                                                            >
                                                                {i18n.t(
                                                                    'calloutVerticeText1'
                                                                )}
                                                            </Text>
                                                        )}
                                                        <Text
                                                            style={
                                                                Layout.calloutAction
                                                            }
                                                        >
                                                            {i18n.t(
                                                                'calloutVerticeText2'
                                                            )}
                                                        </Text>
                                                    </View>
                                                </Callout>
                                            </Marker>
                                        )
                                    )}
                            </>
                        )}
                        {state.editingMarker && (
                            <Marker
                                key={state.editingMarker.id}
                                coordinate={state.editingMarker.coordinate}
                                title={state.editingMarker.title}
                                description={state.editingMarker.description}
                                pinColor={'tan'}
                            />
                        )}
                    </MapView>
                    <Snackbar
                        visible={snackBarMessage && snackBarVisible == true}
                        style={{
                            position: 'absolute',
                            bottom: metrics.tenWidth * 6
                        }}
                        duration={2000}
                        onDismiss={() => {
                            setSnackBarVisible(false)
                        }}
                    >
                        {snackBarMessage}
                    </Snackbar>
                    {[
                        TELAS.MAPEAR_TERRITORIO,
                        TELAS.MAPEAR_USOSECONFLITOS
                    ].indexOf(state.tela) > -1 && (
                        <>
                            <View
                                style={{
                                    position: 'absolute',
                                    top: 0,
                                    width: '100%'
                                }}
                            >
                                <AppbarTonomapa
                                    navigation={navigation}
                                    title={appBarTitle}
                                    rightAction={{
                                        icon: 'crosshairs-gps',
                                        color: state.locationMarkerCoordinate
                                            ? colors.primary
                                            : colors.divider,
                                        fn: () => {
                                            if (
                                                state.locationMarkerCoordinate
                                            ) {
                                                Alert.alert(
                                                    i18n.t('gpsAlertp1'),
                                                    i18n.t('gpsAlertp2'),
                                                    [
                                                        {
                                                            text: i18n.t(
                                                                'gpsAlertText1'
                                                            )
                                                        },
                                                        {
                                                            text: i18n.t(
                                                                'gpsAlertText2'
                                                            ),
                                                            onPress:
                                                                async () => {
                                                                    updateSettings(
                                                                        {
                                                                            settings:
                                                                                {
                                                                                    useGPS: false
                                                                                }
                                                                        }
                                                                    )
                                                                    locationDesativar()
                                                                }
                                                        }
                                                    ],
                                                    { cancelable: true }
                                                )
                                            } else {
                                                Alert.alert(
                                                    i18n.t('gpsAlert2p1'),
                                                    i18n.t('gpsAlert2p2'),
                                                    [
                                                        {
                                                            text: i18n.t(
                                                                'gpsAlert2op1'
                                                            )
                                                        },
                                                        {
                                                            text: i18n.t(
                                                                'gpsAlert2op2'
                                                            ),
                                                            onPress:
                                                                async () => {
                                                                    locationAtivar()
                                                                }
                                                        }
                                                    ],
                                                    { cancelable: true }
                                                )
                                            }
                                        }
                                    }}
                                />
                                <View style={Layout.statusBar}>
                                    <Text style={Layout.statusBarText}>
                                        {statusMapeamentoTxt}
                                    </Text>
                                </View>
                            </View>
                        </>
                    )}
                    {[
                        TELAS.MAPEAR_TERRITORIO,
                        TELAS.MAPEAR_USOSECONFLITOS
                    ].indexOf(state.tela) > -1 && (
                        <View style={Layout.col}>
                            {state.locationMarkerCoordinate && (
                                <View
                                    style={{
                                        ...LayoutMapa.mapearButtonsContainer,
                                        display: 'none'
                                    }}
                                >
                                    <Button
                                        mode='contained'
                                        onPress={
                                            state.tela ==
                                            TELAS.MAPEAR_TERRITORIO
                                                ? mapearTerritorioModoGPSAddPonto
                                                : mapearUsoConflitoModoGPSAddPonto
                                        }
                                        color={colors.white}
                                        style={LayoutMapa.mapearButtons}
                                        contentStyle={{
                                            height: metrics.tenWidth * 4.5
                                        }}
                                        labelStyle={{
                                            ...LayoutMapa.mapearButtonsLabel,
                                            color: colors.text
                                        }}
                                        icon={() => (
                                            <Avatar.Icon
                                                size={metrics.tenWidth * 3.6}
                                                icon='compass'
                                                style={
                                                    LayoutMapa.avatarButtonModoInativo
                                                }
                                            />
                                        )}
                                    >
                                        {i18n.t('mapTerrPositionButton')}
                                    </Button>
                                </View>
                            )}

                            <View style={{ flexDirection: 'row' }}>
                                <View
                                    style={LayoutMapa.mapearButtonsContainerRow}
                                >
                                    <Button
                                        mode='contained'
                                        onPress={
                                            state.settings.editing
                                                ? apagarPoligonoEmEdicao
                                                : apagarTodosPoligonosOuMarkers
                                        }
                                        disabled={
                                            (state.tela ==
                                                TELAS.MAPEAR_TERRITORIO &&
                                                !state.settings.editing &&
                                                !qtdePoligonos()) ||
                                            (state.tela ==
                                                TELAS.MAPEAR_USOSECONFLITOS &&
                                                !state.markers.length)
                                        }
                                        color={colors.danger}
                                        style={LayoutMapa.mapearButtonsLeft}
                                        contentStyle={{
                                            height: metrics.tenWidth * 4.5
                                        }}
                                        labelStyle={{
                                            ...LayoutMapa.mapearButtonsLabel
                                        }}
                                        icon={({ size, color, direction }) => (
                                            <Avatar.Icon
                                                size={metrics.tenWidth * 3.6}
                                                icon={
                                                    state.settings.editing
                                                        ? 'cancel'
                                                        : 'broom'
                                                }
                                                style={
                                                    LayoutMapa.avatarButtonExit
                                                }
                                            />
                                        )}
                                    >
                                        {state.settings.editing ? (
                                            <Text>
                                                {i18n.t(
                                                    'mapearButtonsContainerRowOp1'
                                                )}
                                            </Text>
                                        ) : (
                                            <Text>
                                                {i18n.t(
                                                    'mapearButtonsContainerRowOp2'
                                                )}
                                            </Text>
                                        )}
                                    </Button>

                                    <Button
                                        mode='contained'
                                        disabled={
                                            state.tela ==
                                                TELAS.MAPEAR_TERRITORIO &&
                                            state.settings.editing &&
                                            state.settings.editing.coordinates
                                                .length < 3
                                        }
                                        onPress={() => {
                                            switch (state.tela) {
                                                case TELAS.MAPEAR_TERRITORIO:
                                                    if (
                                                        state.settings.editing
                                                    ) {
                                                        persisteGraphqlData({
                                                            polygons: [
                                                                ...state.polygons,
                                                                state.settings
                                                                    .editing
                                                            ],
                                                            settings: {
                                                                editing: null
                                                            },
                                                            snackBarMessage:
                                                                i18n.t(
                                                                    'mapTerrSnackBarMessage'
                                                                )
                                                        })
                                                    } else {
                                                        navigation.navigate(
                                                            'Home',
                                                            { tela: TELAS.MAPA }
                                                        )
                                                    }
                                                    break
                                                case TELAS.MAPEAR_USOSECONFLITOS:
                                                    navigation.navigate(
                                                        'Home',
                                                        { tela: TELAS.MAPA }
                                                    )
                                                    break
                                            }
                                        }}
                                        color={colors.primary}
                                        style={LayoutMapa.mapearButtonsRight}
                                        contentStyle={{
                                            height: metrics.tenWidth * 4.5
                                        }}
                                        labelStyle={
                                            LayoutMapa.mapearButtonsLabel
                                        }
                                        icon={({ size, color, direction }) => (
                                            <Avatar.Icon
                                                size={metrics.tenWidth * 3.6}
                                                icon='check'
                                                style={
                                                    LayoutMapa.avatarButtonDone
                                                }
                                            />
                                        )}
                                    >
                                        {state.settings.editing
                                            ? i18n.t('mapTerrEditing1')
                                            : i18n.t('mapTerrEditing2')}
                                    </Button>
                                </View>
                            </View>
                        </View>
                    )}
                    {state.currentUser && state.tela === TELAS.ENVIAR && (
                        <Portal>
                            <Dialog
                                visible={true}
                                onDismiss={() => {
                                    navigation.navigate('Home', {
                                        tela: TELAS.MAPA
                                    })
                                }}
                            >
                                <Dialog.Title>
                                    {i18n.t('portalDialogTelaEnviar')}
                                </Dialog.Title>
                                <Dialog.Content>
                                    <Text style={Layout.enviarTitle}>
                                        {
                                            state.currentUser.currentTerritorio
                                                .nome
                                        }
                                    </Text>
                                    {state.enviarDadosDialogo.etapa ==
                                        'situacao' &&
                                        !state.settings.editing &&
                                        (!state.settings.syncedToDashboard ||
                                            state.settings.syncedToDashboard ==
                                                STATUS.TELEFONE_NAO_REGISTRADO) &&
                                        state.enviarDadosDialogo.situacao.map(
                                            (resultado, index) => {
                                                return (
                                                    <View
                                                        style={Layout.row}
                                                        key={index}
                                                    >
                                                        <Avatar.Icon
                                                            size={
                                                                metrics.tenWidth *
                                                                3.6
                                                            }
                                                            icon={
                                                                resultado.icon
                                                            }
                                                            color={
                                                                resultado
                                                                    .iconStyle
                                                                    .color
                                                            }
                                                            style={
                                                                resultado.iconStyle
                                                            }
                                                        />
                                                        <Text>
                                                            {resultado.text}
                                                        </Text>
                                                    </View>
                                                )
                                            }
                                        )}
                                    {state.enviarDadosDialogo.etapa ==
                                        'situacao' &&
                                        state.settings.editing && (
                                            <View style={Layout.colLeft}>
                                                <Text
                                                    style={Layout.textParagraph}
                                                >
                                                    {i18n.t(
                                                        'portalDialogTelaEnviarEditingPart1'
                                                    )}
                                                </Text>
                                                <Text
                                                    style={Layout.textParagraph}
                                                >
                                                    {i18n.t(
                                                        'portalDialogTelaEnviarEditingPart2'
                                                    )}
                                                </Text>
                                            </View>
                                        )}
                                    {
                                        //O importante, quando o usuário não mudou nada
                                        state.enviarDadosDialogo.etapa ==
                                            'situacao' &&
                                            state.settings.syncedToDashboard &&
                                            state.settings.syncedToDashboard !=
                                                STATUS.TELEFONE_NAO_REGISTRADO && (
                                                <View style={Layout.colLeft}>
                                                    <Text
                                                        style={
                                                            Layout.textParagraph
                                                        }
                                                    >
                                                        {i18n.t(
                                                            'portalDialogTelaEnviarSyncServerPart1'
                                                        )}
                                                    </Text>
                                                    <Text
                                                        style={
                                                            Layout.textParagraph
                                                        }
                                                    >
                                                        {i18n.t(
                                                            'portalDialogTelaEnviarSyncServerPart2'
                                                        )}
                                                    </Text>
                                                </View>
                                            )
                                    }
                                    {state.enviarDadosDialogo.etapa ==
                                        'carregando' && (
                                        <View style={Layout.rowCenter}>
                                            <ActivityIndicator
                                                animating={true}
                                                color={colors.primary}
                                            />
                                        </View>
                                    )}
                                    {state.enviarDadosDialogo.etapa ==
                                        'sucesso' && (
                                        <View style={Layout.col}>
                                            <Text style={Layout.textParagraph}>
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoPart1'
                                                )}
                                            </Text>
                                            <Text style={Layout.textParagraph}>
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoPart2'
                                                )}
                                            </Text>
                                            <Text style={Layout.textParagraph}>
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoPart3'
                                                )}
                                            </Text>
                                        </View>
                                    )}
                                    {state.enviarDadosDialogo.etapa ==
                                        'erro' && (
                                        <View style={Layout.col}>
                                            <Text
                                                style={{
                                                    ...Layout.textParagraph,
                                                    ...Layout.bold
                                                }}
                                            >
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoErroPart1'
                                                )}
                                            </Text>
                                            <Text style={Layout.textParagraph}>
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoErroPart2'
                                                )}
                                            </Text>
                                            {state.enviarDadosDialogo.erro && (
                                                <>
                                                    <Text
                                                        style={{
                                                            ...Layout.textParagraph,
                                                            ...Layout.italic
                                                        }}
                                                    >
                                                        {i18n.t(
                                                            'portalDialogEnviarDadosDialogoErroPart3'
                                                        )}{' '}
                                                        {
                                                            state
                                                                .enviarDadosDialogo
                                                                .erro
                                                        }
                                                    </Text>
                                                </>
                                            )}
                                            <Text style={Layout.textParagraph}>
                                                {i18n.t(
                                                    'portalDialogEnviarDadosDialogoErroPart4'
                                                )}
                                            </Text>
                                        </View>
                                    )}
                                </Dialog.Content>
                                <Dialog.Actions>
                                    {!state.settings.editing &&
                                        (!state.settings.syncedToDashboard ||
                                            state.settings.syncedToDashboard ==
                                                STATUS.TELEFONE_NAO_REGISTRADO) &&
                                        state.enviarDadosDialogo
                                            .botaoCancelar && (
                                            <Button
                                                onPress={() => {
                                                    navigation.navigate(
                                                        'Home',
                                                        { tela: TELAS.MAPA }
                                                    )
                                                }}
                                            >
                                                {i18n.t(
                                                    'portalDialogEnviarDadosCancelButton'
                                                )}
                                            </Button>
                                        )}
                                    {!state.settings.editing &&
                                        (!state.settings.syncedToDashboard ||
                                            state.settings.syncedToDashboard ==
                                                STATUS.TELEFONE_NAO_REGISTRADO) &&
                                        state.enviarDadosDialogo
                                            .botaoEnviar && (
                                            <Button
                                                onPress={() => {
                                                    setState({
                                                        enviarDadosDialogo:
                                                            enviarDadosDialogoCarregando
                                                    })
                                                    enviaParaDashboard(
                                                        state.currentUser,
                                                        state.settings.appFiles
                                                    )
                                                        .then(
                                                            (res) => {
                                                                if (res) {
                                                                    // res devolve res.settings e res.currentUser
                                                                    // /*DEBUG*/ console.log("resposta enviaParaDashboard", res);

                                                                    if (
                                                                        res.token
                                                                    ) {
                                                                        signUp(
                                                                            res.token
                                                                        )
                                                                    }

                                                                    const stateChange =
                                                                        {
                                                                            enviarDadosDialogo:
                                                                                {
                                                                                    ...enviarDadosDialogoErro,
                                                                                    situacao:
                                                                                        'erro'
                                                                                }
                                                                        }
                                                                    if (
                                                                        res.currentUser
                                                                    ) {
                                                                        snackBarMessage =
                                                                            'Dados enviados com sucesso ao Tô no Prevê!'
                                                                        stateChange.enviarDadosDialogo.etapa =
                                                                            'sucesso'
                                                                        stateChange.snackBarVisible = true
                                                                        stateChange.currentUser =
                                                                            null // force data reload and status update
                                                                        otherUsersInfo(
                                                                            res.currentUser
                                                                        ).then(
                                                                            (
                                                                                val
                                                                            ) => {
                                                                                otherUserDataToMap(
                                                                                    val
                                                                                )
                                                                            }
                                                                        )
                                                                    } else {
                                                                        stateChange.enviarDadosDialogo.erro =
                                                                            res.erros
                                                                    }
                                                                    delete res.erros
                                                                    setState(
                                                                        stateChange
                                                                    )
                                                                } else {
                                                                    // TODO: que casos dá aqui?
                                                                    console.log(
                                                                        'Erro no enviaParaDashboard',
                                                                        e
                                                                    )
                                                                    setState({
                                                                        enviarDadosDialogo:
                                                                            {
                                                                                ...enviarDadosDialogoErro,
                                                                                erro: '[código 0] Erro enviando dados ao Tô no Prevê :( Por favor, tire um screenshot/print desta tela e envie para a equipe que administra o app.'
                                                                            }
                                                                    })
                                                                }
                                                            },
                                                            (resultError) => {
                                                                // TODO: que casos dá aqui?
                                                                console.log(
                                                                    'enviar dados outro tipo de erro',
                                                                    resultError
                                                                )
                                                                setState({
                                                                    enviarDadosDialogo:
                                                                        {
                                                                            ...enviarDadosDialogoErro,
                                                                            erro: '[código 1] Erro enviando dados ao Tô no Prevê :( Por favor, tire um screenshot/print desta tela e envie para a equipe que administra o app.'
                                                                        }
                                                                })
                                                            }
                                                        )
                                                        .catch((e) => {
                                                            // TODO: que casos dá aqui?
                                                            console.log(
                                                                'Erro no enviaParaDashboard',
                                                                e
                                                            )
                                                            setState({
                                                                enviarDadosDialogo:
                                                                    {
                                                                        ...enviarDadosDialogoErro,
                                                                        erro: '[código 2] Erro enviando dados ao Tô no Prevê :( Por favor, tire um screenshot/print desta tela e envie para a equipe que administra o app.'
                                                                    }
                                                            })
                                                        })
                                                }}
                                            >
                                                {i18n.t(
                                                    'portalDialogEnviarDadosSendButton'
                                                )}
                                            </Button>
                                        )}
                                    {state.enviarDadosDialogo.etapa ==
                                        'sucesso' && (
                                        <Button
                                            onPress={() => {
                                                setState({
                                                    enviarDadosDialogo:
                                                        initialState.enviarDadosDialogo
                                                })
                                                navigation.navigate('Home', {
                                                    tela: TELAS.MAPA,
                                                    ajustaMapaEExportaPDF: true
                                                })
                                            }}
                                        >
                                            {i18n.t(
                                                'portalDialogEnviarDadosExpReport'
                                            )}
                                        </Button>
                                    )}
                                    {(state.settings.editing ||
                                        (state.settings.syncedToDashboard &&
                                            state.settings.syncedToDashboard !=
                                                STATUS.TELEFONE_NAO_REGISTRADO) ||
                                        state.enviarDadosDialogo.botaoOk) && (
                                        <Button
                                            onPress={() => {
                                                setState({
                                                    enviarDadosDialogo:
                                                        initialState.enviarDadosDialogo
                                                })
                                                navigation.navigate('Home', {
                                                    tela: state.settings.editing
                                                        ? TELAS.MAPEAR_TERRITORIO
                                                        : TELAS.MAPA
                                                })
                                            }}
                                        >
                                            Ok
                                        </Button>
                                    )}
                                </Dialog.Actions>
                            </Dialog>
                        </Portal>
                    )}
                    {state.tela !== TELAS.ENVIAR && (
                        <View
                            style={{
                                paddingHorizontal: metrics.tenWidth * 0.5,
                                position: 'absolute',
                                top:
                                    state.tela == TELAS.MAPA
                                        ? metrics.tenWidth * 2
                                        : metrics.tenWidth * 11,
                                left: 0,
                                width: '100%'
                            }}
                        >
                            <SafeAreaView
                                style={{
                                    flexDirection: 'row',
                                    justifyContent:
                                        state.tela == TELAS.MAPA
                                            ? 'space-between'
                                            : 'flex-end',
                                    alignItems: 'center'
                                }}
                            >
                                {[TELAS.MAPA, TELAS.ENVIAR].indexOf(
                                    state.tela
                                ) > -1 &&
                                    state.statusMessage && (
                                        <IconButton
                                            size={metrics.tenWidth * 3.6}
                                            color={state.statusMessage.color}
                                            icon={state.statusMessage.icon}
                                            style={
                                                mapTypeStyles.iconButtonStyle
                                            }
                                            onPress={() => {
                                                Alert.alert(
                                                    state.statusMessage.nome,
                                                    state.statusMessage
                                                        .mensagem,
                                                    [
                                                        {
                                                            text: 'Entendi'
                                                        }
                                                    ],
                                                    { cancelable: true }
                                                )
                                            }}
                                        />
                                    )}
                                {!Constants.manifest.extra.xyzRTiles.using && (
                                    <Button
                                        mode='contained'
                                        contentStyle={{
                                            height: metrics.tenWidth * 3.6,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                        color={colors.background}
                                        labelStyle={{
                                            textAlign: 'center',
                                            textAlignVertical: 'center',
                                            fontSize: metrics.tenWidth * 1.6
                                        }}
                                        onPress={() => {
                                            switch (state.settings.mapType) {
                                                case MAP_TYPES.STANDARD:
                                                    state.settings.mapType =
                                                        MAP_TYPES.HYBRID
                                                    break
                                                case MAP_TYPES.HYBRID:
                                                    state.settings.mapType =
                                                        MAP_TYPES.STANDARD
                                                    break
                                            }
                                            updateSettings({
                                                settings: {
                                                    mapType:
                                                        state.settings.mapType
                                                },
                                                noSnackBarMessage: true
                                            })
                                        }}
                                    >
                                        {mapTypeStyles.label}
                                    </Button>
                                )}
                                {[TELAS.MAPA, TELAS.ENVIAR].indexOf(
                                    state.tela
                                ) > -1 && (
                                    <Image
                                        source={mapTypeStyles.logoImageToShow}
                                        style={LayoutMapa.mapearLogoImage}
                                        resizeMode='contain'
                                    />
                                )}
                            </SafeAreaView>
                        </View>
                    )}
                    {state.mapSnapshotUri && (
                        <View style={Layout.appOverlay}>
                            <Image source={{ uri: state.mapSnapshotUri }} />
                        </View>
                    )}
                    {[TELAS.MAPA, TELAS.ENVIAR].indexOf(state.tela) > -1 && (
                        <Appbar style={LayoutMapa.appbarBottom}>
                            <Appbar.Action
                                icon='tooltip-account'
                                size={metrics.tenWidth * 3}
                                onPress={() =>
                                    navigation.navigate('DadosBasicos', {
                                        atualizarDados: true
                                    })
                                }
                            />
                            <Appbar.Action
                                icon='map'
                                size={metrics.tenWidth * 3}
                                onPress={() =>
                                    navigation.navigate('Home', {
                                        tela: TELAS.MAPEAR_TERRITORIO
                                    })
                                }
                            />
                            <Appbar.Action
                                icon='map-marker'
                                size={metrics.tenWidth * 3}
                                onPress={() =>
                                    navigation.navigate('Home', {
                                        tela: TELAS.MAPEAR_USOSECONFLITOS
                                    })
                                }
                            />
                            <Appbar.Action
                                icon='file'
                                size={metrics.tenWidth * 3}
                                onPress={() => {
                                    navigation.navigate('AnexoList', {
                                        atualizarDados: true
                                    })
                                }}
                            />
                            <Appbar.Action
                                icon='menu'
                                size={metrics.tenWidth * 3}
                                onPress={() => navigation.openDrawer()}
                            />
                            {(!state.settings.syncedToDashboard ||
                                state.settings.syncedToDashboard ==
                                    STATUS.TELEFONE_NAO_REGISTRADO) && (
                                <View
                                    style={{
                                        position: 'absolute',
                                        bottom: metrics.tenWidth * 1.7,
                                        right: metrics.tenWidth * 1.3
                                    }}
                                >
                                    <IconButton
                                        icon='circle'
                                        size={metrics.tenWidth * 1.5}
                                        color={colors.warning}
                                        onPress={() => navigation.openDrawer()}
                                    />
                                </View>
                            )}
                        </Appbar>
                    )}
                </View>
            )}
            {(!state.settings || !state.region || state.carregando) && (
                <View style={Layout.appOverlay}>
                    <View
                        style={{
                            backgroundColor: colors.primary,
                            padding: metrics.tenWidth * 4,
                            alignItems: 'center',
                            borderRadius: metrics.tenWidth * 0.8
                        }}
                    >
                        <ActivityIndicator
                            size='large'
                            animating={true}
                            color={colors.white}
                        />
                        <Text
                            style={{
                                color: colors.white,
                                marginTop: metrics.tenWidth * 3
                            }}
                        >
                            {state.carregando
                                ? state.carregando
                                : i18n.t('stateCarregando')}
                        </Text>
                    </View>
                </View>
            )}
        </>
    )
}

MapaScreen.propTypes = {
    provider: ProviderPropType
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    containerShot: {
        width: 794,
        height: 1123,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
})
