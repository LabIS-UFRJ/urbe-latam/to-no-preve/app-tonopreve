import React from 'react';

import {Image, StyleSheet, View} from "react-native";
import { ActivityIndicator, Colors } from 'react-native-paper';

const styles = StyleSheet.create({
    logoImage: {
        marginTop: 0,

        height: '100%',
        width: '100%',
    },
});

export default function WaitingScreen() {
    return (
        <View style={{flex: 1}}>
            <ActivityIndicator animating={true} color={'#A1C736'} size={'large'} style={styles.logoImage} />
        </View>
        );
}
